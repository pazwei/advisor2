# Financial Advisor

## Why

Tracking investments and invetment opportunities is tedious, I don't have the time or the will to do so.
Sentiment is tightly cloded to the performance of the market.
Have calendar for compelling events that move significantly the market.

## Goals

1. Create simple tools that track indices and major news outlets for sentiment analysis.

2. The tool will run on daily basis automatically and will send daily report by mail

## Solution

Once a day sample and record the following:

1. Run sentiment analysis on major new outlets and track the scores. Show daily average on a graph

2. Run RSI analysis on major indices (Nasdaq, VIX, SP500)

3. Track top tier companies (GOOG, AMZN, AAPL, WIX, Pfizer...) 

4. Track some sort of calendar with market events

9. Track all data in a sqlite file

10. Present graphs

11. Show alerts when the sentiment short moving average is bellow or above the long moving average

12. Present buying opportunities when the RSI is low  

13. Present data in graph

14. Send daily email with alerts regarding market behavior, stock behavior, news sentiment

## Use the following libs:

1. Sqlalchemy

2. bote3 (amazon AWS) for sentiment analysis

3. Pandas

4. Alphavantage free plan (5 API requests per minute; 500 API requests per day) ^VIX ^GSPC

5. Jinja2

6. Beautifulsoup4

7. Requests

## Data sources

1. Seeking alpha

2. MarketWatch

3. Benzinga

4. Motley Fool

5. Yahoo finance

6. Bloomberg

7. cnbc

8. cnn

9. financial times

10. reuters

11. Investor's Business Daily

12. wall street journal

13. barron's

14. Business Wire

15. the fly

16. pr newswire

17. https://stocksearning.com/

18. https://iknowfirst.com/