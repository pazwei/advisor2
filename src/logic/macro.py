#!/usr/bin/env python3

from xml.etree import ElementTree as etree
import json
import base64
import io
import pprint
import datetime
import itertools

import requests
import bs4 as bs

import pandas as pd
import pandas_ta as ta # https://github.com/twopirllc/pandas-ta
import numpy as np

import db.queries as queries
import logic.charts
import utils.cli as cli

def ipos():
    #TODO: send latest ipos and track newly issued symbols
    ipos = queries.get_ipos_deal_ids()
    upcomming = [i for i in ipos if i.ipoDate > datetime.datetime.now()]
    latest_ipos = [i for i in ipos if i.ipoDate <= datetime.datetime.now()][:20]
    
    table = [['Symbol','Name','CEO','Description','sharedOutstanding','proposedSharePriceLow','proposedSharePriceHigh']]
    for u in upcomming:
        table.append([u.id, u.companyName, u.ipoDate, u.ceo, u.decription, u.sharedOutstanding, u.proposedSharePriceLow, u.proposedSharePriceHigh])
    
    res = {
        'upcomming':[i for i in ipos if i.ipoDate > datetime.datetime.now()],
        'latest_performance':[i for i in ipos if i.ipoDate <= datetime.datetime.now()][:20]
    }

    pass

import statistics
def detect_anomalies():
    statistic.stdev([1,2,3,4,5])
    statistic.mean([1,2,3,4,5])

    # print(statistics.median_grouped([1, 2, 3, 4], 2))
    # TODO: run on all priority 2 stock and calculate it's change standard diviation, day range standard diviation, then calculate for each day the z-score
    # Use time window of 50 days
    # if z-score is > 0.9 then it's an anomaly
    pass

def magic_formula_rank():
    # https://en.wikipedia.org/wiki/Magic_formula_investing
    pass

def gainers_and_losers_by_posts(time_frame = 5, limit = 30, sort_by = 'ups'):
    data = queries.get_aggregate_post_metrics(time_frame)
    table = [['Symbol','Name','Industry','Post Count','Ups','Downs','Awards','Comments','Positives','Negatives','Unknowns','% Change']]

    sorted_data = {k: v for k, v in sorted(data.items(), key=lambda item: item[1][sort_by], reverse=True)}

    for tpl in list(sorted_data.items())[:limit]:
        symbolId = tpl[0]
        metrics = tpl[1]
        table.append([symbolId, metrics['symbol'].name, 
        metrics['symbol'].industry, 
        metrics['count'], 
        metrics['ups'], 
        metrics['downs'], 
        metrics['awards'], 
        metrics['comments'], 
        metrics['positives'], 
        metrics['negatives'], 
        metrics['unknowns'],
        cli.percent((metrics['records'][0].close / metrics['records'][time_frame - 1].close) - 1) if metrics['records'] is not None else '-'])

    cli.print_table(table, title=f'Gainers and losers by posts order by {sort_by.capitalize()}')

    return sorted_data

def gainers_and_losers(priorities = [2,1], time_frames = [1,2,4,9], sort_by_time_frame = 4, limit = 20):
    results = []
    gainers = [['Symbol','Name','Industry', "Current/Min", "Current/Max", *[f'{tf}D%' for tf in time_frames]]]
    losers = [['Symbol','Name','Industry', "Current/Min", "Current/Max", *[f'{tf}D%' for tf in time_frames]]]

    symbols = []
    for p in priorities:
        symbols += queries.get_symbols_by_priority(p)
    
    all_records = queries.get_all_latest_records(max(time_frames) + 1)
    for item in symbols:
        if item.id in all_records.keys():
            records = all_records[item.id] #queries.get_lastest_records(item.id, max(time_frames)) #reverse order, newer record are prior to older records
            if len(records) == max(time_frames) + 1:
                item = {
                    'symbolId':item.id,
                    'name':item.name,
                    'sector':item.sector,
                    'industry':item.industry
                }

                for tf in time_frames:
                    item[f'{tf}dayChange'] = round(((records[0].close / records[tf].close) - 1) * 100, 2)
                
                item[f'min'] = min([i.low for i in records])
                item[f'max'] = max([i.high for i in records])
                item[f'currentToMin'] = cli.percent((records[0].close / item[f'min']) - 1)
                item[f'currentToMax'] = cli.percent((records[0].close / item[f'max']) - 1)
                
                results.append(item)

            else:
                cli.danger('Macro', f'{item.id} get only {len(records)} and not {max(time_frames)}')

    results.sort(key=lambda i: i[f'{sort_by_time_frame}dayChange'], reverse=True)

    gainers += [[item['symbolId'], item['name'], item['industry'], item[f'currentToMin'], item[f'currentToMax'], *[item[f'{c}dayChange'] for c in time_frames]] for item in results[:limit]]
    losers += [[item['symbolId'], item['name'], item['industry'], item[f'currentToMin'], item[f'currentToMax'], *[item[f'{c}dayChange'] for c in time_frames]] for item in results[::-1][:limit]]

    cli.print_table(gainers, title=f'Top Gainers (Order By {sort_by_time_frame} Day % Change)', max_cell_length=20)
    cli.print_table(losers, title=f'Top Losers (Order By {sort_by_time_frame} Day % Change)', max_cell_length=20)

    return {
        'gainers': results[:limit],
        'losers': results[::-1][:limit]
    }

def get_gainer_and_losers_by_articles(from_date = datetime.datetime.now(), days_ago = 5, limit = 20):
    results = []
    positive = [['Symbol','Name','Sector','Industry','Positive','Negative','Unknown','Total']]
    negative = [['Symbol','Name','Sector','Industry','Positive','Negative','Unknown','Total']]
    unknown = [['Symbol','Name','Sector','Industry','Positive','Negative','Unknown','Total']]
    total = [['Symbol','Name','Sector','Industry','Positive','Negative','Unknown','Total']]
    
    symbols = {item.id:item for item in (queries.get_symbols_by_priority(2) + queries.get_symbols_by_priority(1))}
    grouped = {key:value for key,value in queries.get_all_articles_grouped().items() if key in symbols.keys()}

    for symbolId, articles in grouped.items():
        item = {
            'symbolId':symbolId,
            'name':symbols[symbolId].name,
            'sector':symbols[symbolId].sector,
            'industry':symbols[symbolId].industry,
            'positiveCount': len([a for a in articles if a.publishedAt >= (from_date - datetime.timedelta(days = days_ago)) and a.naiveSentiment == 'Positive']),
            'negativeCount': len([a for a in articles if a.publishedAt >= (from_date - datetime.timedelta(days = days_ago)) and a.naiveSentiment == 'Negative']),
            'unknownCount': len([a for a in articles if a.publishedAt >= (from_date - datetime.timedelta(days = days_ago)) and a.naiveSentiment == 'Unknown']),
            'totalCount': len([a for a in articles if a.publishedAt >= (from_date - datetime.timedelta(days = days_ago))])
        }
        
        results.append(item)

    results.sort(key=lambda i: i[f'positiveCount'], reverse=True)
    positive += [[item['symbolId'], item['name'], item['sector'], item['industry'], item['positiveCount'], item['negativeCount'], item['unknownCount'], item['totalCount'] ] for item in results[:limit]]
    cli.print_table(positive, title=f'Most Positive Media Coverage By Articles Count')

    results.sort(key=lambda i: i[f'negativeCount'], reverse=True)
    negative += [[item['symbolId'], item['name'], item['sector'], item['industry'], item['positiveCount'], item['negativeCount'], item['unknownCount'], item['totalCount'] ] for item in results[:limit]]
    cli.print_table(negative, title=f'Most Negative Media Coverage By Articles Count')

    results.sort(key=lambda i: i[f'unknownCount'], reverse=True)
    unknown += [[item['symbolId'], item['name'], item['sector'], item['industry'], item['positiveCount'], item['negativeCount'], item['unknownCount'], item['totalCount'] ] for item in results[:limit]]
    cli.print_table(unknown, title=f'Most Unknown Media Coverage By Articles Count')

    results.sort(key=lambda i: i[f'unknownCount'] + i[f'negativeCount'] + i[f'positiveCount'], reverse=True)
    total += [[item['symbolId'], item['name'], item['sector'], item['industry'], item['positiveCount'], item['negativeCount'], item['unknownCount'], item['totalCount'] ] for item in results[:limit]]
    cli.print_table(total, title=f'Most Media Coverage Of Any Type By Articles Count')

    return {
        'positive': sorted(results, key=lambda i: i[f'positiveCount'], reverse=True),
        'negative': sorted(results, key=lambda i: i[f'negativeCount'], reverse=True),
        'unknown': sorted(results, key=lambda i: i[f'unknownCount'], reverse=True),
        'whatevet': sorted(results, key=lambda i: i[f'unknownCount'] + i[f'negativeCount'] + i[f'positiveCount'], reverse=True)
    }

def get_sector_and_industry_performance(from_date = datetime.datetime.now(), days_ago = 5):

    sectors_table = [['Sector','Average % Return','Symbol Count']]
    industries_table = [['Industry','Average % Return','Symbol Count']]

    sectors = []
    industries = []

    all_symbols = [i for i in (queries.get_symbols_by_priority(2) + queries.get_symbols_by_priority(1)) if i.sector is not None and i.industry is not None]
    records = queries.get_all_latest_records(days_ago)
    
    all_symbols.sort(key = lambda s: s.sector)
    for sector, group in itertools.groupby(all_symbols, lambda s: s.sector):
        returns = []
        for symbol in list(group):
            if symbol.id in records.keys() and len(records[symbol.id]) == days_ago:
                symbol_return = ((records[symbol.id][0].close / records[symbol.id][-1].close) - 1) * 100
                returns.append(symbol_return)
        if len(returns) > 0:
            sectors.append({
                'name': sector,
                'return': round(sum(returns) / len(returns), 2),
                'count': len(returns)
            })

    sectors.sort(key = lambda s: s['return'], reverse=True)
    sectors_table += [[i['name'], i['return'], i['count']] for i in sectors]
    cli.print_table(sectors_table, title=f'Sector Returns', max_cell_length=200)

    all_symbols.sort(key = lambda s: s.industry)
    for industry, group in itertools.groupby(all_symbols, lambda s: s.industry):
        returns = []
        for symbol in list(group):
            if symbol.id in records.keys() and len(records[symbol.id]) == days_ago:
                symbol_return = ((records[symbol.id][0].close / records[symbol.id][-1].close) - 1) * 100
                returns.append(symbol_return)
        
        if len(returns) > 0:
            industries.append({
                'name': industry,
                'return': round(sum(returns) / len(returns), 2),
                'count': len(returns)
            })

    industries.sort(key = lambda s: s['return'], reverse=True)
    industries_table += [[i['name'], i['return'], i['count']] for i in industries]
    cli.print_table(industries_table, title=f'Industry Returns', max_cell_length=200)

    return {
        'sector':sectors,
        'industry':industries
    }

def get_etf_performance(from_date = datetime.datetime.now(), time_frames = [1,2,4,9], sort_by_time_frame = 4):
    etfs = [i for i in queries.get_symbols_by_priority(1) if i.assetType == 'ETF']
    records = {}
    res = []
    table = [['SymbolId', 'Name', *[f'{tf} Days Return' for tf in time_frames]]]
    
    for etf in etfs:
        records = queries.get_lastest_records(etf.id,max(time_frames) + 1)
        item = {
            'name': etf.name,
            'symbolId':etf.id,
        }

        for tf in time_frames:
            item[f'{tf} Days Return'] = round(((records[0].close / records[tf].close) - 1) * 100, 2) 
        
        res.append(item)
    
    res.sort(key = lambda a: a[f'{sort_by_time_frame} Days Return'], reverse = True)

    table += [[row['symbolId'], row['name'], *[row[f'{tf} Days Return'] for tf in time_frames]] for row in res]
    cli.print_table(table, "ETF Return")

    return res

def get_symbols_by_analyst_recommendations(from_date=datetime.datetime(datetime.datetime.now().year - 1, 1, 1), to_date = datetime.datetime.now(), table_length = 20):
    all_recommendations = queries.get_all_analyst_recommendations(from_date=from_date, to_date=to_date)
    all_records = queries.get_all_records(reverse=True)

    change_days = [5,20,50,100,150,200]
    
    gainers_table = [['Symbol','Name','Sector','Industry','Buys','Neutrals','Sells','Total', *[f'{d}D %' for d in change_days]]]
    neutrals_table = [['Symbol','Name','Sector','Industry','Buys','Neutrals','Sells','Total', *[f'{d}D %' for d in change_days]]]
    losers_table = [['Symbol','Name','Sector','Industry','Buys','Neutrals','Sells','Total', *[f'{d}D %' for d in change_days]]]

    res = []
    for symbolId, recommendations in all_recommendations.items():
        item = {
            'symbolId': symbolId,
            'name':recommendations[0].symbol.name,
            'sector': recommendations[0].symbol.sector,
            'industry': recommendations[0].symbol.industry,
            'buys': sum([1 for r in recommendations if r.toGrade in ['Overweight','Buy','Outperform'] and r.gradeAction in ['up','init','main']]),
            'sells': sum([1 for r in recommendations if r.toGrade in ['Sell','Underperform'] and r.gradeAction in ['down','init','main']]),
            'neutrals': sum([1 for r in recommendations if r.toGrade in ['Equal-Weight','Neutral','Hold'] and r.gradeAction in ['down','up','init','main']]),
            'total': len(recommendations)
        }

        for d in change_days:
            item[f'{d} Days Change'] = cli.percent((all_records[symbolId][0].close / all_records[symbolId][d].close) - 1) if symbolId in all_records.keys() and len(all_records[symbolId]) >= d else 'N/A'
        
        res.append(item)

    res.sort(key=lambda i: i['buys'], reverse=True)
    gainers_table += [[i['symbolId'], i['name'], i['sector'], i['industry'], i['buys'], i['neutrals'], i['sells'], i['total'], *[i[f'{d} Days Change'] for d in change_days]] for i in res[:table_length]]

    res.sort(key=lambda i: i['neutrals'], reverse=True)
    neutrals_table += [[i['symbolId'], i['name'], i['sector'], i['industry'], i['buys'], i['neutrals'], i['sells'], i['total'], *[i[f'{d} Days Change'] for d in change_days]] for i in res[:table_length]]

    res.sort(key=lambda i: i['sells'], reverse=True)
    losers_table += [[i['symbolId'], i['name'], i['sector'], i['industry'], i['buys'], i['neutrals'], i['sells'], i['total'] ,*[i[f'{d} Days Change'] for d in change_days]] for i in res[:table_length]] 

    cli.print_table(gainers_table, title=f'Most Buy Recommended By Analyst')
    cli.print_table(neutrals_table, title=f'Most Neutral Recommended By Analyst')
    cli.print_table(losers_table, title=f'Most Sell Recommended By Analyst')
    
    return {
        'gainers': gainers_table,
        'neutrals': neutrals_table,
        'losers': losers_table
    }

def get_symbols_by_next_dividend_yield(table_length = 20):
    all_dividends = queries.get_all_dividends()
    all_records = queries.get_all_records(reverse=True)

    next_table = [['Symbol','Name','Sector','Industry','Next Ex-Date','Next Payment','Next Dividend Yield', 'Annual Dividends', 'Annual Yield']]
    annual_table = [['Symbol','Name','Sector','Industry','Next Ex-Date','Next Payment','Next Dividend Yield', 'Annual Dividends', 'Annual Yield']]

    res = []

    for symbolId, dividends in all_dividends.items():
        future_dividend = [d for d in dividends if d.exDate > datetime.datetime.now()]
        annual_payments = sum([d.payment for d in dividends if d.exDate > (datetime.datetime.now() - datetime.timedelta(days = 365))])

        item = {
            'symbolId':symbolId,
            'name':dividends[0].symbol.name,
            'sector':dividends[0].symbol.sector,
            'industry':dividends[0].symbol.industry,
            'nextExDate':future_dividend[0].exDate.strftime('%Y-%m-%d') if len(future_dividend) > 0 else 'N/A',
            'nextPayment': future_dividend[0].payment if len(future_dividend) > 0 else 'N/A',
            'nextYield': future_dividend[0].payment / all_records[symbolId][0].close if len(future_dividend) > 0 else 'N/A',
            'annualDividends': dividends[0].symbol.dividendPaymentsPerYear,
            'annualYield': annual_payments / all_records[symbolId][0].close,
        }

        res.append(item)

    res.sort(key=lambda i: i['annualYield'], reverse=True)
    annual_table += [[i['symbolId'], i['name'], i['sector'], i['industry'], i['nextExDate'], i['nextPayment'], cli.percent(i['nextYield']), i['annualDividends'], cli.percent(i['annualYield'])] for i in res[:table_length]] 
    cli.print_table(annual_table, title=f'Stocks by Annual Dividend Yield')

    next_dividends =  sorted([d for d in res if d['nextYield'] != 'N/A'], key=lambda i: i['nextYield'], reverse=True)
    next_table += [[i['symbolId'], i['name'], i['sector'], i['industry'], i['nextExDate'], i['nextPayment'], cli.percent(i['nextYield']), i['annualDividends'], cli.percent(i['annualYield'])] for i in next_dividends[:table_length]] 
    cli.print_table(next_table, title=f'Stocks by Next Dividend Yield')

    return res