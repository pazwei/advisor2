#!/usr/bin/env python3

from xml.etree import ElementTree as etree
import json
import base64
import io
import pprint
import datetime
import itertools

import requests
import bs4 as bs

import pandas as pd
import pandas_ta as ta # https://github.com/twopirllc/pandas-ta
import numpy as np

import db.queries as queries
import logic.charts as charts
import utils.cli as cli

# __growth(1,rows,'totalAssets')
__growth = lambda arg: ((getattr(arg[1][arg[0]], arg[2]) - getattr(arg[1][arg[0] + 1], arg[2])) / getattr(arg[1][arg[0] + 1], arg[2]) if arg[0] < len(arg[1]) - 1 else 1)

# https://www.gurufocus.com/stock/QCOM/summary
# https://www.investopedia.com/terms/g/graham-number.asp
# F-Score piotroski
# Z-Score altman
# m-Score beneish
# https://www.dividata.com/
# https://en.wikipedia.org/wiki/Magic_formula_investing
# https://en.wikipedia.org/wiki/Beneish_M-score#:~:text=The%20formula%20to%20calculate%20the,%C3%97%20TATA%20%E2%88%92%200.327%20%C3%97%20LVGI
# https://www.google.com/search?q=working+capital+formula&oq=working+capital+&aqs=chrome.2.69i57j0i67l3j0j0i67j0i20i263l2j0i67j0.4197j0j7&sourceid=chrome&ie=UTF-8


def get_latest_reports(symbolId, from_date = datetime.datetime.now(), terms = 8, type = 'QUATER', divider = 1000000):
    #Balance sheet
    bs = get_last_4_quaters_balance_sheets(symbolId, from_date, terms)
    bs_rows = [['Field',*[d.fiscalDateEnding.strftime('%Y-%m-%d') for d in bs]]]
    bs_fields = ['totalAssets','intangibleAssets','totalLiabilities','totalShareholderEquity','cash','longTermDebt','shortTermDebt','totalLongTermDebt','currentLongTermDebt','commonStock']
    # bs_fields = [f for f in dir(bs[0]) if type(getattr(bs[0], f)).__name__ in ['float','int']]
    for f in bs_fields:
        bs_rows.append([cli.text_from_camel_case(f),*[cli.decimal_reduce(getattr(item,f), divider = divider) if getattr(item,f) is not None else 'N/A' for item in bs]])

    #Financial Ratios
    bs_rows.append(['-DIVIDER-'])
    
    bs_rows.append(['Total Debt / Total Assets',*[cli.formula(item, lambda i: (i.longTermDebt + i.shortTermDebt) / i.totalAssets) for item in bs]])
    bs_rows.append(['(Current LT Debt + ST Debt) / Cash',*[cli.formula(item, lambda i: (i.currentLongTermDebt + i.shortTermDebt) / i.cash) for item in bs]])
    bs_rows.append(['Equity / Share',*[cli.formula(item, lambda i: i.totalShareholderEquity / i.commonStock, output_as = '$') for item in bs]])
    bs_rows.append(['Total Assets / Share',*[cli.formula(item, lambda i: i.totalAssets / i.commonStock, output_as='$') for item in bs]])
    bs_rows.append(['Equity / Total Assets',*[cli.formula(item, lambda i: i.totalShareholderEquity / i.totalAssets, output_as='decimal') for item in bs]])
    bs_rows.append(['Intangible to total assets',*[cli.formula(item, lambda i: i.intangibleAssets / i.totalAssets) for item in bs]])
    
    bs_rows.append(['LT Debt Growth',*[cli.formula((index, bs,'totalLongTermDebt'), __growth) for index,item in enumerate(bs)]])
    bs_rows.append(['Equity Growth',*[cli.formula((index, bs,'totalShareholderEquity'), __growth) for index,item in enumerate(bs)]])
    bs_rows.append(['Assets Growth',*[cli.formula((index, bs,'totalAssets'), __growth) for index,item in enumerate(bs)]])
    bs_rows.append(['Common Stock Growth',*[cli.formula((index, bs,'commonStock'), __growth) for index,item in enumerate(bs)]])

    cli.print_table(bs_rows, f'{symbolId} Balance Sheets (1 = {divider})', show_row_number = False, show_total = False, max_cell_length = 40)

    #Income Statement
    income = get_last_4_quaters_income_statements(symbolId, from_date, terms)
    income_rows = [['Field',*[d.fiscalDateEnding.strftime('%Y-%m-%d') for d in income]]]
    income_fields = ['totalRevenue','costOfRevenue','grossProfit','totalOperatingExpense','researchAndDevelopment','operatingIncome','ebit','netIncome','netIncomeApplicableToCommonShares']

    for f in income_fields:
        income_rows.append([cli.text_from_camel_case(f),*[cli.decimal_reduce(getattr(item,f), divider = divider) if getattr(item,f) is not None else 'N/A' for item in income]])
    
    #Financial Ratios
    income_rows.append(['-DIVIDER-'])
    income_rows.append(['Gross margin',*[cli.formula(item, lambda i: i.grossProfit / i.totalRevenue) for item in income]])
    income_rows.append(['Operating margin',*[cli.formula(item, lambda i: (item.grossProfit - item.totalOperatingExpense) / item.totalRevenue) for item in income]])
    income_rows.append(['Net margin',*[cli.formula(item, lambda i: i.netIncomeApplicableToCommonShares / i.totalRevenue) for item in income]])

    cli.print_table(income_rows, f'{symbolId} Income Statements (1 = {divider})', show_row_number = False, show_total = False, max_cell_length = 40)

    #Cashflow
    cf = get_last_4_quaters_cafhflows(symbolId, from_date, terms)
    cf_rows = [['Field',*[d.fiscalDateEnding.strftime('%Y-%m-%d') for d in cf]]]
    cf_fields = ['netIncome','changeInCashAndCashEquivalents','stockSaleAndPurchase','cashflowFromFinancing','dividendPayout','capitalExpenditures']

    for f in cf_fields:
        cf_rows.append([cli.text_from_camel_case(f), *[cli.decimal_reduce(getattr(item,f), divider = divider) if getattr(item,f) is not None else 'N/A' for item in cf]])

    #Financial Ratios
    cf_rows.append(['-DIVIDER-'])
    cf_rows.append(['CAPEX / Net Income',*[cli.formula(item, lambda i: i.capitalExpenditures / i.netIncome) for item in cf]])
    cf_rows.append(['Dividend / Net Income',*[cli.formula(item, lambda i: i.dividendPayout / i.netIncome) for item in cf]])

    cli.print_table(cf_rows, f'{symbolId} Cash Flows (1 = {divider})', show_row_number = False, show_total = False, max_cell_length = 40)

def get_earnings(symbolId):
    earnings = queries.get_earnings(symbolId)
    table = [['SymbolId', 'Fiscal End Date', 'Report Date', 'Term', 'EPS', 'Estimated EPS', 'EPS Surprise', 'EPS % Suprise']]
    for e in earnings:
        table.append([symbolId, e.fiscalDateEnding.strftime('%Y-%m-%d'), e.reportedDate.strftime('%Y-%m-%d'), e.term, e.reportedEPS, e.estimatedEPS, e.surprise, f'{round(e.surprisePercentage, 2)}%' if e.surprisePercentage is not None else '-'])
    cli.print_table(table, title=f'Earnings for {symbolId}')
    
    return earnings

def get_dividend_history(symbolId):
    dividends = queries.get_dividends(symbolId)
    table = [['SymbolId','Ex-Date','Payment','Payment Date']]

    for d in dividends:
        table.append([symbolId, d.exDate.strftime('%Y-%m-%d') , d.payment, d.paymentDate.strftime('%Y-%m-%d')])

    cli.print_table(table, title = f'Dividend History for {symbolId}')

    return dividends

# https://stackoverflow.com/questions/57006437/calculate-rsi-indicator-from-pandas-dataframe/57037866
def rma(x, n, y0): #running moving average
    a = (n-1) / n
    ak = a**np.arange(len(x)-1, -1, -1)
    return np.append(y0, np.cumsum(ak * x) / ak / n + y0 * a**np.arange(1, len(x)+1))

def test_calculate_rsi():
    df = pd.DataFrame({'close':[4724.89, 4378.51,6463.00,9838.96,13716.36,10285.10,
                          10326.76,6923.91,9246.01,7485.01,6390.07,7730.93,
                          7011.21,6626.57,6371.93,4041.32,3702.90,3434.10,
                          3813.69,4103.95,5320.81,8555.00,10854.10]})

    return calculate_rsi(df, 'close')
    
def calculate_rsi(df, data_column = 'close', n = 14):
    df['change'] = df[data_column].diff()
    df['gain'] = df.change.mask(df.change < 0, 0.0)
    df['loss'] = -df.change.mask(df.change > 0, -0.0)
    df.loc[n:,'avg_gain'] = rma(df.gain[n+1:].values, n, df.loc[:n, 'gain'].mean())
    df.loc[n:,'avg_loss'] = rma(df.loss[n+1:].values, n, df.loc[:n, 'loss'].mean())
    df['rs'] = df.avg_gain / df.avg_loss

    df[f'rsi_{data_column}_{n}'] = 100 - (100 / (1 + df.rs))
    df = df.drop(columns=['change', 'gain', 'loss', 'avg_loss', 'avg_gain','rs']) #house keeping

    return df

def claculate_money_flow_index(df, n=14):
    # https://corporatefinanceinstitute.com/resources/knowledge/trading-investing/money-flow-index/
    df['typicalPrice'] = (df['high'] + df['low'] + df['close']) / 3
    df['rawMoneyFlow'] = df['typicalPrice'] * df['volume']
    df['rawMoneyFlowChange'] = df['rawMoneyFlow'].diff()

    df['moneyRatioLoss'] = -df.rawMoneyFlowChange.mask(df.rawMoneyFlowChange > 0, -0.0)
    df['moneyRatioGain'] = df.rawMoneyFlowChange.mask(df.rawMoneyFlowChange < 0, 0.0) 
    df['moneyRatio'] = df['moneyRatioGain'].rolling(n).sum() / df['moneyRatioLoss'].rolling(n).sum() #Money Ratio = 14-period Positive Money Flow / 14-period Negative Money Flow
    
    df[f'moneyFlowIndex_{n}'] = 100 - (100 / (1 + df['moneyRatio']))

    df = df.drop(columns=['typicalPrice', 'rawMoneyFlow', 'moneyRatioGain', 'rawMoneyFlowChange', 'moneyRatioLoss', 'moneyRatio']) #house keeping

    return df

def calculate_aroon(df, n=25):
    #https://www.investopedia.com/terms/a/aroonoscillator.asp#:~:text=Calculate%20Aroon%20Up%20by%20finding,the%20last%2025%2Dperiod%20low.
    pass

def moving_average(df, column, window, new_column_name):
    df[new_column_name] = df[column].rolling(window=window).mean()
    return df

def moving_standard_diviation(df, column, window, new_column_name):
    df[new_column_name] = df[column].rolling(window=window).std()
    return df

def objects_to_df(arr):
    variables = arr[0].keys()
    df = pd.DataFrame([[getattr(i,j) for j in variables] for i in arr], columns = variables)
    return df

def plt_to_base64(plt):
    pic_IObytes = io.BytesIO()
    plt.savefig(pic_IObytes,  format='png')
    pic_IObytes.seek(0)
    pic_hash = base64.b64encode(pic_IObytes.read())
    return pic_hash
    
def aggregate(items, fields = None):
    """
    sum all the float and int fields provided and returns a dictionary with the summed values
    """
    if fields == None:
        fields = [f for f in dir(items[0]) if (type(getattr(items[0],f)) == float or type(getattr(items[0],f)) == int or type(getattr(items[0],f)) == None) and f.startswith('_') == False and f != 'id']

    res = {field: sum([getattr(item, field, 0) for item in items if getattr(item, field) is not None]) for field in fields}
    # res = {field: sum([getattr(item, field, 0) if getattr(item, field, 0) is not None else 0 for item in items]) for field in fields}
    return res

# this act will basically load all DB into memory, but who cares, when memory is cheap resource [as long as chrome is not running :-)]
__quater_balance_sheets = {}
__quater_income_statements = {}
__quater_cashflows = {}
__quater_earnings = {}
__records = {} #could get very big, 100mb++
__dividends = {}
__articles = queries.get_all_articles_grouped()

def get_articles(symbol_id, from_date, to_date = datetime.datetime.now()):
    global __articles
    if len(__articles.keys()) == 0:
        __articles = queries.get_all_articles_grouped()
    res = [a for a in __articles[symbol_id] if (a.publishedAt >= from_date and a.publishedAt <= to_date)]
    return res

def get_next_dividends(symbolId, from_date = datetime.datetime.now()):
    global __dividends
    if len(__dividends.keys()) == 0:
        __dividends = queries.get_all_dividends()
    if symbolId in __dividends.keys():
        # return next(d for d in __dividends[symbolId] if d.exDate >= from_date)
        next_dividends = [d for d in __dividends[symbolId] if d.exDate >= from_date and d.payment > 0]
        return __dividends[symbolId][-1] if len(next_dividends) == 0 else next_dividends[0] #return last dividend if there is no future one published
    return None

def get_records(symbol_id, from_date, to_date = datetime.datetime.now()):
    global __records
    if len(__records.keys()) == 0:
        __records = queries.get_all_records()

    records = [r for r in __records[symbol_id] if r.date >= from_date and r.date <= to_date]
    records.sort(key=lambda r: r.date, reverse=False)
    return records
    
def get_last_4_quaters_balance_sheets(symbolId, record_date = datetime.datetime.now(), terms = 4, term_type = 'QUATER'):
    global __quater_balance_sheets
    if len(__quater_balance_sheets.keys()) == 0:
        __quater_balance_sheets = queries.get_all_balance_sheets(term_type)
    return sorted([item for item in __quater_balance_sheets[symbolId] if item.fiscalDateEnding <= record_date], key=lambda a: a.fiscalDateEnding, reverse=True)[:terms]

def get_last_quater_balance_sheet(symbolId, record_date = datetime.datetime.now(), term_type = 'QUATER'):
    global __quater_balance_sheets
    if len(__quater_balance_sheets.keys()) == 0:
        __quater_balance_sheets = queries.get_all_balance_sheets(term_type)
    return sorted([item for item in __quater_balance_sheets[symbolId] if item.fiscalDateEnding <= record_date], key=lambda a: a.fiscalDateEnding, reverse=True)[0]

def get_last_4_quaters_cafhflows(symbolId, record_date = datetime.datetime.now(), terms = 4, term_type = 'QUATER'):
    global __quater_cashflows
    if len(__quater_cashflows.keys()) == 0:
        __quater_cashflows = queries.get_all_cashflows(term_type)
    return sorted([item for item in __quater_cashflows[symbolId] if item.fiscalDateEnding <= record_date], key=lambda a: a.fiscalDateEnding, reverse=True)[:terms]

def get_last_4_quaters_income_statements(symbolId, record_date = datetime.datetime.now(), terms = 4, term_type = 'QUATER'):
    global __quater_income_statements
    if len(__quater_income_statements.keys()) == 0:
        __quater_income_statements = queries.get_all_income_statements(term_type)
    return sorted([item for item in __quater_income_statements[symbolId] if item.fiscalDateEnding <= record_date], key=lambda a: a.fiscalDateEnding, reverse=True)[:terms]

def get_last_4_quaters_earnings(symbolId, record_date = datetime.datetime.now(), terms = 4):
    global __quater_earnings
    if len(__quater_earnings.keys()) == 0:
        __quater_earnings = queries.get_all_earnings('QUATER')
    return sorted([item for item in __quater_earnings[symbolId] if item.fiscalDateEnding <= record_date], key=lambda a: a.fiscalDateEnding, reverse=True)[:terms] 

def market_view(date = None):
    """
    Read all the digest from symbol and create large df where the index is the symbol and the data comparable between all symbols + there
    is updated last digest date

    """
    date = date if date is not None else datetime.datetime.now() + datetime.timedelta(days = -1)
    pass

def count_articles(symbolId, date, days_ago, sentiments = ['Positive','Negative','Unknown']):
    articles = get_articles(symbolId, date + datetime.timedelta(days = -1 * days_ago), date)
    return len([a for a in articles if a.naiveSentiment in sentiments])

def next_dividend_yield(symbolId, date, price):
    next_dividends = get_next_dividends(symbolId, date)
    res = 0.0
    if next_dividends is not None:
        res = (100 * next_dividends.payment) / price
    else:
        res = 0.0
    return res


# TODO: make digest more efficient - not n^n
def add_financial_data(symbolId, df):
    
    aggregate(get_last_4_quaters_income_statements(symbolId, d))
    
    
    aggregate(get_last_4_quaters_cafhflows(symbol_id, d))
    
    
    get_last_quater_balance_sheet(symbol_id, d)
    #Optimize the df creation process, try to convert n^2 to n 
    return df

def digest(symbol_id, from_date, to_date = datetime.datetime.now()):
    """
    Calculate all the metrics for historical prices and buying signals. 
    Save the tail (last day) in the digest field of the symbol
    """
    #get all relevant records
    records = get_records(symbol_id, from_date, to_date)
    
    #create df out of list of objects
    record_fields = [i for i in dir(records[0]) if not i.startswith('_')]
    df = pd.DataFrame([[getattr(i,j) if j in record_fields else 0.0 for j in record_fields] for i in records], columns = record_fields) #can add columns that are not in object fields
     

    df['price5High'] = df['high'].rolling(200).max()
    df['price5Low'] = df['low'].rolling(200).min()
    df['price20dHigh'] = df['high'].rolling(200).max()
    df['price20Low'] = df['low'].rolling(200).min()
    df['price60dHigh'] = df['high'].rolling(200).max()
    df['price60Low'] = df['low'].rolling(200).min()
    df['price200dHigh'] = df['high'].rolling(200).max()
    df['price200Low'] = df['low'].rolling(200).min()

    df['price1dChange'] = df['close'].pct_change(periods=1)
    df['price2dChange'] = df['close'].pct_change(periods=2)
    df['price5dChange'] = df['close'].pct_change(periods=5)
    df['price10dChange'] = df['close'].pct_change(periods=10)
    df['price20dChange'] = df['close'].pct_change(periods=20)
    df['price60dChange'] = df['close'].pct_change(periods=60)
    df['price200dChange'] = df['close'].pct_change(periods=200)

    df['price3dMovingAverage'] = df['close'].rolling(3).mean()
    df['price5dMovingAverage'] = df['close'].rolling(5).mean()
    df['price10dMovingAverage'] = df['close'].rolling(10).mean()
    df['price20dMovingAverage'] = df['close'].rolling(20).mean()
    df['price20dMovingAverage'] = df['close'].rolling(60).mean()
    df['price200dMovingAverage'] = df['close'].rolling(200).mean()

    #invent some metric to find the rate of change, I want to invest on linear growth not single pop or convex

    df['price5dStandardDiviation'] = df['close'].rolling(5).std()
    df['price10dStandardDiviation'] = df['close'].rolling(10).std()
    df['price20dStandardDiviation'] = df['close'].rolling(20).std()
    df['price20dStandardDiviation'] = df['close'].rolling(60).std()
    df['price200StandardDiviation'] = df['close'].rolling(200).std()

    df['volume1dChange'] = df['volume'].pct_change(periods=1)
    df['volume2Changed'] = df['volume'].pct_change(periods=2)
    df['volume5Changed'] = df['volume'].pct_change(periods=5)
    df['volume20Changed'] = df['volume'].pct_change(periods=20)
    df['volume60Changed'] = df['volume'].pct_change(periods=60)
    df['volume200Changed'] = df['volume'].pct_change(periods=200)

    df['volume3dMovingAverage'] = df['volume'].rolling(3).mean()
    df['volume5dMovingAverage'] = df['volume'].rolling(5).mean()
    df['volume20dMovingAverage'] = df['volume'].rolling(20).mean()
    df['volume60dMovingAverage'] = df['volume'].rolling(60).mean()
    df['volume200dMovingAverage'] = df['volume'].rolling(200).mean()

    df['volume5dStandardDiviation'] = df['volume'].rolling(5).std()
    df['volume20dStandardDiviation'] = df['volume'].rolling(20).std()
    df['volume60dStandardDiviation'] = df['volume'].rolling(60).std()
    df['volume200StandardDiviation'] = df['volume'].rolling(200).std()
    
    df['articles5dCount'] = [count_articles(symbol_id, d, 5) for d in df['date']]
    df['articles20dCount'] = [count_articles(symbol_id, d, 20) for d in df['date']]
    df['articles200dCount'] = [count_articles(symbol_id, d, 200) for d in df['date']]

    df['positiveArticles5dCount'] = [count_articles(symbol_id, d, 5, ['Positive']) for d in df['date']]
    df['positiveArticles20dCount'] = [count_articles(symbol_id, d, 20, ['Positive']) for d in df['date']]
    df['positiveArticles200dCount'] = [count_articles(symbol_id, d, 200, ['Positive']) for d in df['date']]

    df['negativeArticles5dCount'] = [count_articles(symbol_id, d, 5, ['Negative']) for d in df['date']]
    df['negativeArticles20dCount'] = [count_articles(symbol_id, d, 20, ['Negative']) for d in df['date']]
    df['negativeArticles200dCount'] = [count_articles(symbol_id, d, 200, ['Negative']) for d in df['date']]

    df['unknownArticles5dCount'] = [count_articles(symbol_id, d, 5, ['Unknown']) for d in df['date']]
    df['unknownArticles20dCount'] = [count_articles(symbol_id, d, 20, ['Unknown']) for d in df['date']]
    df['unknownArticles200dCount'] = [count_articles(symbol_id, d, 200, ['Unknown']) for d in df['date']]

    df['sharesOutstanding'] = [get_last_quater_balance_sheet(symbol_id, d).commonStockSharesOutstanding for d in df['date']]
    df['marketcap'] = df['sharesOutstanding'] * df['close']
    
    df['totalRevenue'] = [aggregate(get_last_4_quaters_income_statements(symbol_id, d))['totalRevenue'] for d in df['date']]
    df['ebit'] = [aggregate(get_last_4_quaters_income_statements(symbol_id, d))['ebit'] for d in df['date']]
    df['netIncome'] = [aggregate(get_last_4_quaters_income_statements(symbol_id, d))['netIncome'] for d in df['date']]
    df['incomeTaxExpense'] = [aggregate(get_last_4_quaters_income_statements(symbol_id, d))['incomeTaxExpense'] for d in df['date']]
    df['netProfit'] = df['netIncome'] - df['incomeTaxExpense']
    df['priceToEarnings'] = df['marketcap'] / df['netProfit']
    df['priceToRevenue'] = df['marketcap'] / df['totalRevenue']
    df['epsLast4Quaters'] = df['netIncome'] / df['sharesOutstanding']
    df['dividendYield'] = [next_dividend_yield(symbol_id, r['date'], r['close']) for i,r in df.iterrows()]

    #technical indicators
    df = calculate_rsi(df,'close',14)
    df = claculate_money_flow_index(df,14)

    df['totalAssets'] = [get_last_quater_balance_sheet(symbol_id, d).totalAssets for d in df['date']]
    df['totalLiabilities'] = [get_last_quater_balance_sheet(symbol_id, d).totalLiabilities for d in df['date']]
    df['intangibleAssets'] = [get_last_quater_balance_sheet(symbol_id, d).intangibleAssets for d in df['date']]
    df['bookValue'] = df['totalLiabilities'] - df['intangibleAssets'] - df['totalLiabilities'] # total assets minus intangible assets (patents, goodwill) and liabilities
    df['priceToBookValue'] = df['marketcap'] / df['bookValue']

    df['capitalExpenditures'] = [aggregate(get_last_4_quaters_cafhflows(symbol_id, d))['capitalExpenditures'] for d in df['date']]
    df['cashflowFromFinancing'] = [aggregate(get_last_4_quaters_cafhflows(symbol_id, d))['cashflowFromFinancing'] for d in df['date']]
    # df['dividendPayout'] = [aggregate(get_last_4_quaters_cafhflows(symbol_id, d))['dividendPayout'] for d in df['date']]
    
    #TODO:
    # df['epsGrowthY2Y'] = 0

    # df['revenueLast4Quaters'] = 0
    # df['revenueGrowthY2Y'] = 0
            
    # df['netProfitLast4Quaters'] = 0
    # df['netProfitGrowthY2Y'] = 0

    # df['investments'] = 0 #[aggregate(get_last_4_quaters_cafhflows(symbol_id, d))['investments'] for d in df['date']] #BUG: AAPL do not invest thus item[0] is none
    # df['depreciation'] = 0 #[aggregate(get_last_4_quaters_cafhflows(symbol_id, d))['depreciation'] for d in df['date']]

    # df['freeCashFlow'] = 0 #df['netincome'] + df['depriciation'] + df['cashflowFromInvestment'] + df['cashflowFromFinancing'] - df['dividendPayout'] - df['investment'] - df['capitalExpenditures']
    # df['freeCashflowGrowthY2Y'] = 0

    # df['ebitdaLast4Quaters'] = 0
    # df['ebitdaGrowthY2Y'] = 0
            
    # df['float'] = 0

    # df['forwardPe'] = 0

    
    # df['analystTragetPrice'] = 0
    # df['sharesFloat'] = 0
    # df['sharesShort'] = 0
    # df['shortRatio'] = 0

    # df['revenueLast4Quaters'] = 0
    # df['revenueGrowthLast4Quaters'] = 0
    # df['revenueGrowthLast8Quaters'] = 0
    # df['revenueGrowthLast12Quaters'] = 0

    # df['operatingProfitLast4Quaters'] = 0
    # df['operatingProfitGrowthLast4Quaters'] = 0
    # df['operatingProfitGrowthLast8Quaters'] = 0
    # df['operatingProfitGrowthLast12Quaters'] = 0

    # df['netProfitLast4Quaters'] = 0
    # df['netProfitGrowthLast4Quaters'] = 0
    # df['netProfitGrowthLast8Quaters'] = 0
    # df['netProfitGrowthLast12Quaters'] = 0

    # df['shortTermDebtLastQuater'] = 0
    # df['longTermDebtLastQuater'] = 0
    # df['shortTermToLongTermDebtLastQuater'] = 0
    # df['totalDebtLastQuater'] = 0
    # df['debtGrowthLast8Quaters'] = 0
    # df['debtGrowthLast12Quaters'] = 0

    # # df['totalAssetsLastQuater'] = 0
    # # df['totalAssetsGrowthLast8Quaters'] = 0
    # # df['totalAssetsGrowthLast12Quaters'] = 0

    # df['equityLastQuater'] = 0
    # df['equityGrowthLast8Quaters'] = 0
    # df['equityGrowthLast12Quaters'] = 0

    # df['freeCashflowLast4Quaters'] = 0
    # df['freeCashflowGrowthLast4Quaters'] = 0
    # df['freeCashflowGrowthLast8Quaters'] = 0
    # df['freeCashflowGrowthLast12Quaters'] = 0

    # df['revenuePerShareLast4Quaters'] = 0
    # df['profitMarginLast4Quaters'] = 0
    # df['operatingMarginLast4Quaters'] = 0
    # df['returnOnAssetsLast4Quaters'] = 0
    # df['returnOnEquityLast4Quaters'] = 0


    # df['pegRatio'] = 0

    # df['aroon'] = 0
    # df['aroon'] = 0

    df.set_index(pd.DatetimeIndex(df["date"]), inplace=True)

    return df

def chart(symbolId, from_date, to_date = datetime.datetime.now(), line_fields = ['close'], bar_fields=['volume'], output='native'):
    df = digest(symbolId, from_date, to_date)
    title = f'{symbolId} from {from_date.strftime("%Y-%m-%d")} to {to_date.strftime("%Y-%m-%d")}'
    img = charts.plot(df, title, line_fields = line_fields, bar_fields = bar_fields, output = output) # 'volume','volume20dMovingAverage','volume200dMovingAverage'
    return {'title':title, 'img':img}

def calculate_z_score():
    # https://en.wikipedia.org/wiki/Altman_Z-score
    pass

def calculate_f_score():
    # https://en.wikipedia.org/wiki/Piotroski_F-score
    pass

def calculate_m_score():
    # https://en.wikipedia.org/wiki/Beneish_M-score
    pass

if __name__ == '__main__':
    df = digest('MSFT',datetime.datetime(2021,1,1))
    # charts.plot_candle_stick(df)