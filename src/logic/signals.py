"""
In this file there will contain logic logic for finding buy price, sell price and stop loss prices.

Data:
1. Build a collection of 20-30 cow companies (growth < 10%, market share > 30%) and 20-30 stars companies like WIX (growth > 10%, market share < 30%). Include their peers also
2. For each company, find at least 3 sources of feeds
3. Track Gurus and other influencers via twitter 
4. Invest mostly in software companies (GOOG, MSFT, WIX) or industrial companies with advanced tech (NVIDIA, AAPL, INTC, AMD) or operation companies armed with advanced tech (AMZN, BABA). Tech in the form of software or chip design
5. Stay away from non tech driven compenies aspecially ones which rely on brand names 
6. Check data once a day at night
7. Run the app from Raspberry Pie

Theory:
1. Invest in tickers where their sentiment average is higher or trending upwards than the market's. Stay away from companies with lower than market sentiment.
2. List all the tickers where there is a steady rise in sentiment
3. For all tickers write the 5 most common keyword and correlate it with market's keywords


2. Use keyword analysis for finding opportunities
3. Check peer company sentiment / sector sentiment to find the leaders
4. Use technical analysis on companies with good business moats and sound financials. Don't fall for the value traps

1. track individual look for the sentiment
"""

def create_work_sheet(symbol):
    """
    """
    pass

buy_signals = []
sell_signal = []
stop_loss = []
price = []
symbols_filters = []

algorithm = {
    buy_signal:'',
    sell_signal:'',
    sell_price:'',
    stocks: symbols_filters('sector = Technology','sentiment_5_days_ma > ','[df[column]][operator][value]')
}

def symbols_filters(*args):
    pass
