#!/usr/bin/env python3
import datetime
import os
import uuid

import matplotlib.pyplot as plt
import mplfinance as mpf #https://github.com/matplotlib/mplfinance #TODO

# from matplotlib.finance import candlestick_ohlc
# import matplotlib.finance as plt_finance #obselete

import pandas as pd
import numpy as np
import matplotlib.mlab as mlab
import matplotlib as mpl
import matplotlib.dates as mdates
from matplotlib import gridspec
import mpld3 #enable interactive charts on jupyter notebook
# from mplfinance import candlestick_ohlc 
import matplotlib.dates as mpdates 

#https://www.tutorialspoint.com/matplotlib/matplotlib_bar_plot.htm

def plot(df, title, line_fields = [], bar_fields = [], index_field = 'date', pie_field = None, output='native', x_label = 'Date', y_label = 'Price', sub_plots=False): #, dark_mode = True
    """
    output could be:
    'notebook' (by calling from inside jupter)
    'telegram' for sending byte[] telegram response
    'file' for saving file locally
    'native' for displaying matplotlib native UI
    """
    
    chart_type = ''
    if pie_field is not None:
        chart_type = 'pie'
    elif len(line_fields) > 0 and len(bar_fields) > 0:
        chart_type = 'shared'
    elif len(line_fields) > 0:
        chart_type = 'lines'
    else:
        chart_type = 'bars'

    series = df.set_index(index_field)
    df.index = series.index

    if chart_type == 'lines':
        ts = pd.Series(df[index_field], index=df[index_field])
        df_lines = pd.DataFrame(df, index=ts.index, columns=line_fields)
        if index_field == 'date':
            df_lines.index = df.index.strftime('%Y-%m-%d')
        df_lines.plot.line(rot=90, title=title, ylabel = y_label, xlabel=x_label, figsize=(18, 8) if output == 'native' else (13, 8), subplots=sub_plots)

    if chart_type == 'bars':
        # https://matplotlib.org/stable/gallery/lines_bars_and_markers/barchart.html
        # https://www.tutorialspoint.com/matplotlib/matplotlib_bar_plot.htm
        # https://www.geeksforgeeks.org/create-a-grouped-bar-plot-in-matplotlib/
        # https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.plot.bar.html
        # https://stackoverflow.com/questions/60926723/changing-the-formatting-of-a-datetime-axis-in-matplotlib-bar-chart
        
        ts = pd.Series(df[index_field], index=df[index_field])
        df_bars = pd.DataFrame(df, index=ts.index, columns=bar_fields)
        if index_field == 'date':
            df_bars.index = df.index.strftime('%Y-%m-%d')
        df_bars.plot.bar(rot=90, title=title, ylabel = y_label, xlabel=x_label, figsize=(18, 10) if output == 'native' else (13, 10), subplots=sub_plots)

    if chart_type == 'pie':
        df.plot.pie(y=pie_field, figsize=(18, 8) if output == 'native' else (13, 8), title=title, ylabel = y_label, xlabel=x_label)
    
    if chart_type == 'shared':
        pass

    #output plt
    if output == 'native':
        plt.show()
        return None
    elif output == 'notebook':
        mpld3.display(plt.figure())
        # mpld3.show(plt.figure())
        return
    elif output == 'file':
        file_name = f"{str(uuid.uuid1()).replace('-','')}.png"
        script_dir = os.path.dirname(__file__)
        file_path = os.path.join(script_dir, '..', 'temp', file_name)
        plt.savefig(file_path)
        return file_path
    
def plot_two_graphs_shared_x_axis(ticker, df, top_chart_technical_indicators_to_plot, bottom_chart_field_dictionary, sub_plot_title = None ,sub_plot_y_title = None, output = 'native'):
    #https://stackoverflow.com/questions/37212779/how-to-get-an-intraday-price-volume-plot-in-pandas
    #https://mpld3.github.io/quickstart.html

    df["date"] = df["date"].apply(mpdates.date2num)
    series = df.set_index('date')
    df.index = series.index

    if output == 'native':
        fig, ax = plt.subplots(nrows=2, sharex=True, figsize=(20,8))
    else:
        fig, ax = plt.subplots(nrows=2, sharex=True, figsize=(10.5,7)) #web

    ax[0].xaxis_date()
    if 'open' in top_chart_technical_indicators_to_plot and 'high' in top_chart_technical_indicators_to_plot and 'low' in top_chart_technical_indicators_to_plot and 'close' in top_chart_technical_indicators_to_plot and 'volume' in top_chart_technical_indicators_to_plot:
        mpf.candlestick_ohlc(ax[0], df[['date', 'open', 'high', 'low', 'close', 'volume']].values, width=.3, colorup='#53c156', colordown='#ff1717')
    
    plt.xticks(rotation=90) #45

    if 'mark_buy' or 'mark_sell' in df.columns:
        buy_markers = get_mark_buy_row_number(df)
        sell_markers = get_mark_sell_row_number(df)
        ax[0].plot(df['close'], linestyle='-',marker='^', markerfacecolor='g', markeredgecolor='g', color='b', markevery=buy_markers, label='Buy Indications') #color='g', 
        ax[0].plot(df['close'], linestyle='-',marker='v', markerfacecolor='r', markeredgecolor='r', color='b', markevery=sell_markers ,label='Sell Indications')  #color='r', 
    
    top_chart_technical_indicators_to_plot.remove('date')
    for col in top_chart_technical_indicators_to_plot:
        item = df[col]
        ax[0].plot(item)

    for key in bottom_chart_field_dictionary:
        if bottom_chart_field_dictionary[key] == 'bar':
            ax[1].bar(df.index, df[key]) #, width=1/(5*len(df.index))
        elif bottom_chart_field_dictionary[key] == 'plot':
            ax[1].plot(df[key])

    xfmt = mpl.dates.DateFormatter('%d-%m-%Y') #%Y-%m-%d
    ax[1].xaxis.set_major_locator(mpl.dates.DayLocator(interval=7))
    ax[0].xaxis.set_major_formatter(xfmt)

    ax[1].xaxis.set_major_locator(mpl.dates.DayLocator(interval=7))
    ax[1].xaxis.set_major_formatter(xfmt)
    ax[1].get_xaxis().set_tick_params(which='major', pad=25)

    fig.autofmt_xdate()

    ax[0].set_title(f'{ticker} From {mpdates.num2date(df.iloc[0]["date"]).strftime("%d-%m-%Y")} To {mpdates.num2date(df.iloc[-1]["date"]).strftime("%d-%m-%Y")}')
    ax[1].set_title('Volume' if sub_plot_title is None else sub_plot_title)
    ax[0].set_ylabel('Price')
    ax[1].set_ylabel(','.join(list(bottom_chart_field_dictionary.keys()))) #'Quantity' if sub_plot_y_title is None else sub_plot_y_title
    ax[0].legend(loc="upper left")
    ax[1].legend(loc="lower left")
    # ax[1].lenged()
    # try:
    #     ax[1].lenged()
    # except:
    #     notify('Cannot create legend for lower plots', 'danger')

    if output == 'native':
        plt.show()
    
    if output == 'notebook':
        mpld3.fig_to_html(plt)


    # if output == 'large_png':
    #     plt.savefig(f'{config.graphs_directory}/{ticker} From {mdates.num2date(df.iloc[0]["date"]).strftime("%d-%m-%Y")} To {mdates.num2date(df.iloc[-1]["date"]).strftime("%d-%m-%Y")}.png')

    # if output == 'png':
    #     plt.savefig(f'{config.graphs_directory}/{ticker} From {mdates.num2date(df.iloc[0]["date"]).strftime("%d-%m-%Y")} To {mdates.num2date(df.iloc[-1]["date"]).strftime("%d-%m-%Y")}.png')

    # if output == 'large_png_base64_string':
    #     plt.savefig('temp.png')
    #     import base64

    #     in_file = open("temp.png", "rb") # opening for [r]eading as [b]inary
    #     data = in_file.read() # if you only wanted to read 512 bytes, do .read(512)
    #     in_file.close()
    #     os.remove('temp.png')
    #     return base64.b64encode(data)

    # if output == 'png_base64_string':
    #     plt.savefig('temp.png')
    #     import base64

    #     in_file = open("temp.png", "rb") # opening for [r]eading as [b]inary
    #     data = in_file.read() # if you only wanted to read 512 bytes, do .read(512)
    #     in_file.close()
    #     os.remove('temp.png')
    #     return base64.b64encode(data)

def get_mark_buy_row_number(df):
    res = []
    row_number = 0
    for index, row in df.iterrows():
        if row['mark_buy'] == True:
           res.append(row_number)
        row_number = row_number + 1
    return res

def get_mark_sell_row_number(df):
    res = []
    row_number = 0
    for index, row in df.iterrows():
        if row['mark_sell'] == True:
           res.append(row_number)
        row_number = row_number + 1
    return res

# def plot_one_graph(ticker, df, technical_indicators_to_plot, output='native'):
#     print(technical_indicators_to_plot)
#     #https://matplotlib.org/users/pyplot_tutorial.html
#     #https://mpld3.github.io/modules/API.html#mpld3.fig_to_html

#     if output == 'native':
#         plt.figure(figsize=(20, 8))
#     else:
#         plt.figure(figsize=(10.5, 7)) #web

#     series = df.set_index('date')
#     df.index = series.index

#     plt.ylabel('Price')
#     plt.xlabel('Date')
#     plt.xticks(rotation=45)
#     plt.ylabel('Price')
#     plt.xlabel('Date')
#     plt.title(f'{ticker}')

#     df1 = df[['date', 'open', 'high', 'low', 'close', 'volume']]
#     df1["date"] = df["date"].apply(mpdates.date2num)

#     f1 = plt.subplot2grid((6, 4), (1, 0), rowspan=6, colspan=4, axisbg='#07000d')
#     f1.patch.set_facecolor('white')
#     # https://stackoverflow.com/questions/50559200/candlestick-plot-from-pandas-dataframe-replace-index-by-dates
#     mpf.candlestick_ohlc(f1, df1.values, width=.6, colorup='#53c156', colordown='#ff1717')
#     f1.xaxis_date()

#     # plt.plot(df['close']) #always show the closing price
#     for col in technical_indicators_to_plot:
#         item = series[col]
#         plt.plot(item)

#     plt.legend()

#     if output == 'native':
#         # mpld3.show(plt.figure())
#         plt.show()
    
#     # if output == 'html':
#     #     return mpld3.fig_to_html(plt.figure())

#     # if output == 'large_html':
#     #     return mpld3.fig_to_html(plt.figure()) #fig_to_html fig_to_d3

#     # if output == 'large_png':
#     #     #plt.subplots(figsize=(40,16))
#     #     plt.savefig(f'{config.graphs_directory}/{ticker} From {mdates.num2date(df.iloc[0]["date"]).strftime("%d-%m-%Y")} To {mdates.num2date(df.iloc[-1]["date"]).strftime("%d-%m-%Y")}.png')

#     # if output == 'png':
#     #     plt.savefig(f'{config.graphs_directory}/{ticker} From {mdates.num2date(df.iloc[0]["date"]).strftime("%d-%m-%Y")} To {mdates.num2date(df.iloc[-1]["date"]).strftime("%d-%m-%Y")}.png')




#https://www.geeksforgeeks.org/plot-candlestick-chart-using-mplfinance-module-in-python/
# def plot_candle_stick(df):
#     plt.style.use('dark_background') 

#     # extracting Data for plotting 
#     # df = pd.read_csv('data.csv') 
#     # df = df[['Date', 'Open', 'High', 
#     #         'Low', 'Close']] 

#     # # convert into datetime object 
#     # df['Date'] = pd.to_datetime(df['Date']) 

#     # apply map function 
#     df['date'] = df['date'].map(mpdates.date2num) 

#     # creating Subplots 
#     fig, ax = plt.subplots() 

#     # plotting the data 
#     candlestick_ohlc(ax, df.values, width = 0.6, 
#                     colorup = 'green', colordown = 'red', 
#                     alpha = 0.8) 

#     # allow grid 
#     ax.grid(True) 

#     # Setting labels 
#     ax.set_xlabel('Date') 
#     ax.set_ylabel('Price') 

#     # setting title 
#     plt.title('Prices For the Period 01-07-2020 to 15-07-2020') 

#     # Formatting Date 
#     date_format = mpdates.DateFormatter('%d-%m-%Y') 
#     ax.xaxis.set_major_formatter(date_format) 
#     fig.autofmt_xdate() 

#     fig.tight_layout() 

#     # show the plot 
#     plt.show()