import os.path as path
import db.queries as queries
import utils.cli as cli
import utils.logger as log
import datetime
import re

import utils.cli as cli

good = None
bad = None
symbolIds = None
fake_symbols = None

def join_title_description(title, description):
    return f"{title}{'.' if title[-1] != '.' else ''} {description}{'.' if description[-1] != '.' else ''}"

def classifyText(text):
    try:
        splitted = set(text.replace(',',' ').replace('.',' ').replace(':',' ').replace('?','').replace('"','').replace("'","").replace("!","").replace('(','').replace(')','').replace('\n',' ').lower().strip().split(' '))
        
        global good
        global bad

        script_dir = path.dirname(__file__)

        if good is None:
            abs_good_file_path = path.join(script_dir, "good.txt")
            with open(abs_good_file_path, "r", encoding = "utf-8") as dic1:
                good = dic1.read().lower().split('\n')

        if bad is None:
            abs_bad_file_path = path.join(script_dir, "bad.txt")
            with open(abs_bad_file_path, "r", encoding = "utf-8") as dic2:
                bad = dic2.read().lower().split('\n')

        goodIntersections = len(splitted.intersection(good))
        badIntersections = len(splitted.intersection(bad))
        
        # print(f'Good indicators: {str(goodIntersections)} Bad indicators: {str(badIntersections)}')
        if goodIntersections > badIntersections:
            return 'Positive'
        elif badIntersections > goodIntersections:
            return 'Negative'
        return 'Unknown'
    except:
        return 'Error'

def recalculate_naive_articles_sentiment():
    """
    Run the naive classifier again in order to correct classification errors
    """
    articles = queries.get_all_articles(datetime.datetime(2018,1,1)) 
    items_to_update = []
    for article in articles:
        texts = join_title_description(article.title, article.description)
        classification = classifyText(texts)
        if article.naiveSentiment != classification:
            article.naiveSentiment = classification
            items_to_update.append(article)
    
    if len(items_to_update) > 0:
        cli.warning('Classify', f'Updating {len(items_to_update)} article naive sentiments')
        queries.bulk_update(items_to_update)
    else:
        cli.normal('Classify', 'No article need naive sentiments update')

def recalculate_symbolId_extraction():
    """
    Run the symbol extraction again in order to correct errors
    """
    posts = queries.get_all_posts(datetime.datetime(2018,1,1)) 
    items_to_update = []
    texts = ''
    for post in posts:
        if post.title is not None and len(post.title) > 0 and post.description is not None and len(post.description) > 0:
            texts = join_title_description(post.title, post.description)
        elif len(post.title) > 0:
            texts = post.title
        else:
            texts = post.description

        symbolIds = ','.join(extract_symbols(texts))
        if post.symbolIds != symbolIds:
            post.symbolIds = symbolIds
            items_to_update.append(post)
    
    if len(items_to_update) > 0:
        cli.warning('Classify', f'Updating {len(items_to_update)} symbol extraction')
        queries.bulk_update(items_to_update)
    else:
        cli.normal('Classify', 'No post need symbol extraction update')

def extract_symbols(txt):
    global symbolIds
    global fake_symbols

    if symbolIds is None:
        symbolIds = queries.get_all_symbolIds()

    if fake_symbols is None:
        script_dir = path.dirname(__file__)
        abs_fakes_file_path = path.join(script_dir, "fake_symbols.txt")
        with open(abs_fakes_file_path, "r", encoding = "utf-8") as dic1:
            fake_symbols = dic1.read().split('\n')

    stop_chars = '?$()[].,"'
    stop_chars = stop_chars + "'"

    for c in list(stop_chars):
        txt = txt.replace(c,'')
    
    symbol_candidates = list(set([i.strip() for i in re.findall(r"\b[A-Z]{1,5} ", txt.strip())]))
    res = [sc for sc in symbol_candidates if sc in symbolIds and sc not in fake_symbols]
    # print(res)
    return res

def test_classify():
    negatives = [
        'Boeing reports steep drop in 2020 deliveries, backlog',
        'Search expands for victims of Indonesian plane crash',
        'Boeing ended its worst year in more than 40 years with its best month of 2020.',
        'Cybercrime in the time of Covid — what firms need to do for security',
        "Hackers are sending customers fake shipping messages appearing to come from Amazon and UPS as a 'shipageddon' is expected during a hectic shopping season",
        'Boeing Deliveries Drop Despite 737 Maxs Return To Flight',
        'NASA’s Delayed Deep-Space Rocket Suffers Test Failure on the Ground'
    ]

    cli.warning('Classify', 'Should be all negative')

    for negative in negatives:
        print(f'{negative} = {classifyText(negative)}')

    positives = [
        'Beyond Meat Surges On Reports Of Deal With Taco Bell',
        'Canada clears Boeing 737 Max for flight nearly 2 years after global grounding | CBC News',
        'Canada OKs troubled Boeing 737 MAX 8 jets to resume service this week',
        'How pharmacies and retailers like Walmart, Kroger, and Rite Aid could get a boost from the vaccination push',
        'Check Point Software Technologies has agreed to purchase cybersecurity startup Odo Security to help enterprises enable secure remote access for employees to any application.',
        'Check Point Software partners with edX to deliver free online cyber-security courses (NASDAQ:CHKP)',
        'FinTech Scotland Forms Strategic Partnership With Check Point Software',
        'The Upside Destination For Boeing Stock Is Above $300 This Year',
        'Canada said on Monday it will lift a near two-year flight ban on Boeing Co’s 737 MAX on Jan. 20, joining other countries like the United States that have brought the aircraft back following two fatal crashes involving the model.'
    ]

    cli.warning('Classify', 'Should be all positives')

    for positive in positives:
        print(f'{positive} = {classifyText(positive)}')

    unknowns = [
        'Boeing recorded its worst year for net aircraft sales since 1977 in 2020, but received more than 80 orders for its 737 Max plane in December after the ban on the aircraft was lifted.',
    ]

    cli.warning('Classify', 'Should be all unknowns')
    
    for unknown in unknowns:
        print(f'{unknown} = {classifyText(unknown)}')

def test_extracy_symbols():
    res = extract_symbols(
        """
        In this section, I'm going to just give a brief overview of the support and resistance lines that we have developed. Again, sorry for all of the lines, I am a technical trader who was raised in a crayon factory, so I cannot
        help myself. The first thing to take a look at is the fat orange line. This line was resistance for GME when it had its second pop at the end of February. We have however since broken that resistance. As many of you know, previous resistance usually acts as future support in uptrends and this is VIOLENTLY true for GME. That fat orange line has withstood several short attacks and has literally bounced right back up since. Is this because of psychological levels? Is it a whale doing that on purpose? Is it the hedgies playing games? It's impossible to know, just know that the support is stronger than my wife's boyfriend's hatred of me. The fat yellow line on the bottom is essentially our doomsday level of support... let's not talk about that and hope we never get there. The fat green, yellow, and red lines on the top are our god levels of resistance. We had a bulltrap on the top green line just before that massive short attack. Getting above these levels is extremely crucial as they will be strong support. If we can get above the red line and stay there... MOON. Just know that we currently have some very strong support at 180ish and that is a great thing to have, especially because of how tested it has been.
        """)
    print(res)

if __name__ == '__main__':
    test_extracy_symbols()