#!/usr/bin/env python3
import signal
import sys
import argparse
import datetime

import logic.micro as micro
import logic.charts as charts
import quotes.quote as quote

###########
# globals #
###########
from_date = datetime.datetime(2020,1,1)
to_date = datetime.datetime.now()
symbol = ''
function = ''

def graceful_exit(signal, frame):
    print(quote.get())
    sys.exit(0)

def set_args():
    global symbol
    global from_date
    global to_date
    global function

    # https://docs.python.org/3/library/argparse.html
    parser = argparse.ArgumentParser(description='Micro analysis')
    parser.add_argument("--fn", "--function", help="Function to execute. 'gl' for gainers and losers, 'gla' for gainer and losers by articles, 'sip' for sector and industry performance", type=str, default='sip', required=True)
    parser.add_argument("--s", "--symbol", help="Symbol", type=str, default='MSFT', required=False)
    parser.add_argument("--f", "--from", help="From date yyyy-mm-dd", type=valid_date, default='2020-01-01', required=False)
    parser.add_argument("--t", "--to",help="From date yyyy-mm-dd", type=valid_date, default=datetime.datetime.now().strftime('%Y-%m-%d'), required=False)
    
    args = parser.parse_args()

    from_date = args.f
    to_date = args.t
    symbol = args.s
    function = args.fn

def valid_date(s):
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)

def main():
    global symbol
    global from_date
    global to_date
    global function

    if function == 'reports':
        micro.get_latest_reports(symbol)
    elif function == 'df':
        df = micro.digest(symbol, from_date, to_date)
        charts.plot(symbol, df, ['close'], [], output='notebook')
    elif function == 'dividends':
        micro.get_dividend_history(symbol)
    elif function == 'earnings':
        micro.get_earnings(symbol)

    # if function == 'gl':
    #     worksheet.gainers_and_losers()
    # elif function == 'gla':
    #     worksheet.get_gainer_and_losers_by_articles()
    # elif function == 'sip':
    #     worksheet.get_sector_and_industry_performance()

if __name__ == '__main__':
    signal.signal(signal.SIGINT, graceful_exit)
    set_args()
    main()