#!/usr/bin/env python
# https://www.codementor.io/@karandeepbatra/part-1-how-to-create-a-telegram-bot-in-python-in-under-10-minutes-19yfdv4wrq
import datetime
import re
from glob import glob
import random
import os

import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

import logic.macro as macro
import logic.micro as micro

commands = {}

def register_command(command, params, func, help = None):
    global commands
    commands[command] = {
        'params':params,
        'func':func,
        'help': f'/{command} {" ".join([f"<{p}>" for p in params])} - {help}' if help is not None else None
    }

def extract_params(context, command):
    global commands
    res = {}
    command_params = commands[command]['params']
    try:
        for i,c in enumerate(command_params):
            param_name = c.split(':')[0]
            param_type = c.split(':')[1]
            
            if param_type == 'int':
                res[param_name] = int(context.args[i])
            elif param_type == 'float':
                res[param_name] = float(context.args[i])
            elif param_type == 'date':
                res[param_name] = datetime.datetime.strptime(context.args[i], '%Y-%m-%d')
            else:
                res[param_name] = context.args[i]
        return res
    except:
        command_params['error'] = 'Error'

# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Paz Advisor Is ALIVE!')

def help(update, context):
    telegram.parse_mode = telegram.constants.PARSEMODE_MARKDOWN
    text = '*Help*\n\n'
    text = text + '\n'.join([f"{v['help']}" for k,v in commands.items() if v['help'] is not None])
    update.message.reply_text(text)

def etf(update, context):
    telegram.parse_mode = telegram.constants.PARSEMODE_MARKDOWN
    params = extract_params(context, 'etf')
    etfs = macro.get_etf_performance(time_frames = [params['days_ago']], sort_by_time_frame=params['days_ago'])

    res = '*ETF 4 Days Return*\n\n'
    res += '\n\n'.join([f'{e["symbolId"]}: {[f"{params["days_ago"]} Days Return"]}%' for e in etfs])

    update.message.reply_text(res)

def headlines(update, context):
    telegram.parse_mode = telegram.constants.PARSEMODE_MARKDOWN
    params = extract_params(context, 'headlines')
    articles = micro.get_articles(params['symbol'],datetime.datetime.now() - datetime.timedelta(days=params['days_ago']))

    res = f'*{params["symbol"]} Articles in last {params["days_ago"]} days*'
    res += '\n\n'.join([f'{a.title} - {a.naiveSentiment}' for a in sorted(articles, key=lambda item: item.publishedAt, reverse=True)[:10]])

    update.message.reply_text(res)

def gla(update, context):
    telegram.parse_mode = telegram.constants.PARSEMODE_MARKDOWN
    params = extract_params(context, 'gla')
    data = macro.get_gainer_and_losers_by_articles(days_ago=params['days_ago'])
    res = f'*Media Gainers & Losers in {params["days_ago"]} Last Days*\n\n'
    # TODO:
    res += '\n'.join([i for i in data.items()])

    update.message.reply_text(res)

def chart(update, context):
    # https://stackoverflow.com/questions/36778321/how-to-send-photo-on-telegram-bot
    # https://python-telegram-bot.readthedocs.io/en/stable/telegram.bot.html#telegram.Bot.edit_message_text
    # https://lih-verma.medium.com/lets-chat-with-telegram-bot-24f394a19cfb
    
    params = extract_params(context, 'chart')
    
    # photo = random_photo()
    res = {}
    if params['type'] == 'line':
        res = micro.chart(params['symbol'], datetime.datetime.now() - datetime.timedelta(days=params['days_ago']), line_fields=params['fields'].split(','), bar_fields=[],output='file')
    elif params['type'] == 'bar':
        res = micro.chart(params['symbol'], datetime.datetime.now() - datetime.timedelta(days=params['days_ago']), line_fields = [], bar_fields=params['fields'].split(','), output='file')
    
    photo = open(res['img'],'rb')
    context.bot.send_photo(chat_id=update.effective_message.chat_id, photo=photo, caption=res['title'])
    # os.remove(res['img']) #The process cannot access the file because it is being used by another process

def analyst(update, context):
    res = 'TODO'
    update.message.reply_text(res)

def sip(update, context):
    telegram.parse_mode = telegram.constants.PARSEMODE_MARKDOWN
    res = ['*Performance By Sector*']
    res.append('-' * 40)
    
    params = extract_params(context, 'sip')
    data = macro.get_sector_and_industry_performance(params['days_ago'])
    
    for index, sector in enumerate(list(data['sector'])[:10]):
        res.append(f'{index+1}. {sector["name"]} {sector["return"]}% ({sector["count"]})')
    
    res.append('')
    
    res.append('*Performance By Industry*')
    res.append('-' * 40)
    for index, industry in enumerate(list(data['industry'])[:10]):
        res.append(f'{index+1}. {industry["name"]} {industry["return"]}% ({industry["count"]})')
    
    update.message.reply_text('\n\n'.join(res))

def gl(update, context):
    res = 'TODO'
    update.message.reply_text(res)

def gla(update, context):
    res = 'TODO'
    update.message.reply_text(res)

def glp(update, context):
    metrics = macro.gainers_and_losers_by_posts()
    txt = ['Symbols on Reddit']

    for item in [m for m in metrics.values()][:20]:
        txt.append(f'{item["symbol"].name} ({item["symbol"].id}): UPS: {item["ups"]} DOWNS: {item["downs"]} ')
    
    res = '\n'.join(txt)
    update.message.reply_text(res)

def echo(update, context):
    """Echo the user message."""
    update.message.reply_text(update.message.text)

def error(update, context):
    """Log Errors caused by Updates."""
    print(update)
    print(context.error)
    
    # logger.warning('Update "%s" caused error "%s"', update, context.error)

def main():
    register_commands()
    global commands
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater("1617988416:AAGMEUxNFXtvbezkKLNwnGeopSXZ1X7a5P0", use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    #register all commands
    # dp.add_handler(CommandHandler("chart", chart)) # Single
    for command,details in commands.items():
        dp.add_handler(CommandHandler(command, details['func']))
    
    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # Start the Bot
    updater.start_polling(timeout=120)

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()
    delete_temps()

def delete_temps():
    script_dir = os.path.dirname(__file__)
    file_path = os.path.join(script_dir, '..', 'temp', '*.png')
    files = glob(file_path)
    for file in files:
        os.remove(file)
    print(f'Removed {len(files)} temp files')

def random_photo():
    files = glob("C:\\Users\\adm_user\\Pictures\\Wllpapers\\*.jpg")
    photo = open(random.choice(files), 'rb')
    return photo

def register_commands():
    register_command('gl', [], gl, 'Gainer and Losers')
    register_command('gla', ['days_ago:int'], gla, 'Gainer and Losers by articles')
    register_command('glp', [], glp, 'Gainer and Losers by Reddit posts')
    register_command('sip', ['days_ago:int'], sip, 'Sector & Industry performance')
    register_command('echo', ['text:str'], echo, 'Echo')
    register_command('analyst', [], analyst, 'Show analyst recommendations')
    register_command('chart', ['type:str', 'symbol:str', 'days_ago:int', 'fields:array'], chart, 'Send year chart image of symbol + data (high, low + more), type could be line or bar')
    register_command('headlines', ['symbol:str', 'days_ago:int'], headlines, 'Get headlines for symbol')
    register_command('etf', ['days_ago:int'], etf, 'ETF performance')
    register_command('help', [], help)

if __name__ == '__main__':
    register_commands()
    main()