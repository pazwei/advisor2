import datetime
import re
import pprint
import configparser
from os import path

import requests

# https://towardsdatascience.com/how-to-use-the-reddit-api-in-python-5e05ddfd1e5c

token = {}
headers = {}

def load_config():
    configParser = configparser.ConfigParser()
    script_dir = path.dirname(__file__)
    abs_config_file_path = path.join(script_dir, 'secrets.ini')
    configParser.read(abs_config_file_path)
    return configParser

def set_headers():
    configParser = load_config()
    config = configParser['Reddit']

    global token
    global headers

    # while the token is valid (~2 hours) we just add headers=headers to our requests
    if len(token.keys()) == 0 or len([i for i in token.keys() if ((datetime.datetime.now() - i).seconds / 60) > int(config['TokenMinutesDuration'])]) == 0:
        auth = requests.auth.HTTPBasicAuth(config['ClientId'], config['ClientToken'])
        data = {'grant_type': 'password',
                'username': config['Username'],
                'password': config['Password']}
        
        headers = {'User-Agent': config['UserAgent']}
        
        res = requests.post('https://www.reddit.com/api/v1/access_token', auth=auth, data=data, headers=headers)
        
        access_token = res.json()['access_token']
        # print(access_token)
        token = {
            datetime.datetime.now():access_token
        }
        
        headers = {**headers, **{'Authorization': f"bearer {access_token}"}}

def get_posts(url):
    set_headers()
    res = requests.get(url, headers=headers)
    # pprint.pprint(res.text)
    posts = []
    for post in res.json()['data']['children']:
        # pprint.pprint(res.json())  # let's see what we get
        # print('-' * 200)
        posts.append({
            'subreddit': post['data']['subreddit'],
            'author':post['data']['author'],
            'author_fullname': post['data']['author_fullname'],
            'title': post['data']['title'],
            'selftext': post['data']['selftext'],
            'ups': post['data']['ups'],
            'downs': post['data']['downs'],
            'score': post['data']['score'],
            'num_comments': post['data']['num_comments'],
            'upvote_ratio': post['data']['upvote_ratio'],
            'url': post['data']['url'], #'https://www.reddit.com' + post['data']['permalink']#
            'total_awards_received': post['data']['total_awards_received'],
            'created': datetime.datetime.fromtimestamp(post['data']['created']),
            'created_utc': datetime.datetime.utcfromtimestamp(post['data']['created_utc'])
        })

        
    return posts

if __name__ == '__main__':
    get_posts("https://oauth.reddit.com/r/wallstreetbets")