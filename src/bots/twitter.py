#!/usr/bin/env python3

import datetime
import re
import pprint
import configparser
from os import path
import json

import tweepy

###############
# Credentials #
###############
access_token = ""
access_token_secret = ""
consumer_key = ""
consumer_secret = ""

auth = tweepy.OAuthHandler(consumer_key,consumer_secret)
auth.set_access_token(access_token,access_token_secret)
api = tweepy.API(auth, wait_on_rate_limit=True)

def set_credentials():

    global access_token
    global access_token_secret
    global consumer_key
    global consumer_secret

    configParser = load_config()
    config = configParser['Twitter']

    access_token = config["AccessTokenKey"]
    access_token_secret = config["AccessTokenSecret"]
    consumer_key = config["ConsumerKey"]
    consumer_secret = config["ConsumerSecret"]

def load_config():
    configParser = configparser.ConfigParser()
    script_dir = path.dirname(__file__)
    abs_config_file_path = path.join(script_dir, 'secrets.ini')
    configParser.read(abs_config_file_path)
    return configParser

def follow(user):
    pass

def retweet(tweetId):
    pass

def update_status(message):
    api.update_status(message)

def search(search_words = ['GME','TSLA'], since = datetime.datetime.now() + datetime.timedelta(days = -1), limit = 100):
    res = []
    tweets = tweepy.Cursor(api.search, q=search_words, lang="en", since=since).items(limit)

    for tweet in tweets:
        print(tweet.text)

    res.append(tweets)

    return res

###########################################
# Stream Listener which holds the process #
###########################################
class MyStreamListener(tweepy.StreamListener):
    
    def __init__(self,api=None):
        super(MyStreamListener,self).__init__()
        self.num_tweets = 0
        self.stream_tweet_list = []
        self.file=open("tweet.txt","w")
    
    def on_status(self, status):
        tweet=status._json
        self.file.write(json.dumps(tweet)+ '\n')
        self.stream_tweet_list.append(status)
        self.num_tweets += 1
        if self.num_tweets < 1000:
            return True
        else:
            return False
        self.file.close()

def stream_listener(track=['covid','corona','covid19','coronavirus','facemask','sanitizer','social-distancing']):
    #create streaming object and authenticate
    l = MyStreamListener()
    stream = tweepy.Stream(auth,l)

    #this line filters twiiter streams to capture data by keywords
    stream.filter(track)

    tweets_data_path='copp.txt'
    tweets_data=[]
    tweets_file=open(tweets_data_path, "r")

    #read in tweets and store on list
    for line in tweets_file:
        tweet=json.loads(line)
        tweets_data.append(tweet)
    tweets_file.close()
    print(tweets_data[0])

if __name__ == '__main__':
    set_credentials()
    search()