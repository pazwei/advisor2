import asyncio
import datetime
import aiohttp
import logging
import time
import os


#pipenv install python-pipenv

#from dotenv import load_dotenv
#load_dotenv

url = 'https://www.alphavantage.co/query?function=overview&symbol={}&apikey={}'
api_key = '8M9K' # os.getenv('ALPHAVATNTAGE_API_KEY')
symbols = ['AAPL', 'GOOG', 'TSLA', 'MSFT']
results = []

# https://www.youtube.com/watch?v=nFn4_nA_yk8
# https://realpython.com/async-io-python/

# if we start new task and await for each one to complete, and then start the next one, it's kind of doing thing in sync... 
# #so what we do instead is to create a list of tasks, start them togther, then wait until the last one finished
def get_tasks(session):
    tasks = []
    for symbol in symbols:
        tasks.append(asyncio.create_task(session.get(url.format(symbol, api_key), ssl=False)))
    return tasks

async def get_symbols():
    start = time.time()
    async with aiohttp.ClientSession() as session:
        tasks = get_tasks(session)
        responses = await asyncio.gather(*tasks) # *[] destructure the list to it's items like ...array is JS
        for response in responses:
            results.append(await response.json())
    end = time.time()
    print(f'Total time is {end - start}')
    print(results)

def main():
    if os.name == 'nt':
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(get_symbols()) # asybcio.run encapsulate both the creation, running tasks and the close of the event loop
    
    #before python 3.7
    # loop = asyncio.new_event_loop()
    # loop.run_until_complete(get_symbols())
    # loop.close()

if __name__ == '__main__':
    main()