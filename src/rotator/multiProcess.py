import os
import time
import threading
import queue
import multiprocessing

# https://www.geeksforgeeks.org/multiprocessing-python-set-1/
# https://docs.python.org/3/library/multiprocessing.html

q = queue.Queue()

def tread_worker():
    while True:
        item = q.get()
        print(f'Working on {item}')
        print(f'Finished {item}')
        q.task_done()

def producer():
    for item in range(30):
        q.put(item)
    print('All task requests sent\n', end='')

def single_threaded_async_webserver():
    print("ID of process running worker1: {}".format(os.getpid()))
    #todo can create worker threads inside
    pass

def create_processes():
    res = []
    for i in range(os.cpu_count()):
        #Each process will run single_threaded_async_webserver on a different core. Nginx will reverse proxy it (same as experss app)
        res.append(multiprocessing.Process(target=single_threaded_async_webserver, args=(1,2)))
        pass
    return res

def create_process_thread(process):
    threading.Thread(target=tread_worker, daemon=False).start()
    pass

def main():
    print("ID of main process: {}".format(os.getpid()))
    processes = create_processes()
    for process in processes:
        create_process_thread(process)
        process.start()
        process.join() #wait for the process to end
        q.join()

    print(f'Creating {os.cpu_count()} processes, with 4 threads in each process')    

if __name__ == '__main__':
    main()