#!/usr/bin/env python3

import datetime
import time
import pickle
import hashlib
import configparser
import json
from os import path 
import socket
from subprocess import check_output
import os
import pprint

import requests

import utils.cli as cli

class FreeTierApiRequest():
    def __init__(self, api_name, request_name, verb, url, cost, memoize_days):
        self.api_name = api_name
        self.request_name = request_name
        self.verb = verb
        self.url = url
        self.dispatched_at = datetime.datetime.now()
        self.cost = cost
        self.memoize_days = memoize_days

        self.response = None
        self.hash = None
        
    @staticmethod
    def create_hash(api_name, request_name, querystring_params, json_payload):
        key = f'{api_name}_{request_name}_{str(querystring_params)}_{str(json_payload)}'
        hashed = hashlib.sha256(bytes(key, 'utf-8')).hexdigest()
        return hashed

    @staticmethod
    def to_history(hash, api_name, request_name, dispatched_at,querystring, json_payload, response, cost, memoize_days):
        return {
            hash: {
            'api_name':api_name,
            'request_name':request_name,
            'dispatched_at':dispatched_at,
            'querystring':querystring,
            'json_payload':json_payload,
            'response':response,
            'cost':cost,
            'valid_until':dispatched_at + datetime.timedelta(days = memoize_days)
            }
        }

    def dispatch(self, key_to_use, querystring_params, json_payload, url_override = None):
        self.dispatched_at = datetime.datetime.now()
        compiled_endpoint = self.url if url_override is None else url_override
        if querystring_params is not None:
            qs = '&'.join([f'{key}={querystring_params[key]}' for key in querystring_params.keys()]).replace('%APIKEY%',key_to_use)
            compiled_endpoint = f'{self.url if url_override is None else url_override}?{qs}' #f'https://api.dandelion.eu/datatxt/nex/v1/?lang=en&token=%APIKEY%'
        
        compiled_json_payload = json_payload
        if compiled_json_payload is not None:
            for key in compiled_json_payload.keys():
                if compiled_json_payload[key] == '%APIKEY%':
                    compiled_json_payload[key] = key_to_use
        
        cli.warning('FreeTierApiRotator', f'Dispatch to="{compiled_endpoint}"')
        try:
            request_name = self.request_name if url_override is None else url_override
            if self.verb == 'post':
                res = requests.post(compiled_endpoint, compiled_json_payload)
                self.response = res.json()
                if self.memoize_days > 0:
                    self.hash = FreeTierApiRequest.create_hash(self.api_name, request_name, querystring_params, json_payload) #self.request_name
                return self.response
            elif self.verb == 'get':
                res = requests.get(compiled_endpoint, compiled_json_payload)
                if self.memoize_days > 0:
                    self.hash = FreeTierApiRequest.create_hash(self.api_name, request_name, querystring_params, json_payload) #self.request_name
                self.response = res.json()
                return self.response
            elif self.verb == 'put':
                res = requests.put(compiled_endpoint, compiled_json_payload)
                if self.memoize_days > 0:
                    self.hash = FreeTierApiRequest.create_hash(self.api_name, request_name, querystring_params, json_payload) #self.request_name
                self.response = res.json()
                return self.response
            elif self.verb == 'delete':
                res = requests.delete(compiled_endpoint, compiled_json_payload)
                if self.memoize_days > 0:
                    self.hash = FreeTierApiRequest.create_hash(self.api_name, request_name, querystring_params, json_payload) #self.request_name
                self.response = res.json()
                return self.response
            elif self.verb == 'patch':
                res = requests.patch(compiled_endpoint, compiled_json_payload)
                if self.memoize_days > 0:
                    self.hash = FreeTierApiRequest.create_hash(self.api_name, request_name, querystring_params, json_payload) #self.request_name
                self.response = res.json()
                return self.response
            else:
                cli.danger('FreeTierApiRotator', f'Unkown command for request {self.request_name}')
        except Exception as e:
            cli.danger('FreeTierApiRotator', f'FAILED to access {compiled_endpoint} with json payload {compiled_json_payload}')
            print(e)

class FreeTierApiRotator():
    """
    This class serves 3 purposes regarding external data providers by API keys (Alpha Vantage, IEX, Dandelion)
    1) Manage multikey free tier subscriptions in order not to pay for api requests that exceeds the free tier limit amount and delay
    2) Memoize, save all requests and responses, so duplicate requests will be served from cache.
    3) Track daily usage for each api key and request cost

    When building querystring or json payload, use the %APIKEY% as placeholder for the api key.
    Both querystring and json_payload are dictionaries. %APIKEY% is the value inside one of the keys
    """
    def __init__(self, config_file = 'freeTierApiRotator.ini', pickle_file = 'freeTierApiRotatorState.pickle', verbose = True):
        # api_name, api_keys, daily_credits_per_key_limit, min_delay_between_requests, requests
        config = configparser.ConfigParser()
        script_dir = path.dirname(__file__)
        abs_config_file_path = path.join(script_dir, config_file)
        config.read(abs_config_file_path)
        sections = config.sections()

        self.PICKLE_FILE = path.join(script_dir, pickle_file)
        self.verbose = verbose
        self.ip = self.__get_local_ip()
        self.hostname = self.__get_local_hostname()

        self.apis = {}

        for section in sections:
            # load settings from config, do not cache the setting, since they can be changed overtime. Once changed do some sort of cache reboot
            self.apis[section] = {
                'credits_per_key_per_ip_per_day': int(config[section]['CreditsPerKeyPerIpAddressPerDay']),
                'api_keys': [a.strip() for a in config[section]['ApiKeys'].split(',')],
                'seconds_delay_between_same_key_requests':float(config[section]['SecondsDelayBetweenSameKeyRequests']),
                'requests':[FreeTierApiRequest(section, a['name'], a['verb'], a['url'], int(a['cost']), int(a['MemoizeDays'])) for a in json.loads(config[section]['Requests'])], #name, verb, url, credit_cost
                'auto_save':bool(config[section]['AutoSave'])
            }
        
        state = self.__load_saved_state()
        for section in sections:
            maintained_state = self.__state_house_keeping(state[section], section)
            # print(maintained_state)
            # print(self.__get_local_ip())
            # print(maintained_state)
            self.apis[section]['state'] = maintained_state

    def __verbose(self, msg):
        if self.verbose:
            cli.normal('FreeTierApiRotator', msg)

    def memoize(self, api_name, request_name, querystring_params, json_payload, url_override = None, bulk = 1): # IEX put symbol in the controller/symbol/action and not in querystring
        """
        Wrapper around dispatch which cache response for previous request
        """

        name_to_hash = request_name if url_override is None else url_override
        hash = FreeTierApiRequest.create_hash(api_name, name_to_hash, querystring_params, json_payload) #request_name

        #check for cached response
        if hash in self.apis[api_name]['state']['history'].keys():
            self.__verbose(f'Return request from cache [{hash}]')
            return self.apis[api_name]['state']['history'][hash]['response']
        else:
            self.__verbose(f'Request NOT found in cache')
            request = self.__get_request_by_name(api_name, request_name)
            response = self.dispatch(api_name, request_name, querystring_params, json_payload, url_override, bulk)
            if response is not None:
                if request.memoize_days > 0:
                    history_record = FreeTierApiRequest.to_history(hash, api_name, name_to_hash, datetime.datetime.now(), querystring_params, json_payload, response, request.cost, request.memoize_days) #request_name
                    self.apis[api_name]['state']['history'] = {**self.apis[api_name]['state']['history'], **history_record}
                    if self.apis[api_name]['auto_save']:
                        self.save_state()
                return response
            else:
                cli.danger('FreeTierApiRotator', f'Cannot fulfill [{api_name}: {request_name}] there are no credits nor previous memory.')
                return None

    def dispatch(self, api_name, request_name, querystring_params, json_payload, url_override = None, bulk = 1):
        """
        Dispatch a request with the provided params
        """
        request = self.__get_request_by_name(api_name, request_name)  
        next_key = self.__next_api_key(api_name, request_name)
        if next_key is not None:
            key = next_key['key']
            delay = next_key['delay']
            if delay > 1:
                cli.normal('FreeTierApiRotator',f'Delaying for {delay} seconds...')
            time.sleep(delay)
            response = request.dispatch(key, querystring_params, json_payload, url_override)

            #if there is response: increment daily_counter, decrement ['revolver'][today][key], store response in history
            if response is not None:
                today = self.__today_revolver()
                self.apis[api_name]['state']['revolver'][today][key] -= request.cost * bulk
                self.apis[api_name]['state']['daily_counter'][self.__get_local_ip()] += 1
                if self.apis[api_name]['auto_save']:
                    self.save_state()
            return response
        else:
            cli.danger('FreeTierApiRotator', f'Cannot dispatch [{request_name}: {api_name}] no key was received.')
            return None

    #TO TEST
    def print_state(self, api_name):
        """
        print current status and the amount of requests left to perform
        """
        title = f'| State of {api_name} API |'
        print('-' * len(title))
        print(title)
        print('-' * len(title))
        pprint.pprint(self.apis[api_name]['state'])

    def count_unsaved_requests(self):
        """
        Count how many requests made are not cached
        """
        total = 0
        states = self.__load_saved_state()

        table = [['Api Name','In Memory Requests','Cached Requests','Delta']]

        for api in self.apis.keys():
            saved = len(states[api]['history'].keys())
            in_mem = len(self.apis[api]['state']['history'].keys())

            table.append([api, in_mem, saved, in_mem - saved])
            total += (in_mem - saved)

        if self.verbose:
            cli.print_table(table)

        return total

    def __get_request_by_name(self, api_name, request_name):
        match = [a for a in self.apis[api_name]['requests'] if a.request_name == request_name]
        if match is None or len(match) == 0:
            raise Exception(f'{request_name} request not found in {api_name}')
        return match[0]
    
    def __next_api_key(self, api_name, request_name):
        """
        Return tuple the next api key which has enough credits to fulfill for the request and also the delay delay needed
        {key, seconds delay}
        """

        #current_index = self.apis[api_name]['state']['daily_counter'] % len(self.apis[api_name]['api_keys'])

        # IP address limitation
        if self.apis[api_name]['state']['daily_counter'][self.__get_local_ip()] > self.apis[api_name]['credits_per_key_per_ip_per_day']:
            cli.danger('FreeTierApiRotator', f'Cannot fulfill request! Max credits per IP address was reached!')
            return None

        current_index = sum([value for key,value in self.apis[api_name]['state']['daily_counter'].items()]) // self.apis[api_name]['credits_per_key_per_ip_per_day']  

        request = self.__get_request_by_name(api_name, request_name) 

        res = {
            'key':'',
            'delay':0
        }

        # check the revolver if there are enough credits in the current_index, if so return the key and the delay
        # if not, check if one of the other keys have enough credits, if so return the key and the delay
        # return None, if the request cannot be fulfilled with the credits left

        today = self.__today_revolver()
        
        print(self.apis[api_name]['state']['revolver'])
        
        keys = list(self.apis[api_name]['state']['revolver'][today].keys())
        selected_key = keys[current_index]
        selected_index = keys.index(selected_key)

        fallback_key = [key for key,credits in self.apis[api_name]['state']['revolver'][today].items() if credits >= request.cost] #other key with sufficient credits
        fallback_index = None if len(fallback_key) == 0 else fallback_key[0]
        
        # if self.apis[api_name]['state']['revolver'][today][selected_key] >= request.cost:
        #     res['key'] = self.apis[api_name]['api_keys'][selected_index]
        #     res['delay'] = self.__calculate_next_delay(api_name)
        # elif fallback_index is not None:
        #     res['key'] = self.apis[api_name]['api_keys'][fallback_index]
        #     res['delay'] = self.__calculate_next_delay(api_name)
        # else:
        if current_index <= len(keys):
            res['key'] = self.apis[api_name]['api_keys'][selected_index]
            res['delay'] = self.__calculate_next_delay(api_name)
        elif fallback_index is not None:
            res['key'] = self.apis[api_name]['api_keys'][fallback_index]
            res['delay'] = self.__calculate_next_delay(api_name)
        else:
            cli.danger('FreeTierApiRotator', f'There are no enough credits {str(self.apis[api_name]["state"]["revolver"])}')
            return None
        return res

    def __calculate_next_delay(self, api_name):
        return self.apis[api_name]['seconds_delay_between_same_key_requests']

        # If juggling is possible (doubling the limit by using many keys from same IP):
        # return (self.apis[api_name]['seconds_delay_between_same_key_requests'] / len(self.apis[api_name]['api_keys'])) + 1
        
    def save_state(self):
        data = {key:value['state'] for key,value in self.apis.items() }
        file = open(self.PICKLE_FILE, 'wb')
        pickle.dump(data, file)
        file.close()

    def get_prestine_section_state(self, section):
        local_ip = self.__get_local_ip()
        prestine = {
            "daily_counter":{local_ip:0},
            "revolver": 
            {
                "20201214": {
                    'apiKey1':500, #ip1:apiKey1:500
                    'apiKey2':500
                },
                "20201215": {
                    'apiKey1':500,
                    'apiKey2':500
                }
            },
            "history":{}
        }

        prestine['revolver'].clear()
        key_counters = {}

        for key in self.apis[section]['api_keys']:
            key_counters[key] = self.apis[section]['credits_per_key_per_ip_per_day']

        today = self.__today_revolver()
        prestine['revolver'][today] = key_counters
        
        return prestine

    def __get_prestine_state(self):
        state = {}
        for section in self.apis.keys():
            state[section] = self.get_prestine_section_state(section)
        
        return state

    def __load_saved_state(self):
        
        script_dir = path.dirname(__file__)
        abs_file_path = path.join(script_dir, self.PICKLE_FILE)

        if path.exists(abs_file_path):
            file = open(abs_file_path, 'rb')
            state = pickle.load(file)
            file.close()
            # print(state)
            # Enrich previous state by create prestine states for new apis
            new_apis = [key for key in self.apis.keys() if key not in state.keys()]
            for new_api in new_apis:
                state[new_api] = self.get_prestine_section_state(new_api)

            return state
        else:
            return self.__get_prestine_state()

    def __calculate_daily_creadits(self, api_name):
        """
        Get the total credits lefts
        """
        return len(self.apis[api_name]['api_keys']) * self.apis[api_name]['credits_per_key_per_ip_per_day']

    def __get_remainder_credits(self, api_name):
        today = self.__today_revolver()
        return sum([value for key,value in self.apis[api_name]['state']['revolver'][today].items()])

    def __today_revolver(self, date = None):
        return datetime.datetime.now().strftime('%Y%m%d') if date == None else date.strftime('%Y%m%d') 

    def __parse_today_str(self, dateStr):
        return datetime.datetime.strptime(dateStr, '%Y%m%d')
    
    def get_requests_left_to_dispatch_today(self, api_name, request_name = None):
        today_str = self.__today_revolver()
        total_credits = 0
        for _, value in self.apis[api_name]['state']['revolver'][today_str].items():
            total_credits += value

        if request_name is not None:
            request = self.__get_request_by_name(api_name, request_name)
            return int(total_credits / request.cost)
        else:
            return total_credits

    def __state_house_keeping(self, api_state, api_name):
        """
        Handle loaded state:
        1. Clear expired records
        2. Create new date record for the revolver
        3. Clear daily counter for new date
        4. State can come from in memory activity for long running app or from cached version on disk
        """
        
        revolver_dates = api_state['revolver'].keys()
        
        #clear expired memos in the revolver - old revolvers don't matter since their credits cannot be used
        #maybe for usage tracking keep history??
        api_state['revolver'] = {key:value for key,value in api_state['revolver'].items() if key == self.__today_revolver()} #datetime.datetime.now() - self.__parse_today_str(key)).days <= self.apis[api_name]['memoize_days']}
        
        # Clear expired histories
        api_state['history'] = { key:record for key,record in api_state['history'].items() if record['valid_until'] >= datetime.datetime.now()}

        # Add today to revolver with dictionary of key counters
        today = self.__today_revolver()
        if today not in revolver_dates:
            api_state['daily_counter'] = {self.__get_local_ip() : 0}
            api_state['revolver'][today] = {key:self.apis[api_name]['credits_per_key_per_ip_per_day'] for key in self.apis[api_name]['api_keys']}
    
        if self.__get_local_ip() not in api_state['daily_counter'].keys():
            api_state['daily_counter'][self.__get_local_ip()] = 0

        return api_state

    def __get_api_keys(self, section):
        return self.apis[section]['api_keys']

    #https://stackoverflow.com/questions/24196932/how-can-i-get-the-ip-address-from-nic-in-python
    def __get_local_ip(self):
        ip_address = ''
        if os.name == 'nt':
            hostname = socket.gethostname()
            ip_address = socket.gethostbyname(hostname)
        else:
            ip_address = check_output(['hostname', '--all-ip-addresses']).strip().decode("utf-8")
        return ip_address

    def __get_local_hostname(self):
        hostname = socket.gethostname()
        return hostname


API_ROTATOR = FreeTierApiRotator()

def test():
    # text1 = 'Amazon Has a 2021 Growth Driver Going Under the Radar, Says Analyst. While many companies have been crushed by Covid-19’s impact, Amazon (AMZN) has benefited immensely. Sales have gone through the roof and the company has made all the right moves in catering for the outsized demand.Of course, apart from the core ecommerce business, the company has multiple revenue streams. However, Needham analyst Laura Martin has pinpointed a segment which she believes is currently being overlooked. The 5-star analyst says Amazon’s ad revenue stream is undervalued and anticipates “advertising revenue growth will be an important upside profit driver for AMZN during 2021.”The latest third-party data indicates Amazon is increasingly taking more product search share from its search engine rivals, namely Google. Why is this important on the advertising end?Martin explained, “Growing product search share attracts more advertisers and improves conversion rates, which attracts more advertisers.”A ChannelAdvisor survey conducted in August found that if a U.S. adult plans on making an online purchase, 53% will head to Amazon first. In comparison, 23% will begin their product search on Google.What’s more, Martin notes, “Searches for products on AMZN more frequently turns into a purchase.”According to online ad specialist Wordstream, Google search’s conversion rate stands at roughly 4%, whilst AdBadger analytics indicate Amazon’s is at roughly 10%, so the conversion rate at Amazon is more than twice as much as it is on Google.“We believe AMZNs much higher conversion rate is because the ads "context" is better at AMZN,” Martin said. “That is, the mental starting point for many consumers when they open the AMZN app is to buy something.”Talking of buying, that’s what Martin recommends investors do with Amazon stock. The analyst reiterated a Buy rating alongside a $3,700 price target. Investors are looking at gains of 19%, should Martin’s thesis play out in the year ahead. (To watch Martin’s track record, click here)Barring one lone Hold rating, all 36 other recent Amazon reviews have reached the same conclusion – Buy. With a $3,819.89 average price target alongside the Strong Buy consensus rating, the projection is for ~23% upside over the next 12 months. (See Amazon stock analysis on TipRanks)To find good ideas for stocks trading at attractive valuations, visit TipRanks’ Best Stocks to Buy, a newly launched tool that unites all of TipRanks’ equity insights.Disclaimer: The opinions expressed in this article are solely those of the featured analyst. The content is intended to be used for informational purposes only. It is very important to do your own analysis before making any investment.'
    # response1 = API_ROTATOR.memoize('Dandelion','keywords',{'lang':'en', 'token':'%APIKEY%', 'text':text1}, None)
    # print(response1)
    
    text2 = 'Apple investigating Wistron facility in India after violence. Apple Inc said it is investigating if its Taiwanese contractor Wistron Corp flouted supplier guidelines at a violence-hit iPhone manufacturing facility near the Indian city of Bengaluru, following violence at the plant.'
    response2 = API_ROTATOR.memoize('Dandelion','keywords',{'lang':'en', 'token':'%APIKEY%', 'text':text2}, None)
    # print(response2)
    
    API_ROTATOR.count_unsaved_requests()

if __name__  == '__main__':
    test()