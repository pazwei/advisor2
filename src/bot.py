#!/usr/bin/env python3
import signal
import sys
import argparse
import datetime

# import logic.macro as macro
import logic.micro as micro
import quotes.quote as quote
import bots.telegram as telegram

def graceful_exit(signal, frame):
    print(quote.get())
    sys.exit(0)

def main():
    telegram.main()
    # micro.chart('TSLA', datetime.datetime.now() + datetime.timedelta(days = -200))
    
if __name__ == '__main__':
    signal.signal(signal.SIGINT, graceful_exit)
    main()