#!/usr/bin/env python3
# from datetime import datetime, timedelta
import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session

import config.config as appConfig

import db.entities #all entities are imported via package loader

def get_session():
    db_exists = os.path.exists(appConfig.SQLITE_FILENAME_PATH)
    engine = create_engine(f'sqlite:///{appConfig.SQLITE_FILENAME_PATH}', connect_args={'check_same_thread': False}, echo=False) #, pool_size=10
    
    if(db_exists == False):
        db.entities.ORM.SqlAlchemySchemaBuilder.metadata.create_all(engine)

    # https://stackoverflow.com/questions/6297404/multi-threaded-use-of-sqlalchemy
    session_factory = sessionmaker(bind=engine, autoflush=False) #, expire_on_commit=False
    session = scoped_session(session_factory)

    return session

if __name__ == '__main__':
    session = get_session()