from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class Overview(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'overviews'

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("overviews", uselist=True), lazy='select')

    fiscalYearEnd = Column(String(), nullable=True)
    latestQuarter = Column(DateTime(), nullable=True)
    marketCapitalization = Column(Integer(), nullable=True)
    ebitda = Column(Integer(), nullable=True)
    peRatio = Column(Float(), nullable=True)
    pegRatio = Column(Float(), nullable=True)
    bookValue = Column(Float(), nullable=True)
    dividendPerShare = Column(Float(), nullable=True)
    dividendYield = Column(Float(), nullable=True)
    eps = Column(Float(), nullable=True)
    revenuePerShareTTM = Column(Float(), nullable=True)
    profitMargin = Column(Float(), nullable=True)
    operatingMarginTTM = Column(Float(), nullable=True)
    returnOnAssetsTTM = Column(Float(), nullable=True)
    returnOnEquityTTM = Column(Float(), nullable=True)
    revenueTTM = Column(Integer(), nullable=True)
    grossProfitTTM = Column(Integer(), nullable=True)
    dilutedEPSTTM = Column(Float(), nullable=True)
    quarterlyEarningsGrowthYOY = Column(Float(), nullable=True)
    quarterlyRevenueGrowthYOY = Column(Float(), nullable=True)
    analystTargetPrice = Column(Float(), nullable=True)
    trailingPE = Column(Float(), nullable=True)
    forwardPE = Column(Float(), nullable=True)
    priceToSalesRatioTTM = Column(Float(), nullable=True)
    priceToBookRatio = Column(Float(), nullable=True)
    evToRevenue = Column(Float(), nullable=True)
    evToEBITDA = Column(Float(), nullable=True)
    beta = Column(Float(), nullable=True)
    week52High = Column(Float(), nullable=True)
    week52Low = Column(Float(), nullable=True)
    day50MovingAverage = Column(Float(), nullable=True)
    day200MovingAverage = Column(Float(), nullable=True)
    sharesOutstanding = Column(Integer(), nullable=True)
    sharesFloat = Column(Integer(), nullable=True)
    sharesShort = Column(Integer(), nullable=True)
    sharesShortPriorMonth = Column(Float(), nullable=True)
    shortRatio = Column(Float(), nullable=True)
    shortPercentOutstanding = Column(Float(), nullable=True)
    shortPercentFloat = Column(Float(), nullable=True)
    percentInsiders = Column(Float(), nullable=True)
    percentInstitutions = Column(Float(), nullable=True)
    forwardAnnualDividendRate = Column(Float(), nullable=True)
    forwardAnnualDividendYield = Column(Float(), nullable=True)
    payoutRatio = Column(Float(), nullable=True)
    dividendDate = Column(DateTime(), nullable=True)
    exDividendDate = Column(DateTime(), nullable=True)
    lastSplitFactor = Column(String(), nullable=True)
    lastSplitDate = Column(DateTime(), nullable=True)