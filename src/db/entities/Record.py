from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class Record(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'records'

    symbolId = Column(String(), ForeignKey('symbols.id'))
    symbol = relationship("Symbol", backref=backref("Record", uselist=True), lazy='select')

    date = Column(DateTime, nullable=False, default = datetime.now())

    open = Column(Float(), nullable=False)
    high = Column(Float(), nullable=False)
    low = Column(Float(), nullable=False)
    close = Column(Float(), nullable=False)
    adjustedClose = Column(Float(), nullable=True)
    volatility = Column(Float(), nullable=False)
    change = Column(Float(), nullable=False)
    volume = Column(Integer(), nullable=False)

    def __repr__(self):
       return f'<Record id={self.id} date={self.date.strftime("%Y-%m-%d")} close={self.close} change={self.change}>'