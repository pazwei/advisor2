from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class BalanceSheet(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'balancesheets'

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("balancesheets", uselist=True), lazy='select')

    term = Column(String(), nullable=False)

    fiscalDateEnding = Column(DateTime(), nullable=True)
    reportedCurrency = Column(String(), nullable=True)
    totalAssets = Column(Integer(), nullable=True)
    intangibleAssets = Column(Integer(), nullable=True)
    earningAssets = Column(Integer(), nullable=True)
    otherCurrentAssets = Column(Integer(), nullable=True)
    totalLiabilities = Column(Integer(), nullable=True)
    totalShareholderEquity = Column(Integer(), nullable=True)
    deferredLongTermLiabilities = Column(Integer(), nullable=True)
    otherCurrentLiabilities = Column(Integer(), nullable=True)
    commonStock = Column(Integer(), nullable=True)
    retainedEarnings = Column(Integer(), nullable=True)
    otherLiabilities = Column(Integer(), nullable=True)
    goodwill = Column(Integer(), nullable=True)
    otherAssets = Column(Integer(), nullable=True)
    cash = Column(Integer(), nullable=True)
    totalCurrentLiabilities = Column(Integer(), nullable=True)
    shortTermDebt = Column(Integer(), nullable=True)
    currentLongTermDebt = Column(Integer(), nullable=True)
    otherShareholderEquity = Column(Integer(), nullable=True)
    propertyPlantEquipment = Column(Integer(), nullable=True)
    totalCurrentAssets = Column(Integer(), nullable=True)
    longTermInvestments = Column(Integer(), nullable=True)
    netTangibleAssets = Column(Integer(), nullable=True)
    shortTermInvestments = Column(Integer(), nullable=True)
    netReceivables = Column(Integer(), nullable=True)
    longTermDebt = Column(Integer(), nullable=True)
    inventory = Column(Integer(), nullable=True)
    accountsPayable = Column(Integer(), nullable=True)
    totalPermanentEquity = Column(Integer(), nullable=True)
    additionalPaidInCapital = Column(Integer(), nullable=True)
    commonStockTotalEquity = Column(Integer(), nullable=True)
    preferredStockTotalEquity = Column(Integer(), nullable=True)
    retainedEarningsTotalEquity = Column(Integer(), nullable=True)
    treasuryStock = Column(Integer(), nullable=True)
    accumulatedAmortization = Column(Integer(), nullable=True)
    otherNonCurrrentAssets = Column(Integer(), nullable=True)
    deferredLongTermAssetCharges = Column(Integer(), nullable=True)
    totalNonCurrentAssets = Column(Integer(), nullable=True)
    capitalLeaseObligations = Column(Integer(), nullable=True)
    totalLongTermDebt = Column(Integer(), nullable=True)
    otherNonCurrentLiabilities = Column(Integer(), nullable=True)
    totalNonCurrentLiabilities = Column(Integer(), nullable=True)
    negativeGoodwill = Column(Integer(), nullable=True)
    warrants = Column(Integer(), nullable=True)
    preferredStockRedeemable = Column(Integer(), nullable=True)
    capitalSurplus = Column(Integer(), nullable=True)
    liabilitiesAndShareholderEquity = Column(Integer(), nullable=True)
    cashAndShortTermInvestments = Column(Integer(), nullable=True)
    accumulatedDepreciation = Column(Integer(), nullable=True)
    commonStockSharesOutstanding = Column(Integer(), nullable=True)