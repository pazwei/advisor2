from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class Event(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'events'

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("events", uselist=True), lazy='select')

    date = Column(DateTime(), nullable=False, default = datetime.now())
    name = Column(String(), nullable=True)
    effect = Column(String(), nullable=False)
    description = Column(String(), nullable=True)

    def __repr__(self):
       return f'<Event id={self.id} name={self.name} date={self.date.strftime("%Y-%m-%d")}>'