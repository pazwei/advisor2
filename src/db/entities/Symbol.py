from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class Symbol(ORM.SqlAlchemySchemaBuilder):
    __tablename__ = 'symbols'

    id = Column(String(), primary_key=True)
    assetType = Column(String(), nullable=True)
    name = Column(String(), nullable=True)
    description = Column(String(), nullable=True)
    exchange = Column(String(), nullable=True)
    currency = Column(String(), nullable=True)
    exchange = Column(String(), nullable=True)
    indices = Column(String(), nullable=True) #csv of indices the stock is included in
    ipoDate = Column(String(), nullable=True)
    country = Column(String(), nullable=True)
    sector = Column(String(), nullable=True)
    industry = Column(String(), nullable=True)
    address = Column(String(), nullable=True)
    city = Column(String(), nullable=True)
    tags = Column(String(), nullable=True)
    website = Column(String(), nullable=True)
    primarySicCode = Column(String(), nullable=True)
    fullTimeEmployees = Column(Integer(), nullable=True)
    ceo = Column(String(), nullable=True)
    peers = Column(String(), nullable=True)
    dividendPaymentsPerYear = Column(Integer(), default=0, nullable=True)
    meta = Column(JSON(), nullable=True)
    isDeleted = Column(Boolean(), nullable=True, default=False)

    priority = Column(Integer(), nullable=False, default=-1)
    dateCreated = Column(DateTime(), nullable=False, default = datetime.now())

    digest = Column(JSON(), nullable=True)
    sustainability = Column(JSON(), nullable=True)

    def __repr__(self):
       return f'<Symbol id={self.id} name={self.name}>'
