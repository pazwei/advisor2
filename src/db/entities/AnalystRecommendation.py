from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class AnalystRecommendation(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'analystRecommendation'

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("analyst_recommendations", uselist=True), lazy='select')

    date = Column(DateTime(), nullable=True)
    firm = Column(String(), nullable=True)
    fromGrade = Column(String(), nullable=True)
    toGrade = Column(String(), nullable=True)
    gradeAction = Column(String(), nullable=True)
    targetPriceHigh = Column(Float(), nullable=True)
    targetPriceLow = Column(Float(), nullable=True)