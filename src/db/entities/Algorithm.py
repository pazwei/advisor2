from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class Algorithm(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'algorithms'

    name = Column(String(), nullable=True)
    execution_timeframe = Column(Integer(), nullable=True)
    formula = Column(String(), nullable=True)
    stockFilter = Column(String(), nullable=True)
    stockFilter_value = Column(String(), nullable=True)
    trendFilter = Column(String(), nullable=True)