from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class Ipo(ORM.SqlAlchemySchemaBuilder):
    __tablename__ = 'ipos'

    id = Column(String(), primary_key=True)
    dealId = Column(String(), nullable=False)
    dateCreated = Column(DateTime(), nullable=False, default = datetime.now())
    companyName = Column(String(), nullable=False)
    exchange = Column(String(), nullable=False)
    proposedSharePriceLow = Column(Float(), nullable=False) #proposedSharePrice=14.00-16.00
    proposedSharePriceHigh = Column(Float(), nullable=False) #proposedSharePrice=14.00-16.00
    openingPrice = Column(Float(), nullable=False)
    sharesOffered = Column(Integer(), nullable=False) #8,900,000
    sharedOutstanding = Column(Integer(), nullable=True)
    ipoDate = Column(DateTime(), nullable=False) #01/29/2021
    valueOfSharedOffered = Column(Integer(), nullable=False)
    employees = Column(String(), nullable=True) #4 (as of 01/20/2021)
    website = Column(String(), nullable=True)
    ceo = Column(String(), nullable=True)
    description = Column(String(), nullable=True)