from datetime import datetime
import hashlib

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class Article(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'articles'

    feedId = Column(Integer(), ForeignKey('feeds.id'), nullable=True)
    feed = relationship("Feed", backref=backref("articles", uselist=False), lazy='select') #back_populates="articles")

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("articles", uselist=True), lazy='select')

    link = Column(String(), nullable=False)
    title = Column(String(), nullable=False)
    description = Column(String(), nullable=True)
    source = Column(String(), nullable=True)

    hash = Column(String(), nullable=False) #since media outlets have referers, where each referer has it's own query param uuid for tracking, I need to make sure the description content is unique 
    naiveSentiment = Column(String(), nullable=True)
    sentiment = Column(String(), nullable=True)
    sentimentScore = Column(Float(), nullable=True)
    sentimentConfidence = Column(Float(), nullable=True)
    keywords = Column(String(), nullable=True)

    publishedAt = Column(DateTime(), nullable=False, default = datetime.now())

    def join_texts(self):
        return f'{self.title}{"." if self.title[-1] != "." else ""} {self.description}'

    def create_hash(self):
        return hashlib.sha256(bytes(self.join_texts(), 'utf-8')).hexdigest()

    def __repr__(self):
       return f'<Article id={self.id} symbol={self.symbolId} naiveSentiment={self.naiveSentiment}>'

    # def hash(self):
    #     text = self.title
    #     if self.description != None:
    #         text = text + self.description
    #     h = hashlib.md5(text.encode())
    #     self.hash = h.hexdigest()

    # def join_texts(self):
    #     return f'{self.title}{"." if self.title[-1] != "." else ""} {self.description}'

