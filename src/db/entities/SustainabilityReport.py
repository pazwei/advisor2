from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class SustainabilityReport(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'sustainabilityReports'

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("sustainabilityreports", uselist=True), lazy='select')
    
    year = Column(Integer(), nullable=True)
    quater = Column(Integer(), nullable=True)
    palmOil = Column(Boolean(), nullable=True)
    controversialWeapons = Column(Boolean(), nullable=True)
    gambling = Column(Boolean(), nullable=True)
    socialScore = Column(Float(), nullable=True)
    nuclear = Column(Boolean(), nullable=True)
    furLeather = Column(Boolean(), nullable=True)
    alcoholic = Column(Boolean(), nullable=True)
    gmo  = Column(Boolean(), nullable=True)
    catholic = Column(Boolean(), nullable=True)
    socialPercentile = Column(Float(), nullable=True)
    peerCount = Column(Integer(), nullable=True)
    governanceScore = Column(Float(), nullable=True)
    environmentPercentile = Column(Float(), nullable=True)
    animalTesting  = Column(Boolean(), nullable=True)
    tobacco  = Column(Boolean(), nullable=True)
    totalEsg = Column(Float(), nullable=True)
    highestControversy = Column(Integer(), nullable=True)
    esgPerformance = Column(String(), nullable=True)
    coal = Column(Boolean(), nullable=True)
    pesticides = Column(Boolean(), nullable=True)
    adult = Column(Boolean(), nullable=True)
    percentile = Column(Float(), nullable=True)
    peerGroup = Column(String(), nullable=True)
    smallArms = Column(Boolean(), nullable=True)
    environmentScore = Column(Integer(), nullable=True)
    governancePercentile = Column(Float(), nullable=True)
    militaryContract = Column(Boolean(), nullable=True)