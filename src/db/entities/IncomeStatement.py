from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class IncomeStatement(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'incomestatements'

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("incomestatements", uselist=True), lazy='select')

    term = Column(String(), nullable=False)

    fiscalDateEnding = Column(DateTime(), nullable=True)
    reportedCurrency = Column(String(), nullable=True)
    totalRevenue = Column(Integer(), nullable=True)
    totalOperatingExpense = Column(Integer(), nullable=True)
    costOfRevenue = Column(Integer(), nullable=True)
    grossProfit = Column(Integer(), nullable=True)
    ebit = Column(Integer(), nullable=True)
    netIncome = Column(Integer(), nullable=True)
    researchAndDevelopment = Column(Integer(), nullable=True)
    effectOfAccountingCharges = Column(Integer(), nullable=True)
    incomeBeforeTax = Column(Integer(), nullable=True)
    minorityInterest = Column(Integer(), nullable=True)
    sellingGeneralAdministrative = Column(Integer(), nullable=True)
    otherNonOperatingIncome = Column(Integer(), nullable=True)
    operatingIncome = Column(Integer(), nullable=True)
    otherOperatingExpense = Column(Integer(), nullable=True)
    interestExpense = Column(Integer(), nullable=True)
    taxProvision = Column(Integer(), nullable=True)
    interestIncome = Column(Integer(), nullable=True)
    netInterestIncome = Column(Integer(), nullable=True)
    extraordinaryItems = Column(Integer(), nullable=True)
    nonRecurring = Column(Integer(), nullable=True)
    otherItems = Column(Integer(), nullable=True)
    incomeTaxExpense = Column(Integer(), nullable=True)
    totalOtherIncomeExpense = Column(Integer(), nullable=True)
    discontinuedOperations = Column(Integer(), nullable=True)
    netIncomeFromContinuingOperations = Column(Integer(), nullable=True)
    netIncomeApplicableToCommonShares = Column(Integer(), nullable=True)
    preferredStockAndOtherAdjustments = Column(Integer(), nullable=True)