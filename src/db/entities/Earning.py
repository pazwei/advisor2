from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class Earning(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'earnings'

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("earnings", uselist=True), lazy='select')

    term = Column(String(), nullable=False)

    fiscalDateEnding = Column(DateTime(), nullable = True)
    reportedDate = Column(DateTime(), nullable = True)
    reportedEPS = Column(Float(), nullable = True)
    estimatedEPS = Column(Float(), nullable = True)
    surprise = Column(Float(), nullable = True)
    surprisePercentage = Column(Float(), nullable = True)