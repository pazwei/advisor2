from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class CashFlow(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'cashflows'

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("cashflows", uselist=True), lazy='select')

    term = Column(String(), nullable=False)

    fiscalDateEnding = Column(DateTime(), nullable=False)
    reportedCurrency = Column(String(), nullable=False)
    investments = Column(Integer(), nullable=True)
    changeInLiabilities = Column(Integer(), nullable=True)
    cashflowFromInvestment = Column(Integer(), nullable=True)
    otherCashflowFromInvestment = Column(Integer(), nullable=True)
    netBorrowings = Column(Integer(), nullable=True)
    cashflowFromFinancing = Column(Integer(), nullable=True)
    otherCashflowFromFinancing = Column(Integer(), nullable=True)
    changeInOperatingActivities = Column(Integer(), nullable=True)
    netIncome = Column(Integer(), nullable=True)
    changeInCash = Column(Integer(), nullable=True)
    operatingCashflow = Column(Integer(), nullable=True)
    otherOperatingCashflow = Column(Integer(), nullable=True)
    depreciation = Column(Integer(), nullable=True)
    dividendPayout = Column(Integer(), nullable=True)
    stockSaleAndPurchase = Column(Integer(), nullable=True)
    changeInInventory = Column(Integer(), nullable=True)
    changeInAccountReceivables = Column(Integer(), nullable=True)
    changeInNetIncome = Column(Integer(), nullable=True)
    capitalExpenditures = Column(Integer(), nullable=True)
    changeInReceivables = Column(Integer(), nullable=True)
    changeInExchangeRate = Column(Integer(), nullable=True)
    changeInCashAndCashEquivalents = Column(Integer(), nullable=True)