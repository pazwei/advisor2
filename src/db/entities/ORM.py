from datetime import datetime

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON

#this is the orm's singleton for creating schema, it must be static or module level variable
SqlAlchemySchemaBuilder = declarative_base()

class BaseSymbolChildEntity():
    id = Column(Integer(), primary_key=True)
    dateCreated = Column(DateTime, nullable=False, default = datetime.now())

    def __repr__(self):
        return f'<{self.__class__.__name__} id={self.id} dateCreated={self.dateCreated.strftime("%Y-%m-%d")}>'
