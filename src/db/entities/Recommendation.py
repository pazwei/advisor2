from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class Recommendation(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'recommendations'

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("recommendations", uselist=True), lazy='select')

    algorithmId = Column(Integer(), ForeignKey('algorithms.id'), nullable = True)
    algorithm = relationship("Algorithm", backref=backref("recommendations", uselist=True), lazy='select')

    # algorithm = ForeignKeyField(Algorithm, related_name='recommendations')
    date = Column(DateTime(), nullable=False, default = datetime.now())

    entryPrice = Column(Float(), nullable=True)
    exitPrice = Column(Float(), nullable=True)
    stopLoss = Column(Float(), nullable=True)
    execution_date = Column(DateTime(), nullable=True)
    dueDate = Column(DateTime(), nullable=False)
    outcome = Column(String(), nullable=True)
    comments = Column(String(), nullable=True)
    profit = Column(Float(), nullable=True)