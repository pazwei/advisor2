from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class Feed(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'feeds'

    type = Column(String(), nullable=False) # RSS|ATOM|TWITTER|REDDIT
    url = Column(String(), unique=True)
    name = Column(String(), unique=True)
    isActive = Column(Boolean(), nullable=False, default=True)
    
    class FEED_TYPE:
        RSS = 'RSS'
        ATOM = 'ATOM'
        TWITTER = 'TWITTER'
        REDDIT = 'REDDIT'
        
    def __repr__(self):
       return f'<Feed id={self.id} symbolId={self.type} name={self.name}>'