from datetime import datetime
import hashlib

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class Post(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'posts'

    feedId = Column(Integer(), ForeignKey('feeds.id'), nullable=True)
    feed = relationship("Feed", backref=backref("posts", uselist=False), lazy='select')

    symbolIds = Column(String(), nullable = False) #will contain csv of found symbols

    link = Column(String(), nullable=False)
    title = Column(String(), nullable=False)
    description = Column(String(), nullable=True)
    author = Column(String(), nullable=True)
    hash = Column(String(), nullable=False) #since media outlets have referers, where each referer has it's own query param uuid for tracking, I need to make sure the description content is unique 
    naiveSentiment = Column(String(), nullable=True)

    ups = Column(Integer(), nullable=True)
    downs = Column(Integer(), nullable=True)
    awards = Column(Integer(), nullable=True)
    comments = Column(Integer(), nullable=True)

    publishedAt = Column(DateTime(), nullable=False, default = datetime.now())

    def join_texts(self):
        return f'{self.title}{"." if self.title[-1] != "." else ""} {self.description}'

    def create_hash(self):
        return hashlib.sha256(bytes(self.join_texts(), 'utf-8')).hexdigest()
        
    def __repr__(self):
       return f'<Post id={self.id} symbolIds={self.symbolIds} naiveSentiment={self.naiveSentiment}>'