from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class Dividend(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'dividends'

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("dividends", uselist=True), lazy='select')

    announceDate = Column(DateTime(), nullable=False)
    exDate = Column(DateTime(), nullable=False)
    paymentDate = Column(DateTime(), nullable=False)
    payment = Column(Float(), nullable=False)