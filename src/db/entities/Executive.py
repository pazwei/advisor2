from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey

import db.entities.ORM as ORM

class Executive(ORM.SqlAlchemySchemaBuilder, ORM.BaseSymbolChildEntity):
    __tablename__ = 'executives'

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("executives", uselist=True), lazy='select')

    fullname = Column(String(), nullable=False)
    position = Column(String(), nullable=False)
    bio = Column(String(), nullable=False)
    compensation = Column(String(), nullable=False)
    age = Column(String(), nullable=False)
    since = Column(String(), nullable=False) #check to see if it can be an year or something like that
    retired = Column(Boolean(), nullable=False)
    retiredDate = Column(DateTime(), nullable=False)