#!/usr/bin/env python3
import datetime
import itertools

import pydash
from sqlalchemy import func
from sqlalchemy import distinct

from db.entities.Algorithm import Algorithm
from db.entities.AnalystRecommendation import AnalystRecommendation
from db.entities.Article import Article
from db.entities.BalanceSheet import BalanceSheet
from db.entities.Cashflow import CashFlow
from db.entities.Record import Record
from db.entities.Dividend import Dividend
from db.entities.Earning import Earning
from db.entities.Event import Event
from db.entities.Executive import Executive
from db.entities.Feed import Feed
from db.entities.IncomeStatement import IncomeStatement
from db.entities.Ipo import Ipo
from db.entities.Overview import Overview
from db.entities.Post import Post
from db.entities.Recommendation import Recommendation
from db.entities.Record import Record
from db.entities.SustainabilityReport import SustainabilityReport
from db.entities.Symbol import Symbol

from db.context import get_session
import utils.cli as cli
import classifier.classify as classifier

_session = get_session()

def count_rows(entity):
    query = f'_session.query({entity}).count()'
    res = eval(query)
    return res

def get_distinct_symbolIds(entity):
    query = f'list(_session.query(distinct({entity}.symbolId)).all())'
    res = [i[0] for i in eval(query)]
    return res

def get_oldest_record(entity, age_field):
    query = f'_session.query({entity}.{age_field}).order_by({entity}.{age_field}.desc()).first()'
    res = eval(query)
    return res[0]

def get_symbols_needing_entity_refresh(entity_name, age_date_field, days_from_last_record_threshold):

    """
    Entity			        -1	1	2
    AnalystRecommendaton	X	Y	Y
    Articles		        X	X	Y
    BalanceSheet		    X	Y	Y
    CashFlow		        X	Y	Y
    Dividend		        X	Y	Y
    Earning			        X	Y	Y
    IncomeStatement		    X	Y	Y
    Record			        X	Y	Y
    Sustainability		    X	Y	Y
    Ipo			
    Symbol
    """
    # Get symbols that should have entity data according to their priority and the table above. 
    symbols_that_should_have_entity_data = []
    if entity_name in ['AnalystRecommendation','BalanceSheet','CashFlow','IncomeStatement','Dividend','Earning','Record']:
        symbols_that_should_have_entity_data = list(_session.query(Symbol).filter(Symbol.priority >= 0).all())
    elif entity_name in ['Article']:
        symbols_that_should_have_entity_data = list(_session.query(Symbol).filter(Symbol.priority == 2).all())
    
    # Get existing symbol's that needs to be updated according to their last entity 
    dynamic_query = f'_session.query({entity_name}, func.max({entity_name}.{age_date_field}).label("age")).group_by({entity_name}.symbolId).all()'
    existing_entities_grouped_by_symbolId = list(eval(dynamic_query))
    symbols_that_have_aged_entity_data = [entity.symbol for entity,date in existing_entities_grouped_by_symbolId if (datetime.datetime.now() - date).days >= days_from_last_record_threshold]

    symbolIds_that_dont_have_entity_data = pydash.arrays.difference([i.id for i in symbols_that_should_have_entity_data], [i.id for i in symbols_that_have_aged_entity_data])
    symbols_that_dont_have_entity_data = list(_session.query(Symbol).filter(Symbol.id.in_(symbolIds_that_dont_have_entity_data)).all())
    
    #Get data for symbols that don't have any data + symbols with aged data
    res = symbols_that_dont_have_entity_data + symbols_that_have_aged_entity_data
    #TODO: sort by priority priority desc and remove dupes
    return res

def get_all_articles(from_date):
    return list(_session.query(Article).filter(Article.publishedAt > from_date).order_by(Article.publishedAt.asc()).all())

def get_all_feeds():
    return list(_session.query(Feed).filter(Feed.isActive == True).all())

def get_all_posts(from_date):
    return list(_session.query(Post).filter(Post.publishedAt > from_date).order_by(Post.publishedAt.asc()).all())

def bulk_insert(entities):
    chunks = pydash.arrays.chunk(entities, size=100)
    for chunk in chunks:
        cli.success('Queries',f'BulkInsert rows={len(chunk)}')
        _session.bulk_save_objects(chunk)
        _session.commit()
        # _session.remove() #check if it makes errors on any entity

def bulk_delete(entities):
    entity_name = entities[0].__class__.__name__
    chunks = pydash.arrays.chunk(entities, size=100)
    for chunk in chunks:
        cli.danger('Queries',f'BulkDelete rows={len(chunk)}')
        ids = [item.id for item in chunk]
        table = eval(f'{entity_name}.__table__')
        delete_query = eval(f'table.delete().where({entity_name}.id.in_(ids))')
        _session.execute(delete_query)
        _session.commit()

def bulk_delete_by_ids(entity_name, ids):
    chunks = pydash.arrays.chunk(ids, size=100)
    for ids in chunks:
        cli.danger('Queries',f'BulkDeleteByIds rows={len(ids)}')
        table = eval(f'{entity_name}.__table__')
        delete_query = eval(f'{table}.delete().where({entity_name}.id.in_(ids))')
        _session.execute(delete_query)
        _session.commit()

def bulk_update(entities):
    entity_name = entities[0].__class__.__name__
    fields = [f for f in dir(entities[0]) if not f.startswith('_') and not f.startswith('__') and str(type(getattr(entities[0], f)).__name__).lower() in ['int','float','str','datetime','json']]
    chunks = pydash.arrays.chunk(entities, size=100)
    for chunk in chunks:
        cli.success('Queries',f'BulkUpdate rows={len(chunk)}')
        for item in chunk:
            if item is not None:
                obj = eval(f'_session.query({entity_name}).filter({entity_name}.id == item.id).first()')
                if obj is not None:
                    for field in fields:
                        value = getattr(item,field)
                        setattr(obj, field, value)
        _session.commit()
        # _session.remove()

def get_last_record(symbol_id):
    return _session.query(Record).filter(Record.symbolId == symbol_id).order_by(Record.date.desc()).first()

def get_records_from_date(symbol_id, from_date, to_date = None):
    return list(_session.query(Record).filter((Record.symbolId == symbol_id) & (Record.date >= from_date) & (to_date if to_date is not None else datetime.datetime.now())).order_by(Record.date.asc()).all())

def get_lastest_records(symbol_id, limit):
    return list(_session.query(Record).filter(Record.symbolId == symbol_id).order_by(Record.date.desc()).limit(limit).all())

def get_all_latest_records(limit):
    rows = _session.query(Record).order_by(Record.symbolId, Record.date.desc()).all()
    groups = itertools.groupby(rows, lambda r: r.symbolId)
    res = {}
    for symbolId, group in groups:
        res[symbolId] = list(group)[:limit]
    return res

#Use the Yahoo api for caldendar?
def get_all_events(symbol_id):
    return list(_session.query(Event).filter(Event.symbolId == symbol_id).order_by(Event.date.asc()).all())

def get_all_symbols():
    return list(_session.query(Symbol).all())

def get_all_symbolIds():
    return [i[0] for i in list(_session.query(Symbol.id).all())]

def get_all_posts_hashed():
    return [i[0] for i in list(_session.query(Post.hash).all())]

def get_symbols_by_priority(priority):
    return list(_session.query(Symbol).filter(Symbol.priority == priority).all())

def get_last_balancesheet(symbolId, term):
    return _session.query(BalanceSheet).filter((BalanceSheet.symbolId == symbolId) & (BalanceSheet.term == term)).order_by(BalanceSheet.fiscalDateEnding.desc()).first() # & (IncomeStatement.symbol.assetType == 'Stock')

def get_last_incomestatement(symbolId, term):
    return _session.query(IncomeStatement).filter((IncomeStatement.symbolId == symbolId) & (IncomeStatement.term == term)).order_by(IncomeStatement.fiscalDateEnding.desc()).first() # & (IncomeStatement.symbol.assetType == 'Stock')

def get_last_earning(symbolId, term):
    return _session.query(Earning).filter((Earning.symbolId == symbolId) & (Earning.term == term)).order_by(Earning.fiscalDateEnding.desc()).first() # & (IncomeStatement.symbol.assetType == 'Stock')

def get_last_cashflow(symbolId, term):
    return _session.query(CashFlow).filter((CashFlow.symbolId == symbolId)  & (CashFlow.term == term)).order_by(CashFlow.fiscalDateEnding.desc()).first() # & (IncomeStatement.symbol.assetType == 'Stock')

def get_latest_articles(symbolId, limit = 10):
    articles = list(_session.query(Article).filter(Article.symbolId == symbolId).order_by(Article.publishedAt.desc()).limit(limit).all())
    return articles

def get_ipos_deal_ids():
    return list(_session.query(Ipo).order_by(Ipo.ipoDate.desc()).all())

def get_last_substainability_report(symbolId):
    return _session.query(SustainabilityReport).filter(SustainabilityReport.symbolId == symbolId).order_by(SustainabilityReport.year.desc(), SustainabilityReport.quater.desc()).first()

def get_latest_balancesheets(symbolId, term, amount):
    return list(_session.query(BalanceSheet)\
    .filter((BalanceSheet.symbolId == symbolId) & (BalanceSheet.term == term) & (BalanceSheet.symbol.assetType == 'Stock'))\
    .order_by(BalanceSheet.fiscalDateEnding.desc())\
    .limit(amount).all())

def get_latest_incomestatements(symbolId, term, amount):
    return list(_session.query(IncomeStatement)\
    .filter((IncomeStatement.symbolId == symbolId) & (IncomeStatement.term == term) & (IncomeStatement.symbol.assetType == 'Stock'))\
    .order_by(IncomeStatement.fiscalDateEnding.desc())\
    .limit(amount).all())

def get_latest_cashflows(symbolId, term, amount):
    return list(_session.query(CashFlow)\
    .filter((CashFlow.symbolId == symbolId) & (CashFlow.term == term) & (CashFlow.symbol.assetType == 'Stock'))\
    .order_by(CashFlow.fiscalDateEnding.desc())\
    .limit(amount).all())

def get_latest_earnings(symbolId, term, amount):
    return list(_session.query(Earning)\
    .filter((Earning.symbolId == symbolId) & (Earning.term == term) & (Earning.symbol.assetType == 'Stock'))\
    .order_by(Earning.fiscalDateEnding.desc())\
    .limit(amount).all())

def get_latest_records(symbolId, amount):
    return list(_session.query(Record)\
    .filter(Record.symbolId == symbolId)\
    .order_by(Record.date.desc()).limit(amount))

def get_latest_recommendations(symbolId, amount):
    return list(_session.query(AnalystRecommendation)\
    .filter(AnalystRecommendation.symbolId == symbolId)\
    .order_by(AnalystRecommendation.date.desc()).limit(amount).all())

def get_last_items_by_entity(name):
    if name == 'Symbol':
        return _session.query(Symbol).order_by(Symbol.dateCreated.desc()).first()
    elif name == 'Ipo':
        return _session.query(Ipo).order_by(Ipo.dateCreated.desc()).first()

def get_last_items_by_symbolId(name, symbolId):
    if name == 'BalanceSheet': # array of one
        return get_last_balancesheet(symbolId, 'QUATER')
    elif name == 'IncomeStatement': # array of one
        return get_last_incomestatement(symbolId, 'QUATER')
    elif name == 'CashFlow': # array of one
        return get_last_cashflow(symbolId, 'QUATER')
    elif name == 'Earning': # array of one
        return get_last_earning(symbolId, 'QUATER')
    elif name == 'Record': # array of one
        return get_last_record(symbolId)
    elif name == 'AnalystRecommendation': # array of 10
        res = get_latest_recommendations(symbolId, 5)
        return None if (res is None or len(res) == 0) else res[0]
    elif name == 'Article': # array of 10
        res = get_latest_articles(symbolId, 10)
        return None if (res is None or len(res) == 0) else res[0]

def get_all_balance_sheets(term = 'QUATER'):
    items = list(_session.query(BalanceSheet).filter(BalanceSheet.term == term).order_by(BalanceSheet.symbolId).all()) #{'AAPL':[], 'MSFT':[]}
    grouped = {k: sorted(list(g), key=lambda k: k.fiscalDateEnding, reverse=False) for k, g in itertools.groupby(items, lambda t: t.symbolId)} #__get_fiscalDateEnding
    return grouped

def get_all_analyst_recommendations(to_date = datetime.datetime.now(), from_date = datetime.datetime(datetime.datetime.now().year - 1, 1, 1)):
    items = list(_session.query(AnalystRecommendation).filter(AnalystRecommendation.date >= from_date).order_by(AnalystRecommendation.symbolId, AnalystRecommendation.date).all()) #{'AAPL':[], 'MSFT':[]}
    grouped = {k: sorted(list(g), key=lambda k: k.date, reverse=False) for k, g in itertools.groupby(items, lambda t: t.symbolId)}
    return grouped


def get_all_cashflows(term = 'QUATER'):
    items = list(_session.query(CashFlow).filter(CashFlow.term == term).order_by(CashFlow.symbolId).all()) #{'AAPL':[], 'MSFT':[]}
    grouped = {k: sorted(list(g), key=lambda k: k.fiscalDateEnding, reverse=False) for k, g in itertools.groupby(items, lambda t: t.symbolId)}
    return grouped

def get_all_income_statements(term = 'QUATER'):
    items = list(_session.query(IncomeStatement).filter(IncomeStatement.term == term).order_by(IncomeStatement.symbolId).all()) #{'AAPL':[], 'MSFT':[]}
    grouped = {k: sorted(list(g), key=lambda k: k.fiscalDateEnding, reverse=False) for k, g in itertools.groupby(items, lambda t: t.symbolId)}
    return grouped

def get_all_earnings(term = 'QUATER'):
    items = list(_session.query(Earning).filter(Earning.term == term).order_by(Earning.symbolId).all())
    grouped = {k: sorted(list(g), key=lambda k: k.fiscalDateEnding, reverse=False) for k, g in itertools.groupby(items, lambda t: t.symbolId)} #{'AAPL':[], 'MSFT':[]}
    return grouped

def get_earnings(symbolId, term = 'QUATER'):
    return list(_session.query(Earning).filter((Earning.symbolId == symbolId) & (Earning.term == term)).order_by(Earning.fiscalDateEnding).all())

def get_all_articles_grouped():
    items = list(_session.query(Article).order_by(Article.symbolId).all())
    grouped = {k: sorted(list(g), key=lambda k: k.publishedAt, reverse=False) for k, g in itertools.groupby(items, lambda t: t.symbolId)} #{'AAPL':[], 'MSFT':[]}
    return grouped

def get_all_records(reverse=False):
    items = list(_session.query(Record).order_by(Record.symbolId).all())
    grouped = {k: sorted(list(g), key=lambda k: k.date, reverse=reverse) for k, g in itertools.groupby(items, lambda t: t.symbolId)} #{'AAPL':[], 'MSFT':[]}
    return grouped

def get_all_dividends():
    items = list(_session.query(Dividend).filter(Dividend.payment > 0).order_by(Dividend.symbolId).all())
    grouped = {k: sorted(list(g), key=lambda k: k.exDate, reverse=False) for k, g in itertools.groupby(items, lambda t: t.symbolId)} #{'AAPL':[], 'MSFT':[]} 
    return grouped

def get_dividends(symbolId):
    return list(_session.query(Dividend).filter((Dividend.symbolId == symbolId)).order_by(Dividend.exDate).all())

def update_symbols_with_sustainability_report(reports):
    for report in reports:
        symbol_to_update = _session.query(Symbol).filter(Symbol.id == report['symbolId']).first()
        symbol_to_update.SustainabilityReport = report
    _session.commit()

def get_dividendPaymentsPerYear(symbolIds):
    data = list(_session.query(Symbol).filter(Symbol.id.in_(symbolIds)).all())
    return {item.id:item.dividendPaymentsPerYear for item in data}

def update_dividendPaymentsPerYear(symbolId, amount):
    symbol_to_update = _session.query(Symbol).filter(Symbol.id == symbolId).first()
    symbol_to_update.dividendPaymentsPerYear = amount
    _session.commit()

def get_aggregate_post_metrics(days_ago):
    res = {}
    posts = list(_session.query(Post).filter(Post.publishedAt >= datetime.datetime.now() - datetime.timedelta(days = days_ago)))
    syms = {k.id:k for k in get_all_symbols()}
    all_records = get_all_records(True)
    for post in posts:
        symbolIds = [s for s in post.symbolIds.split(',') if s != '']
        for symbolId in symbolIds:
            if symbolId not in res.keys():
                res[symbolId] = {
                        'symbol': syms[symbolId],
                        'records': all_records[symbolId][:days_ago] if symbolId in all_records.keys() else None, #get_latest_records(symbolId,days_ago),
                        'count': 1,
                        'ups':post.ups,
                        'downs':post.downs,
                        'comments':post.comments,
                        'awards':post.awards,
                        'positives': 1 if post.naiveSentiment == 'Positive' else 0,
                        'negatives': 1 if post.naiveSentiment == 'Negative' else 0,
                        'unknowns': 1 if post.naiveSentiment == 'Unknown' else 0
                    }
            else:
                res[symbolId]['count'] += 1
                res[symbolId]['ups'] += post.ups
                res[symbolId]['downs'] += post.downs
                res[symbolId]['comments'] += post.comments
                res[symbolId]['awards'] += post.awards
                res[symbolId]['positives'] += 1 if post.naiveSentiment == 'Positive' else 0
                res[symbolId]['negatives'] += 1 if post.naiveSentiment == 'Negative' else 0
                res[symbolId]['unknowns'] += 1 if post.naiveSentiment == 'Unknown' else 0

    return res