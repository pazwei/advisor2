#!/usr/bin/env python3
import signal
import sys
import argparse
import datetime

import classifier.classify as naive_classifer
import quotes.quote as quote
import utils.cli as cli
from importers.BaseImporter import BaseImporter

###########
# globals #
###########
function = ''

def graceful_exit(signal, frame):
    print(quote.get())
    sys.exit(0)

def set_args():
    global function

    # https://docs.python.org/3/library/argparse.html
    parser = argparse.ArgumentParser(description='Maintenance')
    parser.add_argument("--fn", "--function", help="Function to execute. 'sentiment' for sentiment recalculate, 'status' for db status,", type=str, default='status', required=True)
    
    args = parser.parse_args()
    function = args.fn

def valid_date(s:str)->datetime.datetime:
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)

def main():
    global function

    if function == 'sentiment':
        naive_classifer.recalculate_naive_articles_sentiment()
    elif function == 'status':
        BaseImporter.get_db_overview()
    elif function == 'extract':
        naive_classifer.recalculate_symbolId_extraction()
    elif function == 'logs':
        # use glob, delete 2 weeks old logs
        # Delete old logs
        pass
    elif function == 'record':    
        # Delete old records that have no use
        pass 
    
if __name__ == '__main__':
    signal.signal(signal.SIGINT, graceful_exit)
    set_args()
    main()