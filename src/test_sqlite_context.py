import unittest

from db.context import get_session

#https://docs.python.org/3/library/unittest.html

#python -m unittest discover //command to discover tests named "*_test.py"

#conventions: class should start with the substring "test", test metohd also need to start with "test_"
class TestSqliteContext(unittest.TestCase):

    def setUp(self) -> None:
        return super().tearDown()

    def tearDown(self) -> None:
        return super().tearDown()

    def test_sum(self):
        self.assertEqual(1+1,2) #sum([1, 2, 3]), 6, "Should be 6")

    def test_sum_tuple(self):
        self.assertEqual(sum((1, 2, 2)), 6, "Should be 6")

if __name__ == '__main__':
    unittest.main()