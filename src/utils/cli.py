from os import system, name
import re
import datetime
import functools

from colorama import init, Fore, Back, Style

init(autoreset=True) #init colorama

def success(caller, message):
    formatted_message = f'[{datetime.datetime.now().strftime("%H:%M:%S")} {caller}] {message}'
    print(Fore.GREEN + formatted_message)

def warning(caller, message):
    formatted_message = f'[{datetime.datetime.now().strftime("%H:%M:%S")} {caller}] {message}'
    print(Fore.YELLOW + formatted_message)

def danger(caller, message):
    formatted_message = f'[{datetime.datetime.now().strftime("%H:%M:%S")} {caller}] {message}'
    print(Back.RED + Fore.WHITE + formatted_message)

def normal(caller, message):
    formatted_message = f'[{datetime.datetime.now().strftime("%H:%M:%S")} {caller}] {message}'
    print(Fore.WHITE + formatted_message)

def section(section_title, color = 'normal'):
    rows = []
    rows.append('┌' + '─' * (len(section_title) + 2) + '┐')
    rows.append('│ ' + section_title + ' │')
    rows.append('└' + '─' * (len(section_title) + 2) + '┘')

    if color == 'success':
        print(Fore.GREEN + '\n'.join(rows))
    elif color == 'warning':
        print(Fore.YELLOW + '\n'.join(rows))
    elif color == 'danger':
        print(Back.RED + Fore.WHITE + '\n'.join(rows))
    else:
        print(Fore.WHITE + '\n'.join(rows))


def clear():
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')

def print_table(rows = [['aaaaaaaaaa', 'bbbbbbb', 'ccc'],['1','2','3'],['4','5','6'],['7','8','999999999']], title = None, show_seperator_line = False, show_row_number = True, show_total = True, max_cell_length = 25):
        #https://theasciicode.com.ar/extended-ascii-code/macron-symbol-ascii-code-238.html
        ##¯ ┴ ─ ┼ │ ┴ ┬ └ ┐ ┤ ┘ ┌ # print('─' * len(headers))  #¯ ┴ ─ ┼ │ ┴ ┬ └ ┐ ┤ ┘ ┌ ┤ ├
        
        #repeat divider for all cells
        for index,row in enumerate(rows):
            if '-DIVIDER-' in row:
                rows[index] = ['-DIVIDER-' for _ in range(len(rows[0]))]
                show_row_number = False

        if show_row_number:
            for index, row in enumerate(rows):
                row = row.insert(0, '#' if index == 0 else str(index))
                
        headers = rows[0]
        rows = rows[1:]

        for index, header in enumerate(headers):
            headers[index] = header.ljust(max([len(header)] + [len(str(row[index])) if len(str(row[index])) <= max_cell_length else max_cell_length for row in rows]))

        title_row = '│ ' + ' │ '.join(headers) + ' │'
        
        print('')
        
        if title != None:
            print(Fore.GREEN + title)
        
        print('┌' + '┬'.join(['─' * (len(header) + 2) for header in headers]) + '┐') 
        print(title_row)
        print('├' + '┼'.join(['─' * (len(header) + 2) for header in headers]) + '┤')

        data_rows = []
        seperator_row = '├' + '┼'.join(['─' * (len(header) + 2) for header in headers]) + '┤\n'

        for row in rows:
            if '-DIVIDER-' not in row:
                padded = '│ ' +  ' │ '.join([(str(cell) if len(str(cell)) <= max_cell_length else str(cell)[0:max_cell_length-2] + '..').ljust(len(headers[cell_index])) for cell_index, cell in enumerate(row)]) + ' │\n'
            else:
                padded = seperator_row
            data_rows.append(padded)

        # seperator_row = '├' + '┼'.join(['─' * (len(header) + 2) for header in headers]) + '┤\n'
        formated_rows = seperator_row.join(data_rows) if show_seperator_line else ''.join(data_rows)

        print(formated_rows[:-1]) #remove \n from last seperator / row

        print('└' + '┴'.join(['─' * (len(header) + 2) for header in headers]) + '┘')

        if show_total:
            print(f'Total: {len(rows)}')
        
        print('')

def csv(rows, output_file_path = None, delimiter = ','):
    acc = []
    for row in rows:
        acc.append(delimiter.join(row))
    res = '\n'.join(acc)
    if output_file_path is None:
        print(res)
    else:
        file = open(output_file_path, 'w')
        file.write(res)
        file.close()

def text_from_camel_case(field_name):
    txt = re.sub("([a-z])([A-Z])","\g<1> \g<2>", field_name)
    return txt.capitalize()

def percent(num, completing = False):
    if num == 'N/A' or num == None:
        return num
    if completing:
        return f'{(1 - num):.2%}'
    return f'{num:.2%}' #f'{round(num * 100, 2)} %'

def decimal_reduce(num, divider = 1000000):
    # https://mkaz.blog/code/python-string-format-cookbook/
    if num == 'N/A' or num == None:
        return num
    if divider == 1000000:
        return f'$M {int(num / divider):,}'
    elif divider == 1000:
        return f'$K {int(num / divider):,}'
    elif divider == 1000000000:
        return f'$B {int(num / divider):,}'

def none_to_zero(num):
    return 0 if num is None else num

def formula(item, func, output_as = 'percent', completing = False):
    fields = [field for field in dir(item) if not field.startswith('_') and type(getattr(item, field)) in ['float','int']]
    try:
        for field in fields:
            setattr(item, field, none_to_zero(getattr(item, field)))
        res = func(item)
        if output_as == 'percent':
            return percent(res, completing)
        elif type(res).__name__ in ['int','float'] and output_as == '$':
            return f'${rounded(res)}'
        elif type(res).__name__ in ['int','float']:
            return rounded(res)
        else:
            return res
    except:
        return 'N/A'

def rounded(num):
    return round(num,2) if num != 'N/A' else num

if __name__ == '__main__':
    clear()
    danger('CLI','Test')
    warning('CLI','Test')
    success('CLI','Test')
    normal('CLI','Test')
    print_table()
    section('TEST','success')
    csv([['A','B'],['A','B'],['A','B'],['A','B']])