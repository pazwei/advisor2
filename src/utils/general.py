import os

def get_default_list(name = 'tracked_symbols.txt'):
    script_dir = os.path.dirname(__file__)
    abs_file_path = os.path.join(script_dir, '..', 'data', name)
    f = open(abs_file_path, "r")
    res = f.read().split('\n')
    return res

if __name__ == '__main__':
    get_default_list('sp500.txt')