#!/usr/bin/env python3

import time 
import math
import pprint

import config
import mailer

def delay(seconds = 5, verbose = True): 
    def decorator(func):
        def wrapper(*args, **kwargs): 
            if verbose:
                print(f'Delaying {func.__name__} by {seconds} seconds...')
            time.sleep(seconds)
            return func(*args, **kwargs) 
        return wrapper
    return decorator

def time_meter(): 
	def decorator(func):
		def wrapper(*args, **kwargs): 
			begin = time.time()
			func(*args, **kwargs)
			end = time.time()
			print("Total time taken in : ", func.__name__, end - begin) 
		return wrapper
	return decorator

def email_result(subject, is_prod = False):
    def decorator(func):
        def wrapper(*args, **kwargs):
            res = func(*args, **kwargs)
            mailer.send_email(subject, res)
        return wrapper
    return decorator

# def print_return(): 
# 	def decorator(func):
# 		def wrapper(*args, **kwargs): 
# 			res = func(*args, **kwargs)
#             pprint.pprint(res)
# 		return wrapper
# 	return decorator

# @time_meter()
# def factorial(num): 

# 	time.sleep(2) 
# 	print(math.factorial(num)) 

# factorial(10) 


# @delay(5)
# def factorial(num): 
# 	print(math.factorial(num)) 

# if __name__ == '__main__':
#     factorial(10) 
