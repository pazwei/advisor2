import datetime
import re
import codecs

class ManyToOneMapper():
    def __init__(self, entity_api_maps, api_name):
        self.entity_api_maps = entity_api_maps
        self.api_name = api_name
        self.entity_map = self.entity_api_maps[self.api_name]

    def map(self, pristine_db_entity, json_response):
        for entity_field in dir(pristine_db_entity):
            value = None
            if entity_field in self.entity_map.keys(): # field is mapped
                response_field_name = self.entity_map[entity_field].split('|')[0]
                if '|' in self.entity_map[entity_field]: #with pipe
                    pipe = self.entity_map[entity_field].split('|')[1:]
                    if len(pipe) == 1: # regular pipe
                        p = f'ManyToOneMapper.{pipe[0]}({str(eval(str(json_response[response_field_name])))})'
                        value = eval(p)
                    elif len(pipe) > 1: # pipe with param like to_datetime
                        p = f'ManyToOneMapper.{pipe[0]}("{json_response[response_field_name]}","{pipe[1]}")'
                        value = eval(p)
                else: #without pipe
                    value = self.__get_value_by_regex(json_response[response_field_name]) #if response_field_name in json_response.keys() else None
            elif entity_field in json_response.keys(): #exact field is on entity, json could have different type str for example
                value = self.__get_value_by_regex(json_response[entity_field]) 
            else: #python object prototype field
                continue
            
            setattr(pristine_db_entity, entity_field, value)

        return pristine_db_entity
    
    def __get_value_by_regex(self, json_value):
        #https://www.w3schools.com/python/python_regex.asp
        if json_value is None:
            return None

        str_value = str(json_value)

        if str_value.lower() in ['none','n/a','null','']:
            return None
        elif re.match('^\d{4}-\d{2}-\d{2}$',str_value) is not None:
            if str_value == '0000-00-00':
                return datetime.datetime(1900,1,1)
            return datetime.datetime.strptime(json_value, '%Y-%m-%d')
        elif re.match('^-*\d*\.\d*$',str_value) is not None:
            return float(json_value)
        elif re.match('^-*\d*$',str_value) is not None:
            return int(json_value)
        else:
            return codecs.decode(bytes(json_value.strip(), encoding='utf-8')) #string
    
    #########
    # Pipes #
    #########
    @staticmethod
    def to_float(value):
        return None if (value == '' or value is None or value == 'None') else float(value)

    @staticmethod
    def to_int(value):
        return None if (value == '' or value is None or value == 'None') else int(value)

    @staticmethod
    def to_datetime(value, format):
        try:
            if value == '0000-00-00':
                return datetime.datetime(1900,1,1)
            res = datetime.datetime.strptime(value, format)
            return res
        except:
            print(f'[Cannot parse {value} to datatime using {format}]')
            return None

    @staticmethod
    def from_timestamp(value):
        res = datetime.datetime.fromtimestamp(int(value) / 1000.0)
        return res

    @staticmethod
    def join_array(value):
        if type(value).__name__ == 'str':
            return value
        return None if (value == '' or value is None or value == 'None') else ','.join(value) #convert the string to list object

    @staticmethod
    def translate(value):
        translations = {
            'ad':'ADR',
            're':'REIT',
            'ce':'Closed End Fund',
            'si':'Secondary Issue',
            'lp':'Limited Partnership',
            'cs':'Stock',
            'et':'ETF',
            'wt':'Warrant',
            'rt':'Right',
            '':'N/A',
            'ut':'Unit',
            'temp':'Temporary'
        }
        if value in translations.keys():
            return translations[value]
        else:
            return value