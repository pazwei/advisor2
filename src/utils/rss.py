import datetime
from xml.etree import ElementTree as etree
import hashlib
import html
import urllib.parse

import requests
import bs4 as bs

headers_dict = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36', "Upgrade-Insecure-Requests": "1","DNT": "1","Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8","Accept-Language": "en-US,en;q=0.5","Accept-Encoding": "gzip, deflate"}

def get_rss_feed(url):
    articles = []

    try:
        s = requests.Session()
        parsed_link = urllib.parse.urlparse(url)
        s.get(f'{parsed_link.scheme}://{parsed_link.netloc}', headers=headers_dict, timeout=5, allow_redirects = False)
        rss = s.get(url, headers=headers_dict, timeout=5)

        root = etree.fromstring(rss.text)
        item = root.findall('channel/item')

        for entry in item:
            title = html.unescape(entry.findtext('title'))
            link = entry.findtext('link')

            articles.append({
                'title': title,
                'link': link,
                'description': html.unescape(bs.BeautifulSoup(title if entry.findtext('description') is None else entry.findtext('description'), features="html.parser").text)
            })

    except requests.errors.Timeout as e:
        print(e)

    return articles

def get_atom_feed(url):
    print(f'<ATOM link="{url}" />')
    articles = []
    try:
        res = requests.get(url, headers=headers_dict, timeout=5)

        root = etree.fromstring(res.text)
        atom_prefix = '{http://www.w3.org/2005/Atom}'
        for entry in root.findall(f'{atom_prefix}entry'):

            title = html.unescape(entry.findtext(f'{atom_prefix}title')) 
            link = entry.findtext(f'{atom_prefix}link')
            description = html.unescape(bs.BeautifulSoup(entry.findtext(f'{atom_prefix}summary'), features="html.parser").text) if entry.find(f'{atom_prefix}summary') is not None else 'N/A'
            
            articles.append({
                'title': title, 
                'link': link, 
                'description': description
            })

    except requests.errors.Timeout as e:
        print(e)

    return articles