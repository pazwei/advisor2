import logging
from datetime import datetime
import os

#https://docs.python.org/3/howto/logging.html
#https://realpython.com/python-logging/
#https://docs.python.org/3/howto/logging-cookbook.html

filename = f'{datetime.now().strftime("%Y-%m-%d")}.log'
script_dir = os.path.dirname(__file__)
logfilepath = os.path.join(script_dir, '..', 'logs', filename)

default_formatter = '%(asctime)s %(levelname)-8s %(message)s'

logging.basicConfig(
    level=logging.INFO, 
    # filename=logfilepath, 
    datefmt='%Y-%m-%d %H:%M:%S', 
    format=default_formatter,
    handlers=[
        logging.FileHandler(logfilepath),
        logging.StreamHandler()
    ]) 

__logger = logging.getLogger('Advisor')

# def debug(sender, msg):
#     __logger.debug(f'[{sender}] {msg}')

def info(sender, msg):
    __logger.info(f'[{sender}] {msg}')

def warning(sender, msg):
    __logger.warning(f'[{sender}] {msg}')

# def exception(sender, ex):
#     __logger.error(f'[{sender}] {ex}')

def error(sender, msg):
    __logger.error(f'[{sender}] {msg}')

if __name__ == '__main__':
    # debug('Test','DEBUG Logging pformatted data')
    info('Test','INFO Logging pformatted data')
    warning('Test','WARNING Logging pformatted data')
    # exception('Test', Exception("test"))
    error('Test','ERROR Logging pformatted data')
    # info('TEST','aaaLogging pformatted data')
    # logging.basicConfig(format='%(asctime)s %(message)s')