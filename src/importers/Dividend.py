import datetime

import yfinance as yf
import pandas as pd

from db.entities.Dividend import Dividend
import db.queries as queries
from importers.BaseImporter import BaseImporter
import utils.cli as cli
import utils.logger as log

class DividendImporter(BaseImporter):

    def __init__(self):
        super().__init__('Dividend')

    def iex(self, symbolIds_to_refresh):
        #use yahoo finance for historical, use iex for the next dividends only
        policies = queries.get_dividendPaymentsPerYear(symbolIds_to_refresh.keys())

        mapped = {symbolId:[] for symbolId,value in policies.items()} #symbols that pay dividends or ones that are unknown

        # get historical dividends for stocks with no policy (in case when company does not pay, the number should be 0)
        # print(policies)
        # return
        for symbolId in policies.keys():
            if policies[symbolId] is None:
                historical_dividends = self.yahoo_finance({symbolId:None})[symbolId]
                mapped[symbolId] = historical_dividends
        # print(mapped)
        # return mapped
        # bulk request to get the next dividends for the relevant stocks
        # join the key one month after exDate

        bulk_url = 'https://cloud.iexapis.com/stable/stock/market/batch'
        querystring = {
            'symbols': ','.join([k for k,v in mapped.items()]), #check symbols that at least 30 days passed from last exDate
            'types':'dividends',
            'range': 'next', # ranges: 5y, 2y, 1y, ytd, 6m, 3m, 1m, next
            'token':'%APIKEY%'
        }

        response = self.rotator.dispatch('IEX', 'dividends', querystring, None, bulk_url, len(symbolIds_to_refresh))
        for symbolId in response.keys():
            for item in response[symbolId]['dividends']: # add only the new ones
                obj = Dividend()
                obj.dateCreated = datetime.datetime.now()
                obj.symbolId = symbolId
                item.pop('id',None)
                item.pop('symbol',None)
                obj = self.mapper.map(obj, item)
                if obj.paymentDate is not None and obj.exDate is not None and obj.payment is not None:
                    mapped[symbolId].append(obj)
        
        return mapped
        
    def yahoo_finance(self, symbolIds_to_refresh):
        """
        Only historical dividends, no next one, both on the action and the calendar objects.
        """
        # print(f'[Yahoo does not dupport bulk operations. FUCKERS!!!]')
        cli.normal('Dividend',f'Getting historical dividends for {symbolIds_to_refresh.keys()}')
        mapped = {k:[] for k,v in symbolIds_to_refresh.items()}

        for symbolId, days_from_last_update in symbolIds_to_refresh.items():
            try:
                company = yf.Ticker(symbolId)
                df = company.actions
                # Take 2 years of dividend history, I'm not a historian
                if days_from_last_update is None:
                    df = df[df.index > datetime.datetime(datetime.datetime.now().year - 2, 1, 1)]

                if len(df) > 0:
                    for row in df.iterrows():
                        # if row[1] > 0: #is dividend and not split
                        obj = Dividend()
                        obj.symbolId = symbolId
                        obj.dateCreated = datetime.datetime.now()
                        obj.exDate = row[0].to_pydatetime()
                        obj.payment = row[1].Dividends

                        # Manually mapping since most of the data is approximations. FUCK YAHOO FINANCE
                        obj.announceDate = row[0] + datetime.timedelta(days = -60) #usually 2 months before exDate
                        obj.paymentDate = row[0] + datetime.timedelta(days = 21) #usually 3 weeks after exDate
                        
                        if obj.exDate is not None and obj.payment is not None:
                            mapped[symbolId].append(obj)

                    lastYearDividends = datetime.datetime.now().year - 1
                    dividendsLastYear = len([row for row in df.iterrows() if row[0] >= datetime.datetime(lastYearDividends,1,1) and row[0] <= datetime.datetime(lastYearDividends,12,31)]) #len(df[pd.to_datetime(df['Dividends'].index) >= datetime.datetime(lastYearDividends,1,1) & pd.to_datetime(df['Dividends'].index) <= datetime.datetime(lastYearDividends,12,31) ]) 
                    queries.update_dividendPaymentsPerYear(symbolId, dividendsLastYear)
                    if dividendsLastYear > 0:
                        cli.normal('Dividend',f'{symbolId} is paying {dividendsLastYear} a year')
                else:
                    queries.update_dividendPaymentsPerYear(symbolId, -1) #not paying dividends
                    # None: unknow policy
                    # 0: does not pay
                    # >0: pay dividends => needs to be checked
            except Exception as e:
                cli.danger('Dividend',f'Error getting dividends for {symbolId} from Yahoo Finance')
                print(e)
                log.error("Dividend", e)
            
        
        return mapped

    def alpha_vantage(self, symbolIds_to_refresh):
        pass

    def scrape(self, symbolIds):
        pass

    def finnhub(self,symbolIds_to_refresh):
        pass

    def marketstack(self,symbolIds_to_refresh):
        pass
    
# Works with paying users only. FUCKERS
# GET /stock/{symbol}/upcoming-earnings
# https://cloud.iexapis.com/stable/stock/AAPL/upcoming-earnings?token=sk_fba46a171e2f45faa8ad25a9671b2a1d
# https://cloud.iexapis.com/stable/stock/market/batch?types=upcoming-earnings&symbols=AAPL,MSFT&token=sk_fba46a171e2f45faa8ad25a9671b2a1d