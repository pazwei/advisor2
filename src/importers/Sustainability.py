import datetime

import importers.json_to_entity_mapper as mapper
import yfinance as yf

from db.entities.SustainabilityReport import SustainabilityReport
import db.queries as queries
from db.context import get_session

def refresh(priorities = [2]):
    symbolIds = []

    for priority in priorities:
        symbolIds = symbolIds + [item.id for item in queries.get_symbols_by_priority(priority)]

    for symbolId in symbolIds:
        print(f'[Getting sustainability report for {symbolId}...]')
        __refresh_db(symbolId)

def __refresh_db(symbolId):
    DAYS_FROM_LAST_RECORD_THRESHOLD = 90
    last = queries.get_last_substainability_report(symbolId)
    if last is None or (datetime.datetime.now() - last.dateCreated).days > DAYS_FROM_LAST_RECORD_THRESHOLD:
        try:
            finance = yf.Ticker(symbolId)
            df = finance.sustainability # returns pandas df
            if df is not None:
                data = df.to_dict()['Value']
                date = df.index.name
                if last is None or (last is not None and last.year != date.split('-')[0] or last.quater != date.split('-')[1]):
                    mapped = []
                    
                    obj = SustainabilityReport()
                    obj.symbolId = symbolId
                    obj.dateCreated = datetime.datetime.now()
                    obj.year = int(date.split('-')[0])
                    obj.quater = int(date.split('-')[1])

                    obj = self.mapper.map(obj,data) #super()fill_entity(obj, 'YahooFinance', data)

                    mapped.append(obj)
                    queries.bulk_insert(mapped)
                else:
                    print(f'No need to insert sustainability report for {symbolId}')
            else:
                print(f'[Yahoo Finance did not return systainability report for {symbolId}]')
        except Exception as x:
                print(f'ERROR getting sustainability reports for {symbolId}!')
                print(x)
    else:
        print(f'No need to get sustainability report for {symbolId}')

    def bulk_save(self, items_to_insert):
        print('[No need to bulk insert sustainability...]')

    def post_batch_insert(self, data):
        """
        For each item, update the json field in the symbol
        """
        queries.update_symbols_with_sustainability_report(data)
        pass

    def finnhub(self,symbolIds_to_refresh):
        pass

    def marketstack(self,symbolIds_to_refresh):
        pass