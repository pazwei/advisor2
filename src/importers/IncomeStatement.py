import datetime

from db.entities.IncomeStatement import IncomeStatement
from importers.BaseImporter import BaseImporter
import utils.cli as cli
import utils.logger as log

class IncomeStatementImporter(BaseImporter):

    def __init__(self):
        super().__init__('IncomeStatement')
    
    def alpha_vantage(self, symbolIds_to_refresh):
        mapped = {}
        # cli.normal('IncomeStatement',f'Alpha Vantage does not support batch queries. FUCKERS!!')
        for symbolId in symbolIds_to_refresh.keys():
            querystring = {
                'function':'INCOME_STATEMENT',
                'symbol':symbolId,
                'apikey':'%APIKEY%'
            }

            response = self.rotator.dispatch('AlphaVantage', 'incomestatements', querystring, None)
            if response is None:
                cli.danger('IncomeStatement',f'Got empty response. Response probably blocked due to lack of credits')
                return mapped

            try:
                for report in response['annualReports']:
                    obj = IncomeStatement()
                    obj.term = 'ANNUAL'
                    obj.symbolId = symbolId
                    obj = self.mapper.map(obj, report) #super().fill_entity(obj, report)
                    
                    if symbolId not in mapped.keys():
                        mapped[symbolId] = []

                    mapped[symbolId].append(obj)

                for report in response['quarterlyReports']:
                    obj = IncomeStatement()
                    obj.term = 'QUATER'
                    obj.symbolId = symbolId
                    obj = self.mapper.map(obj, report) #super().fill_entity(obj, report)

                    if symbolId not in mapped.keys():
                        mapped[symbolId] = []

                    mapped[symbolId].append(obj)
            except Exception as x :
                cli.danger('IncomeStatement',f'ERROR getting income statements for {symbolId}!')
                log.error("IncomeStatement", x)
                print(x)
                print(response)

        return mapped

    def iex(self, symbolIds_to_refresh):
        pass

    def yahoo_finance(self, symbolIds_to_refresh):
        """
        # show financials
        msft.financials
        msft.quarterly_financials
        Doesnt seems to be working
        """
        pass

    def scrape(self, symbolIds_to_refresh):
        pass

    def finnhub(self,symbolIds_to_refresh):
        pass

    def marketstack(self,symbolIds_to_refresh):
        pass