import datetime
from pprint import pprint
from xml.dom import minidom

import numpy as np
import requests

from db.entities.Ipo import Ipo
import db.queries as queries
from importers.BaseImporter import BaseImporter
import utils.cli as cli
import utils.logger as log

class IpoImporter(BaseImporter):

    def __init__(self):
        super().__init__('Ipo')

    def scrape(self, symbolIds):
        #https://api.nasdaq.com/api/calendar/upcoming
        #https://api.nasdaq.com/api/ipo/calendar?date=2021-01
        #https://api.nasdaq.com/api/ipo/overview/?dealId=1139805-95307
        #https://www.nasdaq.com/market-activity/ipos/overview?dealId=1139805-95307
        # https://www.nasdaq.com/market-activity/ipos/overview?dealId=1139805-95307
        existing_ipos = queries.get_ipos_deal_ids()
        existing_ipo_dealIds = [item.dealId for item in existing_ipos]
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0','Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8', 'Accept-Language':'en-US,en;q=0.5'}
        iposResponse = requests.get(f'https://api.nasdaq.com/api/ipo/calendar?date={datetime.datetime.now().strftime("%Y-%m")}', timeout=10, allow_redirects=True, headers=headers) 
        iposData = iposResponse.json()['data']
        mapped = []
        # print(iposData)
                                                
        for ipo in iposData['upcoming']['upcomingTable']['rows']:
            try:
                if ipo['dealID'] not in existing_ipo_dealIds and ipo['proposedTickerSymbol'] is not None and ipo['proposedSharePrice'] is not None:
                    print(ipo)
                    obj = Ipo()
                    obj.id = ipo['proposedTickerSymbol']
                    obj.deateCreated = datetime.datetime.now()
                    obj.dealId = ipo['dealID']
                    obj.companyName = ipo['companyName']
                    obj.exchange = ipo['proposedExchange']
                    obj.proposedSharePriceLow = float(ipo['proposedSharePrice'].split('-')[0]) if '-' in ipo['proposedSharePrice'] else float(ipo['proposedSharePrice'])
                    obj.proposedSharePriceHigh = float(ipo['proposedSharePrice'].split('-')[1]) if '-' in ipo['proposedSharePrice'] else float(ipo['proposedSharePrice'])
                    # obj.openingPrice #After first day of trading
                    obj.sharesOffered = int(ipo['sharesOffered'].replace('$','').replace(',','').replace('.00',''))
                    obj.ipoDate = datetime.datetime.strptime(ipo['expectedPriceDate'], '%m/%d/%Y')
                    obj.valueOfSharedOffered = int(ipo['dollarValueOfSharesOffered'].replace('$','').replace(',','').replace('.00',''))
                    
                    ipoDetailsResponse = requests.get(f'https://api.nasdaq.com/api/ipo/overview/?dealId={obj.dealId}', timeout=10, allow_redirects=True, headers=headers)
                    ipoData = ipoDetailsResponse.json()['data']
                    obj.sharedOutstanding = int(ipoData['poOverview']['SharesOutstanding']['value'].replace('$','').replace(',','').replace('.00',''))
                    obj.employees = int(ipoData['poOverview']['NumberOfEmployees']['value'].split(' ')[0]) #4500 (as of 01/19/2021)
                    obj.website = self.__get_a_tag_value(ipoData['poOverview']['CompanyWebsite']['value']) if '--' not in ipoData['poOverview']['CompanyWebsite']['value'] else None
                    obj.ceo = ipoData['poOverview']['CEO']['value']
                    obj.description = ipoData['companyInformation']['companyDescription']

                    mapped.append(obj)
                    cli.normal('Ipo',f'Added new IPO {obj.id}...')
            
            except Exception as x :
                cli.danger('Ipo',f'ERROR getting scrapping an IPO')
                log.error("Ipo", x)
                print(x)
        
        return {
        'add': mapped,
        'update': [],
        'delete': []
    }

            
    
    def __get_a_tag_value(self, text):
        dom = minidom.parseString(text)
        return dom.getElementsByTagName('a')[0].firstChild.nodeValue if '</a>' in text else text

    def iex(self, symbolIds):
        pass

    def alpha_vantage(self, symbolIds):
        pass

    def yahoo_finance(self, symbolIds):
        pass

    def finnhub(self,symbolIds_to_refresh):
        pass

    def marketstack(self,symbolIds_to_refresh):
        pass