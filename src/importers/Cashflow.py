import datetime

from db.entities.Cashflow import CashFlow
from importers.BaseImporter import BaseImporter
import utils.cli as cli
import utils.logger as log

class CashflowImporter(BaseImporter):

    def __init__(self):
        super().__init__('CashFlow')

    def alpha_vantage(self, symbolIds_to_refresh):
        mapped = {}
        # cli.normal('CashFlow',f'Alpha Vantage does not support batch queries. FUCKERS!!')
        for symbolId in symbolIds_to_refresh.keys():
            querystring = {
                'function':'CASH_FLOW',
                'symbol':symbolId,
                'apikey':'%APIKEY%'
            }

            response = self.rotator.dispatch('AlphaVantage', 'cashflows', querystring, None)
            if response is None:
                cli.danger('CashFlow',f'Got empty response. Response probably blocked due to lack of credits')
                return mapped
            try:
                for report in response['annualReports']:
                    obj = CashFlow()
                    obj.term = 'ANNUAL'
                    obj.symbolId = symbolId
                    obj = self.mapper.map(obj, report) #super().fill_entity(obj, report)
                    if symbolId not in mapped.keys():
                        mapped[symbolId] = []

                    mapped[symbolId].append(obj)

                for report in response['quarterlyReports']:
                    obj = CashFlow()
                    obj.term = 'QUATER'
                    obj.symbolId = symbolId
                    obj = self.mapper.map(obj, report) # super().fill_entity(obj, report)
                    if symbolId not in mapped.keys():
                        mapped[symbolId] = []

                    mapped[symbolId].append(obj)

            except Exception as x :
                cli.danger('CashFlow',f'ERROR getting cashflows for {symbolId}!')
                log.error("CashFlow", x)
                print(x)
                print(response)

        return mapped

    def iex(self, symbolIds_to_refresh):
        """
        # show cashflow
        msft.cashflow
        msft.quarterly_cashflow
        #Doesnt seems to be working
        """
        pass

    def yahoo_finance(self, symbolIds_to_refresh):
        pass

    def scrape(self, symbolIds_to_refresh):
        pass

    def finnhub(self,symbolIds_to_refresh):
        pass

    def marketstack(self,symbolIds_to_refresh):
        pass