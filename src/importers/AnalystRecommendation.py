import datetime
import yfinance as yf
import pandas as pd

from db.entities.AnalystRecommendation import AnalystRecommendation
from importers.BaseImporter import BaseImporter
import utils.cli as cli
import utils.logger as log
class AnalystRecommendationImporter(BaseImporter):

    def __init__(self):
        super().__init__('AnalystRecommendation')
    
    def yahoo_finance(self, symbolIds_to_refresh):
        mapped = {}
        # cli.normal('AnalystRecommendation',f'Yahoo Finances does not support batch queries. FUCKERS!!')
        for symbolId in symbolIds_to_refresh.keys():
            try:
                cli.normal('AnalystRecommendation',f'Getting recommendations for {symbolId}...')
                finance = yf.Ticker(symbolId)
                new_analyst_recommendations = finance.recommendations # returns pandas df
                if new_analyst_recommendations is not None:
                    for index,row in new_analyst_recommendations.iterrows():
                        obj = AnalystRecommendation()
                        
                        obj.dateCreated = datetime.datetime.now()
                        obj.symbolId = symbolId
                        obj.date = index.to_pydatetime()
                        obj.firm = row['Firm']
                        obj.toGrade = row['To Grade']
                        obj.fromGrade = row['From Grade']
                        obj.gradeAction = row['Action']

                        if symbolId not in mapped.keys():
                            mapped[symbolId] = []

                        mapped[symbolId].append(obj)
            except Exception as x :
                    cli.danger('AnalystRecommendation',f'ERROR getting analyst recommendations for {symbolId}!')
                    log.error("AnalystRecommendation", x)
                    print(x)

        for _,arr in mapped.items():
            arr.sort(key=lambda item: item.date, reverse=False) #sort is void, does not return new array

        return mapped
    
    def iex(self, symbolIds_to_refresh):
        pass

    def alpha_vantage(self, symbolIds_to_refresh):
        pass

    def scrape(self, symbolIds_to_refresh):
        pass

    def finnhub(self,symbolIds_to_refresh):
        pass

    def marketstack(self,symbolIds_to_refresh):
        pass

#     >>> pprint.pprint(msft.recommendations)
#                                Firm       To Grade From Grade Action
# Date
# 2012-03-16 08:19:00  Argus Research            Buy                up
# 2012-03-19 14:00:00  Hilliard Lyons  Long-Term Buy              main
# 2012-03-22 07:03:00  Morgan Stanley     Overweight              main
# 2012-04-03 11:53:00             UBS            Buy              main
# 2012-04-20 06:18:00   Deutsche Bank            Buy              main
# ...                             ...            ...        ...    ...
# 2020-10-28 13:22:56   Credit Suisse     Outperform              main
# 2020-10-29 16:00:20   Deutsche Bank            Buy              main
# 2020-11-05 10:48:37     Oppenheimer     Outperform    Perform     up
# 2020-11-24 11:17:51         KeyBanc     Overweight              init
# 2020-12-21 12:04:59       Citigroup            Buy    Neutral     up

# [298 rows x 4 columns]