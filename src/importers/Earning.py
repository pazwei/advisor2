import datetime

from db.entities.Earning import Earning
from importers.BaseImporter import BaseImporter
import utils.cli as cli
import utils.logger as log

class EarningImporter(BaseImporter):

    def __init__(self):
        super().__init__('Earning')

    def alpha_vantage(self, symbolIds_to_refresh):
        mapped = {}
        # cli.normal('Earning',f'Alpha Vantage does not support batch queries. FUCKERS!!')
        for symbolId in symbolIds_to_refresh.keys():
            #TODO: decide how many to fetch from api according to days passed, irrelevant to alpha vantage calls
            querystring = {
                'function':'EARNINGS',
                'symbol':symbolId,
                'apikey':'%APIKEY%'
            }

            response = self.rotator.dispatch('AlphaVantage', 'earnings', querystring, None)
            if response is None:
                cli.danger('Earning',f'Got empty response. Response probably blocked due to lack of credits')
                return mapped
            try:
                for report in response['annualEarnings']:
                    obj = Earning()
                    obj.term = 'ANNUAL'
                    obj.symbolId = symbolId
                    obj.dateCreated = datetime.datetime.now()
                    obj = self.mapper.map(obj, report) #super().fill_entity(obj, report)

                    if symbolId not in mapped.keys():
                        mapped[symbolId] = []

                    mapped[symbolId].append(obj)

                for report in response['quarterlyEarnings']:
                    obj = Earning()
                    obj.term = 'QUATER'
                    obj.symbolId = symbolId
                    obj.dateCreated = datetime.datetime.now()
                    obj = self.mapper.map(obj, report) #super().fill_entity(obj, report)
                    
                    if symbolId not in mapped.keys():
                        mapped[symbolId] = []

                    mapped[symbolId].append(obj)

            except Exception as x :
                cli.danger('Earning',f'ERROR getting earnings for {symbolId}!')
                log.error("Earning", x)
                print(x)
                print(response)
        
        return mapped

    def iex(self, symbolIds):
        pass

    def yahoo_finance(self, symbolIds):
        """
        # show earnings
        msft.earnings
        msft.quarterly_earnings
        Check it out
        """
        pass

    def scrape(self, symbolIds):
        pass

    def finnhub(self,symbolIds_to_refresh):
        pass

    def marketstack(self,symbolIds_to_refresh):
        pass