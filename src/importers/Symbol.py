import datetime
import pydash

from db.entities.Symbol import Symbol
import db.queries as queries
from importers.BaseImporter import BaseImporter
import utils.cli as cli
import utils.logger as log
import utils.general as general

class SymbolImporter(BaseImporter):

    def __init__(self):
        super().__init__('Symbol')

    def __set_index_and_priority(self, symbol):
        sp500_symbols = general.get_default_list('sp500.txt') 
        default_tracked_symbols = general.get_default_list('tracked_symbols.txt') 
        tracked_etfs = general.get_default_list('tracked_etfs.txt')
        tracked_industries = general.get_default_list('tracked_industries.txt')
        tracked_keywords = general.get_default_list('tracked_symbol_description_keywords.txt')

        if symbol.id in sp500_symbols:
            symbol.indices = 'SP500'
            symbol.priority = 1
        
        if symbol.priority is None:
            if symbol.id in tracked_etfs:
                symbol.priority = 1
            elif symbol.id in default_tracked_symbols:
                symbol.priority = 2
            elif symbol.industry in tracked_industries:
                symbol.priority = 2
            elif symbol.description is not None and any([(i in symbol.description.lower()) for i in tracked_keywords]):
                symbol.priority = 2
            else:
                symbol.priority = -1

        return symbol

    def __get_iex_company_infos_mapped(self, symbols_to_enrich):
        bulk_url = 'https://cloud.iexapis.com/stable/stock/market/batch'
        # try:
        symbols = [i.id for i in symbols_to_enrich]
        querystring = {
            'symbols': ','.join(symbols), #i.__dict__['id']
            'types':'company', # peers available only for paying customer FUCKERS'
            'token':'%APIKEY%'
        }

        response = self.rotator.dispatch('IEX', 'company', querystring, None, bulk_url, len(symbols_to_enrich))
        if response is not None and type(response).__name__ != 'str':
            for symbol in symbols_to_enrich:
                print(f'Getting company data for: {symbol.id}')
                if symbol.id in response.keys():
                    item = response[symbol.id]['company']
                    symbol = self.__set_index_and_priority(symbol)
                    symbol = self.mapper.map(symbol, item)
                else:
                    cli.danger('Symbol',f'IEX did not returned comapny data for: {symbol.id}')
                
                # peers available only for paying customer FUCKERS'
                # obj.peers = ','.join(response[symbolId]['peers']) if response[symbolId]['peers'] is not None and len(response[symbolId]['peers']) > 0 else None
        else:
            cli.danger('Symbol','Error getting company infos')
            # log.error("symbol", f'Error getting company infos for {symbols_to_enrich}')
            # print(f'{response} ]')
        # except:
        #     cli.danger('Symbol',f'Error occured with {symbols.name}')
        
        return symbols_to_enrich
                
    def iex(self,symbolIds_to_refresh):
        # find which symbols where added and which were removed
        # https://cloud.iexapis.com/stable/ref-data/symbols

        iex_symbols_response = self.rotator.dispatch('IEX', 'symbols', {'token':'%APIKEY%'}, None)
        iex_symbol_ids = [item['symbol'] for item in iex_symbols_response if str(item['isEnabled']).lower() == 'true' and '-' not in item['symbol'] and '#' not in item['symbol'] and '+' not in item['symbol'] and '=' not in item['symbol']]
        cli.warning('Symbol',f'iex_symbol_ids: {len(iex_symbol_ids)}')
        
        existing_symbols = queries.get_all_symbols()
        existing_symbol_ids = [item.id for item in existing_symbols]

        symbol_ids_to_remove = pydash.arrays.difference(existing_symbol_ids, iex_symbol_ids)
        symbol_ids_to_add = pydash.arrays.difference(iex_symbol_ids, existing_symbol_ids)
        symbol_to_update_company_info = [item for item in existing_symbols if (item.assetType == 'cs' or item.assetType == 'Stock') and (item.name == None or item.name == '' or item.sector == None or item.sector == '' or item.industry == None or item.industry == '' or item.description == None or item.description == '' or item.tags == None or item.tags == '' or item.website == None or item.website == '')]
        
        cli.warning('Symbol',f'symbol_ids_to_remove: {len(symbol_ids_to_remove)}')
        cli.warning('Symbol',f'symbol_ids_to_add: {len(symbol_ids_to_add)}')
        cli.warning('Symbol',f'symbol_to_update_company_info: {len(symbol_to_update_company_info)}')

        # updated = self.__get_iex_company_infos_mapped(symbol_to_update_company_info)
        # print(f'ipdated: {len(updated)}')
        # self.bulk_update(updated)
            
        update_chunks = pydash.arrays.chunk(symbol_to_update_company_info, self.CHUNK_SIZE)
        for chunk in update_chunks:
            updated = self.__get_iex_company_infos_mapped(chunk)
            self.bulk_update(updated)
            cli.success('Symbol', f'Updated {self.CHUNK_SIZE} symbols with company data')

        insert_chunks = pydash.arrays.chunk(symbol_ids_to_add, self.CHUNK_SIZE)
        for chunk in insert_chunks:
            prestine_symbols = [Symbol(dateCreated = datetime.datetime.now(), id = i, priority = None) for i in chunk]

            items = self.__get_iex_company_infos_mapped(prestine_symbols)
            items = [item for item in items if item is not None]
            self.bulk_save(items)
            cli.success('Symbol', f'inserted {self.CHUNK_SIZE} symbols with company data')

        #manually add symbols to db
        return {
            'add': [], #symbols_to_add,
            'update': [], #symbols_to_update,
            'delete': [] #symbol_ids_to_remove
        }

    def alpha_vantage(self,symbolIds_to_refresh):
        pass

    def yahoo_finance(self,symbolIds_to_refresh):
        pass

    def scrape(self,symbolIds_to_refresh):
        pass

    def finnhub(self,symbolIds_to_refresh):
        pass

    def marketstack(self,symbolIds_to_refresh):
        pass