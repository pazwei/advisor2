import datetime

from db.entities.BalanceSheet import BalanceSheet
from importers.BaseImporter import BaseImporter
import utils.cli as cli
import utils.logger as log

class BalancesheetImporter(BaseImporter):

    def __init__(self):
        super().__init__('BalanceSheet')

    def alpha_vantage(self, symbolIds_to_refresh): #{AAPL: 2021-01-01...}
        mapped = {}
        # cli.normal('BalanceSheet',f'Alpha Vantage does not support batch queries. FUCKERS!!')
        for symbolId in symbolIds_to_refresh.keys():
            querystring = {
                'function':'BALANCE_SHEET',
                'symbol':symbolId,
                'apikey':'%APIKEY%'
            }

            response = self.rotator.dispatch('AlphaVantage', 'balancesheets', querystring, None)
            if response is None:
                cli.danger('BalanceSheet',f'Got empty response. Response probably blocked due to lack of credits')
                return mapped
            try:
                for report in response['annualReports']:
                    obj = BalanceSheet()
                    obj.term = 'ANNUAL'
                    obj.symbolId = symbolId
                    obj.dateCreated = datetime.datetime.now()
                    obj = self.mapper.map(obj, report) #super().fill_entity(obj, report)

                    if symbolId not in mapped.keys():
                        mapped[symbolId] = []

                    mapped[symbolId].append(obj)

                for report in response['quarterlyReports']:
                    obj = BalanceSheet()
                    obj.term = 'QUATER'
                    obj.symbolId = symbolId
                    obj.dateCreated = datetime.datetime.now()
                    obj = self.mapper.map(obj, report) #super().fill_entity(obj, report)

                    if symbolId not in mapped.keys():
                        mapped[symbolId] = []

                    mapped[symbolId].append(obj)

            except Exception as e:
                cli.danger('BalanceSheet',f'ERROR occured while fetching balance sheet for {symbolId}')
                log.error("BalaceSheet", e)
                print(e)
        
        return mapped

    def iex(self, symbolIds_to_refresh):
        pass

    def yahoo_finance(self, symbolIds_to_refresh):
        """
        msft.balance_sheet
        msft.quarterly_balance_sheet
        Doesn't seem to be working
        """
        pass

    def scrape(self, symbolIds_to_refresh):
        pass

    def finnhub(self,symbolIds_to_refresh):
        pass

    def marketstack(self,symbolIds_to_refresh):
        pass