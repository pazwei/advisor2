from abc import ABC, abstractmethod
import datetime
import os
import os.path as path
import json
import re
import configparser
import threading
import concurrent.futures
import math

import pydash

import rotator.freeTierApiRotator as rotator
import db.queries as queries
from db.context import get_session
import utils.cli as cli
import utils.logger as logger
from utils.ManyToOneMapper import ManyToOneMapper

class BaseImporter(ABC):
    
    """
    Base class for all importers. Here there is the map loading, mapping logic. Consider add the rotator logic inside, since it's  
    """
    
    def __init__(self, entity_name):
        configParser = BaseImporter.load_config()
        config = configParser[entity_name]
        self.ENTITY_NAME = entity_name

        self.API = config['Api']
        self.CHUNK_SIZE = int(config['BulkInsertChunkSize'])
        self.DAYS_FROM_LAST_RECORD_THRESHOLD = int(config['DaysFromLastRecord'])
        self.AGE_DATE_FIELD = config['AgeDateField']
        self.LIST_FOR_EACH_SYMBOL = config['ListForEachSymbol'] == 'True'
        self.RELEVANT_SYMBOL_TYPES = config['SymbolTypes']
        self.EPOCH = datetime.datetime.strptime(config['Epoch'],'%Y-%m-%d')
        self.MAX_THREADS = int(config['MaxThreads'])

        self.rotator = rotator.API_ROTATOR
        self.__maps__ = {}
        self.__load_mappings()
        self.mapper = ManyToOneMapper(self.__maps__[self.ENTITY_NAME], self.API)

    @staticmethod
    def load_config():
        configParser = configparser.ConfigParser()
        script_dir = path.dirname(__file__)
        abs_config_file_path = path.join(script_dir, 'settings.ini')
        configParser.read(abs_config_file_path)
        return configParser

    def __load_mappings(self):
        """
        Load all json mappings once, on module load.
        """
        script_dir = os.path.dirname(__file__)
        abs_path_to_json_dir = os.path.join(script_dir, 'maps')
        json_files = [f for f in os.listdir(abs_path_to_json_dir) if os.path.isfile(os.path.join(abs_path_to_json_dir, f))]
        
        for item in json_files:
            file = open(os.path.join(abs_path_to_json_dir, item))
            data =  json.load(file)
            file.close()
            entity_name = os.path.splitext(item)[0]
            self.__maps__[entity_name] = data

    def refresh(self, priorities_limit = [], symbolIds_limit = []):         
        """
        Refresh table data from api. This function first of all query selected entity to find which symbolIds should be updated. 
        For example, find all symbols which their income statements are more than 3 months old. After that the symbol list can be further limited by priority or specific symbolIds 
        """ 
        if self.LIST_FOR_EACH_SYMBOL:

            symbols_to_refresh = queries.get_symbols_needing_entity_refresh(self.ENTITY_NAME, self.AGE_DATE_FIELD, self.DAYS_FROM_LAST_RECORD_THRESHOLD) #by priority
            for s in symbolIds_limit:
                if s not in symbols_to_refresh:
                    #check to see what is the priority of the symbol, if it's -1 then update to 2
                    symbols_to_refresh.append(s)
                    #TODO update priority if symbol is new

            # print(symbols_to_refresh)
            #Limits should not be often used
            if len(symbolIds_limit) > 0:
                symbols_to_refresh = [item for item in symbols_to_refresh if item.id in symbolIds_limit]

            if len(priorities_limit) > 0:
                symbols_to_refresh = [item for item in symbols_to_refresh if item.priority in priorities_limit]
            
            symbolIds = [item.id for item in symbols_to_refresh if (str(item.assetType) in self.RELEVANT_SYMBOL_TYPES)]
            chunks = pydash.arrays.chunk(symbolIds, size=self.CHUNK_SIZE)
            total_insereted = 0
            
            # https://realpython.com/intro-to-python-threading/#what-is-a-thread
            # https://www.digitalocean.com/community/tutorials/how-to-use-threadpoolexecutor-in-python-3
            # https://pymotw.com/3/concurrent.futures/
            if self.MAX_THREADS == 1: #sync
                cli.warning('BaseImporter', f'Importing {self.ENTITY_NAME} using {self.MAX_THREADS} thread')
                for chunk in chunks:
                    res = self.refresh_by_symboldIds_executor(chunk)
                    total_insereted += res['add']
            else: #multithread
                with concurrent.futures.ThreadPoolExecutor(max_workers=self.MAX_THREADS) as executor: 
                    cli.warning('BaseImporter', f'Importing {self.ENTITY_NAME} using {self.MAX_THREADS} threads')
                    results = executor.map(self.refresh_by_symboldIds_executor, chunks)
                    total_insereted = sum([f['add'] for f in list(results)])
                    cli.success('BaseImporter', f'Insereted {total_insereted} {self.ENTITY_NAME} items')

                    return {
                        'add': total_insereted,
                        'update':0,
                        'delete':0
                    }
        else:
            deltas = self.refresh_lookups() #{'add':[],'delete':[],'update':[]}
            if len(deltas['update']) + len(deltas['delete']) +len(deltas['add']) == 0:
                # cli.warning('BaseImporter',f'There are no {self.ENTITY_NAME} items to add, update or remove')
                logger.warning('BaseImporter',f'There are no {self.ENTITY_NAME} items to add, update or remove')

            if deltas['update'] is not None and len(deltas['update']) > 0:
                queries.bulk_update(deltas['update'])

            if deltas['delete'] is not None and len(deltas['delete']) > 0:
                queries.bulk_delete_by_ids(deltas['delete'])

            if deltas['add'] is not None and len(deltas['add']) > 0:
                queries.bulk_insert(deltas['add'])
            
            return {
                'add':len(deltas['add']),
                'update':len(deltas['update']),
                'delete':len(deltas['delete'])
            }

    def refresh_lookups(self):
        # last = queries.get_last_items_by_entity(self.ENTITY_NAME)
        # days_passed_from_last_item = (datetime.datetime.now() - getattr(last, self.AGE_DATE_FIELD)).days if last is not None else self.DAYS_FROM_LAST_RECORD_THRESHOLD

        # if days_passed_from_last_item > self.DAYS_FROM_LAST_RECORD_THRESHOLD:

        if self.API == 'AlphaVantage':
            deltas = self.alpha_vantage(None)
        elif self.API == 'IEX':
            deltas = self.iex(None)
        elif self.API == 'YahooFinance':
            deltas = self.yahoo_finance(None)
        elif self.API == 'Scrape':
            deltas = self.scrape(None)

        return deltas
        # else:
        #     return {'add': [], 'delete':[], 'update':[]}

    def refresh_by_symboldIds_executor(self, chunk):
        cli.normal('BaseImporter',f'Getting bulk {self.ENTITY_NAME} for {", ".join(chunk)}...')
        items_to_insert = self.refresh_by_symboldIds(chunk)
        total_insereted = 0
        
        if len(items_to_insert.keys()) > 0:
            cli.success('BaseImporter',f'Got {self.ENTITY_NAME} results for {",".join(items_to_insert.keys())} ({len(items_to_insert.keys())}).')
            all_items_to_insert = []

            if len(items_to_insert.keys()) == 0:
                cli.danger('BaseImporter', f'Received no {self.ENTITY_NAME} data on {chunk} - quiting!')
                logger.warning('BaseImporter', f'Received no {self.ENTITY_NAME} data on {chunk}!')
                return

            for key, items in items_to_insert.items():
                if items is not None:
                    all_items_to_insert = all_items_to_insert + items

            if len(all_items_to_insert) > 0:
                try:
                    valid_items_to_insert = [i for i in all_items_to_insert if type(getattr(i, self.AGE_DATE_FIELD)) is datetime.datetime and type(getattr(i, self.AGE_DATE_FIELD)) is not str and getattr(i, self.AGE_DATE_FIELD) >= self.EPOCH] # Remove items that are not timed correctly 
                    
                    if len(valid_items_to_insert) != len(all_items_to_insert):
                        cli.danger('BaseImporter', f'Inserting {len(valid_items_to_insert)} {self.ENTITY_NAME} out of {len(all_items_to_insert)}, filtered records with no age date or {self.ENTITY_NAME} prior {self.EPOCH.strftime("%Y-%m-%d")}')
                        logger.warning('BaseImporter', f'Inserting {len(valid_items_to_insert)} {self.ENTITY_NAME} out of {len(all_items_to_insert)}, filtered records with no age date {self.ENTITY_NAME} prior {self.EPOCH.strftime("%Y-%m-%d")}')

                    self.bulk_save(valid_items_to_insert)
                    self.post_batch_insert(valid_items_to_insert)
                    total_insereted += len(valid_items_to_insert)
                except:
                    cli.danger('BaseImporter',f'Error inserting {self.ENTITY_NAME} batch containing {chunk}')
                    logger.error('BaseImporter', f'Error inserting {self.ENTITY_NAME} batch containing {chunk}')
            else:
                logger.warning('BaseImporter', f'No {self.ENTITY_NAME} items to insert for {chunk}')
        return {
            'add': total_insereted,
            'update':0,
            'delete':0
        }
    
    def refresh_by_symboldIds(self, symbolIds): #run on seperate threads or in sync on main thread
        """
        Check which stock items needs to be updated, and then pass the chunk into the provider
        """
        items_to_insert = {}
        symbolIds_to_refresh = {}
        for symbolId in symbolIds:
            last = queries.get_last_items_by_symbolId(self.ENTITY_NAME, symbolId)
            days_passed_from_last_item = (datetime.datetime.now() - getattr(last, self.AGE_DATE_FIELD)).days if last is not None else None #self.DAYS_FROM_LAST_RECORD_THRESHOLD
            # print(f'days_passed_from_last_item: {days_passed_from_last_item}')
            # print('self.DAYS_FROM_LAST_RECORD_THRESHOLD: ' + str(self.DAYS_FROM_LAST_RECORD_THRESHOLD))
            if days_passed_from_last_item is None or days_passed_from_last_item >= self.DAYS_FROM_LAST_RECORD_THRESHOLD:
                symbolIds_to_refresh[symbolId] = days_passed_from_last_item # made in order that the importer will know how many items to fetch from API due to costs, relevant for IEX
        
        if len(symbolIds_to_refresh.keys()) > 0:
            # cli.normal('BaseImporter',f'Trying to update {len(symbolIds_to_refresh.keys())} symbols out of {len(symbolIds)}{", the rest does not need to be updated..." if len(symbolIds_to_refresh.keys()) != len(symbolIds) else "..."}]')
        
            if self.API == 'AlphaVantage':
                items_to_insert = self.alpha_vantage(symbolIds_to_refresh) 
            elif self.API == 'IEX':
                items_to_insert = self.iex(symbolIds_to_refresh)
            elif self.API == 'YahooFinance':
                items_to_insert = self.yahoo_finance(symbolIds_to_refresh)
            elif self.API == 'Scrape':
                items_to_insert = self.scrape(symbolIds_to_refresh)

        else:
            cli.normal('BaseImporter',f'Skipping update {self.ENTITY_NAME} for {", ".join(symbolIds)} - not enought days passed...')

        #providers will return items, need to check obsolesence to avoid dups
        #for chunk there could be diffrent items with different obsolesence values
        if len(items_to_insert.keys()) > 0:
            for symbolId, items in items_to_insert.items(): #{AAPL: [], MSFT:[]...}
                last = queries.get_last_items_by_symbolId(self.ENTITY_NAME, symbolId)
                
                #Filter the provided items in order to insert only the newer than the last record. If there is no last record then insert all item
                if (last is not None and items is not None):
                    items_to_insert[symbolId] = [item for item in items if (getattr(item, self.AGE_DATE_FIELD) - getattr(last, self.AGE_DATE_FIELD)).days >= self.DAYS_FROM_LAST_RECORD_THRESHOLD]
        
        return items_to_insert

    @abstractmethod
    def alpha_vantage(self,symbolIds_to_refresh):
        """
        To be overriden in child class
        """
        return {'AAPL':[]}

    @abstractmethod
    def iex(self,symbolIds_to_refresh):
        """
        To be overriden in child class
        """
        return {}

    @abstractmethod
    def yahoo_finance(self,symbolIds_to_refresh):
        """
        To be overriden in child class
        """
        return {}

    @abstractmethod
    def scrape(self,symbolIds_to_refresh):
        """
        To be overriden in child class
        """
        return {}

    @abstractmethod
    def finnhub(self,symbolIds_to_refresh):
        """
        To be overriden in child class
        """
        return {}

    @abstractmethod
    def marketstack(self,symbolIds_to_refresh):
        """
        To be overriden in child class
        """
        return {}

    def post_batch_insert(self, data):
        """
        Used to update something else (log or another entity, after the bulk insert finished)
        """
        pass

    def bulk_save(self, items_to_insert):
        queries.bulk_insert(items_to_insert)

    def bulk_update(self, items_to_update):
        queries.bulk_update(items_to_update)

    def bulk_delete_by_ids(self, entity_name, ids_to_delete):
        queries.bulk_delete_by_ids(entity_name, ids_to_delete)
    
    
    @staticmethod
    def get_db_overview():
        """
        table 1: Print status of db - last update of each table, and what should be updated
        print list of items that are not up-to-date for each symbol child entity type (maybe there is no data for those entitys)
        table 2: symbols by priority, symbols by type, total symbols,  Print hwo many etf and stocks are tracked,
        """
        symbol_child_entities_table = [['Entity','No Data Priority 2 Symbols','No Data Priority 1 Symbols','Latest Item Age','Needs Update','Row Count']]
        static_entities_table = [['Entity','Latest Item Age','Needs Update','Row Count']]
        res = []

        configParser = BaseImporter.load_config()

        symbol_child_entities = sorted([i for i in configParser.sections() if configParser[i]['ListForEachSymbol'] == 'True'], key=lambda a: a)
        static_entities = sorted([i for i in configParser.sections() if configParser[i]['ListForEachSymbol'] == 'False'], key=lambda a: a)

        priority_1_symbols = [i.id for i in queries.get_symbols_by_priority(1)]
        priority_2_symbols = [i.id for i in queries.get_symbols_by_priority(2)]

        print(f'Priority 1 symbols: {len(priority_1_symbols)}')
        print(f'Priority 2 symbols: {len(priority_2_symbols)}')

        comments = []
        for entity in symbol_child_entities:
            config = configParser[entity]
            age_threshold = int(config['DaysFromLastRecord'])
            age_field = config['AgeDateField']

            count = queries.count_rows(entity)
            symbolIds = queries.get_distinct_symbolIds(entity)

            diff_1 = [item for item in priority_1_symbols if item not in symbolIds]
            diff_2 = [item for item in priority_2_symbols if item not in symbolIds]
            
            oldest = queries.get_oldest_record(entity, age_field)
            item = {
                'name':entity,
                'count':count,
                'noDataPriority1': len(diff_1),
                'noDataPriority2': len(diff_2),
                'needsUpdate': len(diff_1) > 0 or len(diff_2) > 0 or (datetime.datetime.now() - oldest).days > age_threshold, 
                'latestItemAge': (datetime.datetime.now() - oldest).days #TODO fix bug with negative numbers???
            }
            res.append(item)
            symbol_child_entities_table.append([entity, item['noDataPriority2'], item['noDataPriority1'], item['latestItemAge'], item['needsUpdate'], item['count']])

        cli.print_table(symbol_child_entities_table, 'Symbol Children Entities')

        for entity in static_entities:
            config = configParser[entity]
            age_threshold = int(config['DaysFromLastRecord'])
            age_field = config['AgeDateField']

            count = queries.count_rows(entity)
            oldest = queries.get_oldest_record(entity, age_field)

            item = {
                'name':entity,
                'count':count,
                'latest':(datetime.datetime.now() - oldest).days,
                'needsUpdate': (datetime.datetime.now() - oldest).days > age_threshold
            }
            res.append(item)
            static_entities_table.append([item['name'],item['latest'],item['needsUpdate'],item['count']])

        cli.print_table(static_entities_table, 'Static Entities')
        return res