import datetime
from concurrent.futures import ThreadPoolExecutor, as_completed

from db.entities.Article import Article
from classifier.classify import classifyText as classify
from importers.BaseImporter import BaseImporter
import utils.cli as cli
import utils.logger as log

import requests
from xml.etree import ElementTree as etree

class ArticleImporter(BaseImporter):

    def __init__(self):
        super().__init__('Article')

    def iex(self, symbolIds_to_refresh):
        #IEX support bulk operations
        cli.normal('Article','Making bulk request to IEX for articles')
        
        symIds_50 = {key:value for key,value in symbolIds_to_refresh.items() if value is None or value > 10} #less than 10 days fetch 10
        symIds_10 = {key:value for key,value in symbolIds_to_refresh.items() if value is not None and value <= 10} #more that 10 days fetch 50
        
        mapped = {}

        if len(symIds_50.keys()) > 0:
            # single_url_override = f'https://cloud.iexapis.com/stable/stock/{symbolId}/news' #single - convert to bulk
            # batch_url_override = f'https://cloud.iexapis.com/stable/stock/market/batch?symbols={symIds_50}&types=news&last=50'
            #/stock/{symbol}/news/last/{last}

            bulk_url = 'https://cloud.iexapis.com/stable/stock/market/batch'
            querystring = {
                'symbols': ','.join(symIds_50.keys()),
                'types':'news',
                'last': '50',
                'token':'%APIKEY%'
            }

            bulk_size = len(symIds_50.keys())
            response = self.rotator.dispatch('IEX', 'articlehistory', querystring, None, bulk_url, bulk_size)
            if response is not None:
                for symbolId in response.keys():
                    try:
                        for item in [a for a in response[symbolId]['news'] if a['lang'] == 'en']:
                            obj = Article()
                            obj.symbolId = symbolId
                            obj.dateCreated = datetime.datetime.now()
                            obj.feedId = None

                            obj = self.mapper.map(obj, item) #self.fill_entity(obj, item)
                            obj.hash = obj.create_hash()
                            obj.naiveSentiment = classify(obj.join_texts())
                            
                            if symbolId not in mapped.keys():
                                mapped[symbolId] = []

                            mapped[symbolId].append(obj)

                            # if obj.hash not in [a.hash for a in latest_articles]:
                            #     mapped.append(obj)

                    except Exception as x :
                        cli.danger('Article',f'Error with {symbolId} articles')
                        log.error("Articles", x)
                        print(x)
            else:
                print(f'Could not get data from IEX query articlehistory with querystring {querystring} and bulkUrl {bulk_url}')
        
        if len(symIds_10.keys()) > 0:
            # url_override = f'https://cloud.iexapis.com/stable/stock/{symbolId}/news' #convert to bulk
            bulk_url = 'https://cloud.iexapis.com/stable/stock/market/batch'
            querystring = {
                'symbols': ','.join(symIds_10.keys()),
                'types':'news',
                'last': '10',
                'token':'%APIKEY%'
            }

            bulk_size = len(symIds_10.keys())
            response = self.rotator.dispatch('IEX', 'articles', querystring, None, bulk_url, bulk_size)
            for symbolId in response.keys():
                try:
                    for item in response[symbolId]['news']:
                        obj = Article()
                        obj.symbolId = symbolId
                        obj.dateCreated = datetime.datetime.now()
                        obj.feedId = None

                        obj = self.mapper.map(obj, item) #self.fill_entity(obj, item)
                        obj.hash = obj.create_hash()
                        obj.naiveSentiment = classify(obj.join_texts())

                        if symbolId not in mapped.keys():
                            mapped[symbolId] = []

                        mapped[symbolId].append(obj)

                        # if obj.hash not in [a.hash for a in latest_articles]:
                        #     mapped[symbolId].append(obj)

                except Exception as x :
                    cli.danger('Article', f'Error with {symbolId} articles')
                    log.error("Articles", x)
                    print(x)

        return mapped

    def alpha_vantage(self, symbolIds_to_refresh):
        pass

    def yahoo_finance(self, symbolIds_to_refresh):
        pass

    def finnhub(self,symbolIds_to_refresh):
        pass

    def marketstack(self,symbolIds_to_refresh):
        pass

    def scrape(self, symbolIds_to_refresh): #{AAPL:10}
        # https://stackoverflow.com/questions/20884798/market-information-by-company-yahoo-rss-feeds
        generic_feeds = [
            'https://www.nasdaq.com/feed/rssoutbound?symbol=%SYMBOLID%',
            'https://seekingalpha.com/api/sa/combined/%SYMBOLID%.xml'
        ]

        feeds = [] #array of dictionaries
        unfiltered_dup_articles = {} #{MSFT:[Article,Article], AAPL:[]...}
        res = {} #{MSFT:[Article,Article], AAPL:[]...}

        #create urls to get
        for symbol in symbolIds_to_refresh:
            for feed in generic_feeds:
                feeds.append({'symbol': symbol, 'feed': feed.replace('%SYMBOLID%', symbol)})

        #https://yasoob.me/2019/05/29/speeding-up-python-code-using-multithreading/
        processes = []
        with ThreadPoolExecutor(max_workers=10) as executor:
            for feed in feeds:
                # print(feed)
                symbol = feed['symbol'] #.keys()['symbol']
                url = feed['feed']
                processes.append(executor.submit(self.__get_rss_articles, url, symbol))

        for task in as_completed(processes):
            output = task.result()
            sym = output['symbol']
            if sym not in unfiltered_dup_articles.keys():
                unfiltered_dup_articles[sym] = output['articles']
            else:
                unfiltered_dup_articles[sym] += output['articles']
        
        #remove dups, same article could be sources by multiple feeds and only newer than min date
        for sym, items in unfiltered_dup_articles.items():
            min_date = datetime.datetime.now() + datetime.timedelta(days = -1 * symbolIds_to_refresh[sym]) if symbolIds_to_refresh[sym] is not None else datetime.datetime.now() + datetime.timedelta(days = -60)
            hashed = {}
            for item in items:
                hashed[item.hash] = item #in case of dup, it will just override, since the key already exists
            res[sym] = [i for i in hashed.values() if i.publishedAt > min_date]

        return res

    def __get_rss_articles(self, url, symbol):
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0','Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8', 'Accept-Language':'en-US,en;q=0.5'}
        r = requests.get(url, timeout=10, allow_redirects=True, headers=headers)
        res = []

        try:
            root = etree.fromstring(r.text)
            item = root.findall('channel/item')

            for entry in item:   
                article = Article()
                article.symbolId = symbol
                article.title = entry.findtext('title')
                article.description = entry.findtext('description')
                article.link = entry.findtext('link')
                article.publishedAt = datetime.datetime.strptime(entry.findtext('pubDate'), '%a, %d %b %Y %H:%M:%S %z')
                article.publishedAt = article.publishedAt.replace(tzinfo=None)
                article.dateCreated = datetime.datetime.now()
                article.naiveSentiment = classify(article.join_texts())
                article.feedId = None
                article.hash = article.create_hash()
                res.append(article)

        except BaseException as ex:
            print(ex)
            print(f'Error getting articles for {symbol} {url}')

        return {'symbol':symbol, 'articles':res}