import datetime

from db.entities.Post import Post
import db.queries as queries
from classifier.classify import classifyText as classify
from classifier.classify import extract_symbols as extract_symbols
from importers.BaseImporter import BaseImporter
import utils.cli as cli
import utils.logger as log
import utils.rss as rss
import bots.reddit as reddit
# import bots.twitter as twitter

class PostImporter(BaseImporter):

    def __init__(self):
        super().__init__('Post')

    def iex(self, symbolIds_to_refresh):
        pass
        
    def alpha_vantage(self, symbolIds_to_refresh):
        pass

    def yahoo_finance(self, symbolIds_to_refresh):
        pass

    def finnhub(self,symbolIds_to_refresh):
        pass

    def marketstack(self,symbolIds_to_refresh):
        pass

    def scrape(self, symbolIds_to_refresh):
        res = {'update':[], 'delete':[], 'add':[]}
        all_feeds = queries.get_all_feeds()
        
        reddit_channels = [i for i in all_feeds if i.type == 'REDDIT']
        twitter_accounts = [i for i in all_feeds if i.type == 'TWITTER'] 

        existing_posts_hashed = queries.get_all_posts_hashed()
        
        for channel in reddit_channels:
            cli.normal('Post',f'Getting latest posts from {channel.type} {channel.url}')
            try:
                posts = reddit.get_posts(channel.url)
                cli.success('Post', f'Received {len(posts)} posts from {channel.name}')
                no_symbolId = 0

                for item in posts:
                        obj = Post()
                        obj.dateCreated = datetime.datetime.now()
                        obj.feedId = channel.id

                        obj.title = item['title']
                        obj.description = item['selftext']
                        obj.link = item['url']
                        obj.author = f"{item['author']} ({item['author_fullname']})"
                        
                        obj.ups = item['ups']
                        obj.downs = item['downs']
                        obj.comments = item['num_comments']
                        obj.awards = item['total_awards_received']
                        obj.publishedAt = item['created']

                        obj.hash = obj.create_hash()
                        obj.naiveSentiment = classify(obj.join_texts())
                        obj.symbolIds = ','.join(extract_symbols(obj.join_texts()))

                        if len(obj.symbolIds) > 0:
                            if obj.hash not in existing_posts_hashed:
                                res['add'].append(obj)
                            else: #update the counters, they are demn important
                                res['update'].append(obj)
                        else:
                            no_symbolId = no_symbolId + 1 

                if no_symbolId > 0:
                    cli.danger('Post', f'Found {no_symbolId} without symbolId')

            except Exception as x :
                cli.danger('Post', f'Error with {channel.name} ({channel.url}) posts')
                log.error("Post", x)
                # import sys, traceback
                # traceback.print_exc(file=sys.stdout)

                print(x)

        for tw in twitter_accounts:
            cli.normal('Post',f'Getting latest tweets from {tw.name}')
            #TODO: once the developer account will

        return res