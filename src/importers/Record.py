import datetime
import yfinance as yf
import pandas as pd

from db.entities.Record import Record
from db.entities.Symbol import Symbol
from importers.BaseImporter import BaseImporter
import utils.cli as cli
import utils.logger as log

class RecordImporter(BaseImporter):

    def __init__(self):
        super().__init__('Record')

    def yahoo_finance(self, symbolIds_to_refresh):
        #Yahoo does support batch requests:
        # data = yf.download(tickers = "SPY AAPL MSFT", period = "ytd")
        mapped = {}
        # cli.warning('Record',f'Learn how to use Yahoo Finance bulk.')
        for symbolId, days_from_last_update in symbolIds_to_refresh.items():
            try:
                company = yf.Ticker(symbolId)
                
                # print(f'days_from_last_update: {days_from_last_update}')
                # days_from_last_update = (datetime.datetime.now() - days_from_last_update).days if days_from_last_update is not None else None
                
                # figure out how much data to query from yahoo
                if days_from_last_update is None:
                    period = "5y"
                elif days_from_last_update < 5:
                    period = "5d"
                elif days_from_last_update < 28:
                    period = "1mo"
                elif days_from_last_update < 80:
                    period = "1mo"
                elif days_from_last_update < 100:
                    period = "6mo"
                elif days_from_last_update < 150:
                    period = "1y"
                elif days_from_last_update < 300:
                    period = "2y"
                else:
                    period = "5y"

                # cli.normal('Record', f'Fetching {period} records for {symbolId}...')
                
                # period = "5y" if days_from_last_update is None else  # valid periods: 1d,5d,1mo,3mo,6mo,1y,2y,5y,10y,ytd,max
                df = company.history(period=period)
                if days_from_last_update is not None:
                    df = df[df.index > (datetime.datetime.now() - datetime.timedelta(days = days_from_last_update))]

                for index,row in df.iterrows():
                    if not (pd.isna(row['Open']) or pd.isna(row['Close']) or pd.isna(row['High']) or pd.isna(row['Low']) or pd.isna(row['Volume'])):
                        obj = Record()
                        obj.symbolId = symbolId
                        obj.date = index.to_pydatetime() #datetime.datetime.strptime(index, '%Y-%m-%d')
                        obj.createdAt = datetime.datetime.now()
                        obj.open = round(row['Open'], 2) if row['Open'] is not None else 0
                        obj.close = round(row['Close'], 2) if row['Close'] is not None else 0
                        # obj.adjustedClose = row['Close']
                        obj.high = round(row['High'], 2) if row['High'] is not None else 0
                        obj.low = round(row['Low'], 2) if row['Low'] is not None else 0
                        obj.volume = round(row['Volume'], 2) if row['Volume'] is not None else 0
                        obj.change = round((100 * (obj.close - obj.open) / obj.open), 2) if obj.open > 0 else 0
                        obj.volatility = round((100 * (obj.high - obj.low) / obj.low), 2) if obj.low > 0 else 0
                        
                        if symbolId not in mapped.keys():
                            mapped[symbolId] = []

                        mapped[symbolId].append(obj)
                
            except Exception as x :
                cli.danger('Record',f'ERROR getting data for {symbolId} from {self.API}!')
                log.error("Record", x)
                print(x)

        return mapped


    def iex(self, symbolIds):
        pass

    def alpha_vantage(self, symbolIds):
        pass

    def scrape(self, symbolIds):
        pass

    def finnhub(self,symbolIds_to_refresh):
        pass

    def marketstack(self,symbolIds_to_refresh):
        pass