#!/usr/bin/env python3

from pathlib import Path
import os.path
import datetime

import mail1
import jinja2
# from jinja2 import Environment, PackageLoader, select_autoescape

from decorators import time_meter
import config

@time_meter()
def send_email(subject = 'Test',  html_text = '<h1>This is a test!</h1>'):
    # https://pypi.org/project/mail1/
    subject = f'Test'

    mail1.send(subject=subject,
        text_html= html_text,
        recipients=config.RECIPIENTS.split(';'),
        sender=config.SENDER,
        smtp_host=config.SMTP_HOST,
        smtp_port=config.SMTP_PORT)

def render_template(data, template_path = 'mail.jinja2.html', save_as = None):
    #https://jinja.palletsprojects.com/en/2.11.x/api/
    templateLoader = jinja2.FileSystemLoader(searchpath="./templates") #ROOT directory of templates, all the template path MUST be relative to it
    templateEnv = jinja2.Environment(loader=templateLoader)
    template = templateEnv.get_template(template_path)
    res = template.render(data = data)

    if save_as is not None:
        common.save_as_file(res, save_as)

    return res

if __name__ == '__main__':
    data = {'today':datetime.datetime.now().strftime('%Y-%m-%d')}
    print(render_template(data))