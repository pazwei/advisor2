#!/usr/bin/env python3
import signal
import sys
import argparse

from importers.BaseImporter import BaseImporter
from importers.BalanceSheet import BalancesheetImporter
from importers.Cashflow import CashflowImporter
from importers.IncomeStatement import IncomeStatementImporter
from importers.Earning import EarningImporter
from importers.AnalystRecommendation import AnalystRecommendationImporter
from importers.Ipo import IpoImporter
from importers.Record import RecordImporter
from importers.Article import ArticleImporter
# from importers.Sustainability import SustainabilityImporter
from importers.Symbol import SymbolImporter
from importers.Post import PostImporter
from importers.Dividend import DividendImporter

import quotes.quote as quote

###########
# globals #
###########

balancesheet_importer = BalancesheetImporter()
cashflow_importer = CashflowImporter()
incomestatement_importer = IncomeStatementImporter()
record_importer = RecordImporter()
article_importer = ArticleImporter()
analyst_recommendation_importer = AnalystRecommendationImporter()
earnings_importer = EarningImporter()
# sustainability_importer = SustainabilityImporter()
ipo_importer = IpoImporter()
dividend_importer = DividendImporter()
symbol_importer = SymbolImporter()
post_importer = PostImporter()

prioritiees = []
symbols = []
entities = []
email = False

def graceful_exit(signal, frame):
    print(quote.get())
    sys.exit(0)

def set_args():
    global priorities
    global symbols
    global entities
    # global email

    # https://docs.python.org/3/library/argparse.html
    parser = argparse.ArgumentParser(description='Refresh database')
    parser.add_argument("--p", "--priorities", help="Limit symbols by priorities (sort of groups) to refresh, seperated by comma", type=str, default='')
    parser.add_argument("--s", "--symbols", help="Limit symbols to refresh, seperated by comma", type=str, default='')
    parser.add_argument("--e", "--entities", help="Items to fetch [balancesheet|incomestatement|cashflow|record|event|symbol|dividend|earning|article|analyst|ipo|sustainability] seperated by comma", type=str, default='watch')
    # parser.add_argument("--email", help="Send summary email when done", type=bool, default=False)
    
    args = parser.parse_args()
    priorities = [int(item) for item in args.p.split(',') if len(item) > 0]
    symbols = [item for item in args.s.split(',') if len(item) > 0]
    entities = [item for item in args.e.lower().split(',') if len(item) > 0]
    # email = args.email

def main():
    global priorities
    global symbols
    global entities
    # global email

    if 'auto' in entities or entities is None:
        items = BaseImporter.get_db_overview()
        entities = sorted([item.lower() for item in items if item['needsUpdate']], key=lambda i: ['latestItemAge'], reverse=True)
        
        #Sort by age, articles and records first
        if 'article' in entities:
            entities.remove('article')
            entities.insert(0,'article')

        if 'post' in entities:
            entities.remove('post')
            entities.insert(1,'post')

        if 'record' in entities:
            entities.remove('record')
            entities.insert(2,'record')
        
        print(f'{entities} needs to be updated')

    if 'post' in entities:
        post_importer.refresh()

    if 'article' in entities:
        article_importer.refresh(priorities_limit = priorities, symbolIds_limit=symbols)

    if 'record' in entities:
        record_importer.refresh(priorities_limit = priorities, symbolIds_limit=symbols)   

    if 'analyst' in entities:
        analyst_recommendation_importer.refresh(priorities_limit = priorities, symbolIds_limit=symbols)

    if 'cashflow' in entities:
        cashflow_importer.refresh(priorities_limit = priorities, symbolIds_limit=symbols)
    
    if 'incomestatement' in entities:
        incomestatement_importer.refresh(priorities_limit = priorities, symbolIds_limit=symbols)

    if 'balancesheet' in entities:
        balancesheet_importer.refresh(priorities_limit = priorities, symbolIds_limit=symbols)

    if 'dividend' in entities:
        dividend_importer.refresh(priorities_limit = priorities, symbolIds_limit=symbols)

    if 'earning' in entities:
        earnings_importer.refresh(priorities_limit = priorities, symbolIds_limit=symbols)

    # if 'sustainability' in entities:
    #     sustainability_importer.refresh(priorities_limit = priorities, symbolIds=symbols)

    if 'symbol' in entities:
        symbol_importer.refresh()

    if 'ipo' in entities:
        ipo_importer.refresh()

if __name__ == '__main__':
    signal.signal(signal.SIGINT, graceful_exit)
    set_args()
    main()