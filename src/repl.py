help_text = """
Use the REPL to interact with the advisor. See latest news, make queries, export csv data, run back-tests on analysis sheets.
Enjoy making money.
"""

def echo(*args):
    print(', '.join(args))

commands = {
    'help':{
        'description':'text to echo back',
        'handler':'echo'
    },
    'csv':{
        'description':'',
        'handler':''
    },
    'backtest':{
        'description':'',
        'handler':''
    },
    'getdata':{
        'description':'',
        'handler':''
    },
    'email':{
        'description':'',
        'handler':''
    },
}

def read_line():
    command = ''
    while command.lower() != 'exit':
        input_str = input('> ')
        params = input_str.split(' ')
        command = params[0]
        available_commands = commands.keys()

        if command == 'desc' and params[1] in available_commands:
            print(commands[params[1]]['description'])
        elif command == 'commands':
            for item in available_commands:
                print(f'{item} : {commands[item]["description"]}')
        elif command in available_commands:
            args = [f'"{a}"' for a in params[1:]]
            binded = commands[command]['handler']
            cmd = f"{binded}({','.join(args)})"
            print(f'[Executing {cmd}]')
            try:
                eval(cmd)
            except Exception as e:
                print(e)
        elif command == 'quite':
            continue
        else:
            print('[Error Unknown Command]')

if __name__ == '__main__':
    print(help_text)
    print(f'Type "exit" to quite the REPL')
    read_line()