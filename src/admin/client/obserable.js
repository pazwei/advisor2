class Observable{
    constructor(data, observers){
        this.data = data;
        this.observers = observers; // {name:'', updateFunction:() => {}}
    }

    addObserver(updateFunction){
        this.observers.push(updateFunction);
    }

    removeObserver(){

    }

    broadcast(data){
        for(observer of this.observerse){
            console.log(`Updating ${observer.name}...`);
            observer.updateFunction(data);
        }
    }


}