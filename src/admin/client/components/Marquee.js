import {BaseHTMLElement} from './BaseHTMLElement.js'

class Marquee extends BaseHTMLElement  {
    constructor() {
      super();
      //https://codepen.io/thomasbormans/pen/EjMBqO
      this.id = this.generateId('marquee');
    }
  
    set message(message) {
      this.setAttribute("message", this.sanitizeHTML(message || 'test'));
    }
  
    get message() {
      return this.hasAttribute("message");
    }
    
    set color(color) {
      this.setAttribute("color", this.sanitizeHTML(color || 'black'));
    }
  
    get color() {
      return this.hasAttribute("color");
    }

    set bgcolor(bgcolor) {
      this.setAttribute("bgcolor", this.sanitizeHTML(bgcolor || 'inherit'));
    }
  
    get bgcolor() {
      return this.hasAttribute("bgcolor");
    }

    set speed(speed) {
      this.setAttribute("speed", this.sanitizeHTML(speed || 15));
    }
  
    get speed() {
      return this.hasAttribute("speed");
    }

    set height(height) {
      this.setAttribute("height", parseInt(this.sanitizeHTML(height || '50px')));
    }
  
    get height() {
      return this.hasAttribute("height");
    }

    connectedCallback() {
      this.render();
    }

    render(){
      this.innerHTML = `
      <style>
        #${this.id} {
          line-height: ${this.getAttribute("height")};
          background-color: ${this.getAttribute("bgcolor")},
          color: ${this.sanitizeHTML(this.getAttribute("color"))};
          white-space: nowrap;
          overflow: hidden;
          box-sizing: border-box;
        }
        #${this.id} p {
          display: inline-block;
          padding-left: 100%;
          animation: #${this.id} ${this.sanitizeHTML(this.getAttribute("speed"))}s linear infinite;
        }
        @keyframes #${this.id} {
          0%   { transform: translate(0, 0); }
          100% { transform: translate(-100%, 0); }
        } 
      </style>
      <div id="${this.id}">
        <p>
        ${this.getAttribute("message")}
        </p>
      </div>
      `;
    }
  }
  
  customElements.define('admin-marquee', Marquee);