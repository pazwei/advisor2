import {BaseHTMLElement} from './BaseHTMLElement.js'

class SideMenu extends BaseHTMLElement {
    constructor() {
      // Аlways call super() first in the constructor.
      // This also calls the extended class' constructor.
      super();
    }
  
    connectedCallback() {
      this.render();
    }

    render(){
      this.innerHTML = `
        
      `;
    }
  }
  
  customElements.define('admin-sidemenu', SideMenu);
