import {BaseHTMLElement} from './BaseHTMLElement.js'

class Alert extends BaseHTMLElement  {
    constructor() {
      // Аlways call super() first in the constructor.
      // This also calls the extended class' constructor.
      super();
      this.id = this.generateId('alert');
    }
  
    set message(message) {
      this.setAttribute("message", this.sanitizeHTML(message));
    }
  
    get message() {
      return this.hasAttribute("message");
    }

    set color(color) {
      this.setAttribute("color", this.sanitizeHTML((color));
    }
  
    get color() {
      return this.hasAttribute("color");
    }

    connectedCallback() {
      this.render();
    }

    render(){
      this.innerHTML = `
        <div id="${this.id}" class="alert alert-${this.getAttribute("color")}" role="alert">
        ${this.getAttribute("message")}
        </div>
      `;
    }
  }
  
  customElements.define('admin-alert', Alert);