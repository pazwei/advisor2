import {BaseHTMLElement} from './BaseHTMLElement.js'

class FormAutoComplete extends BaseHTMLElement {
    constructor() {
      super();
      this.id = this.generateId();
    }
  
    render() {
      this.innerHTML = `
      <div class="mb-3">
        <label for="${this.id}" class="form-label">${this.required ? '<span style="color:red">*</span> ' : ''} ${this.label}</label>
        <input type="${this.type}" class="form-control" id="${this.id}" name=${this.name} placeholder="${this.placeholder}" />
        ${this.valid ?
        `<div class="valid-feedback">
          ${this.validMessage}
        </div>` :
        `<div class="invalid-feedback">
        ${this.invalidMessage}
        </div>`}
      </div>
      `
    }
  }
  
  customElements.define('form-autocomplete', FormAutoComplete);