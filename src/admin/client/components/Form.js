class Form extends DropDown {
    constructor() {
      // Аlways call super() first in the constructor.
      // This also calls the extended class' constructor.
      super();
    }
  
    fancyHoverMethod() {
    }
  }
  
  customElements.define('form-builder', Form);