class MainMenu extends DropDown {
    constructor() {
      // Аlways call super() first in the constructor.
      // This also calls the extended class' constructor.
      super();
    }
  
    fancyHoverMethod() {
    }
  }
  
  customElements.define('main-menu', MainMenu);