import {BaseHTMLElement} from './BaseHTMLElement.js'

class Toast extends BaseHTMLElement {
    constructor() {
      super();
      this.id = this.generateId('taost');
    }
  
    set message(message) {
      this.setAttribute("fill", message);
    }
  
    get message() {
      return this.hasAttribute("message");
    }

    set color(color) {
      this.setAttribute("color", color);
    }
  
    get color() {
      return this.hasAttribute("color");
    }

    connectedCallback() {
      this.render();
      let toastElList = [].slice.call(document.querySelectorAll('.toast'));
      let toastList = toastElList.map(function (toastEl) {
        return new bootstrap.Toast(toastEl, {autoHide: true, animation: true, delay: 5 * 1000})
      });
      toastList.forEach(t => {t.show();});
  }

    update(data){
      this.render();
    }

    render(){
      this.innerHTML = `
      <div id="${this.id}" class="toast d-flex align-items-center text-white bg-${this.getAttribute("color")} border-0 .position-absolute" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-body">
          ${this.getAttribute("message")}
        </div>
        <button type="button" class="btn-close btn-close-white ms-auto me-2" data-bs-dismiss="toast" aria-label="Close"></button>
      </div>`;
    }
  }
  
  customElements.define('admin-toast', Toast);