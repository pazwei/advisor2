import {BaseHTMLElement} from './BaseHTMLElement.js'

class Spinner extends BaseHTMLElement {
    constructor(message = 'Loading...') {
      // Аlways call super() first in the constructor.
      // This also calls the extended class' constructor.
      super();
      this.id = this.generateId();
      this.message = message;
    }
  
    connectedCallback() {
      this.render();
    }

    show(){

    }

    hide(){
      
    }

    render(){
      this.innerHTML = `
      <div class="spinner-border" role="status">
        <span class="visually-hidden">${this.message}</span>
      </div>`;
    }
  }
  
  customElements.define('admin-spinner', Spinner);