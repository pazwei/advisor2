export class BaseHTMLElement extends HTMLElement {
    constructor() {
      // Аlways call super() first in the constructor.
      // This also calls the extended class' constructor.
      super();
    }
  
    static get observedAttributes() {
      return [];
    }

    generateId(name){
      let str = '1234567890qwertyuiopasdfghjklzxcvbnm';
      let res = '';
      for(let i=0; i < 4; i++){
        res += str[Math.round(Math.random() * str.length)];
      }
      return name ? `name_${res}` : res;
    }

    connectedCallback() {
      this.render();
    }

    disconnectedCallback(){
      //TODO: remove listerers: this.firstChild.removeEventListener('click', this.addToCart);
      //TODO: animation fade animation
    }

    attributeChangedCallback(attr, oldValue, newValue){
      if(BaseHTMLElement.observedAttributes.indexOf(attr) > -1){
        this.render();
      }
    }

    update(data){
      this.render();
    }

    sanitizeHTML(str) {
      let temp = document.createElement('div');
      temp.textContent = str;
      return temp.innerHTML;
    };

    render(){
      return `BaseHTMLElement`;
      //this.firstChild.addEventListener('click', this.addToCart);
    }
}