// https://coherent-labs.com/posts/web-components/

class DataTable extends DropDown {
    constructor(columns, rows) {
      super();
      this.columns = columns;
      this.rows = rows;
      this.selectable = selectable;
      this.action = action;
      this.filter = filter;
      this.pageSizes = pageSizes;
      this.pageSize = 50;
      this.currentPage = currentPage;
      this.quickQueries = quickQueries;
      this.onDoubleClick = onDoubleClick;
      this.oncontextmenu = onContextMenu;


    }

    renderPagination(){
        return `
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">Next</a></li>
            </ul>
        </nav>
        `;
    }

    renderColumns(){
        return `
            <tr>
                ${this.selectable ? `<th></th>` : ''}
                ${this.columns.map(column =>
                    `<th style="cursor:pointer" id="${column.name}" onclick="alert('${column.caption}')">${column.caption}</th>`
                ).join('\n')}
            </tr>
        `;
    }

    renderRows(){
        return `
            ${this.rows.map(row => `
                <tr ondbclick="${this.onDoubleClick(`${JSON.stringify(row)}`)}">
                ${this.selectable ? `<td><input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked></td>` : ''}
                ${this.columns.map(column =>
                    `<td style="cursor:pointer" id="${column.name}" onclick="alert('${column.caption}')">${column.caption}</td>`
                ).join('\n')}
                </tr>
                `
            `).join('\n')}
            ;
    }

    renderQuickQueries(){
        return `
            <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                ${this.quickQueries.map(qq => `
                    <input type="radio" class="btn-check" name="${qq.name}" id="${qq.name}" autocomplete="off" ${qq.checked ? 'checked' : ''}>
                    <label class="btn btn-outline-primary" for="${qq.name}">${qq.caption}</label>
                `).join('\n')}
            </div>`;
    }

    render(){
        return `
            <div>
                ${this.renderQuickQueries()}
                <table>
                    <thead>${this.renderColumns()}</thead>
                    <tbody>${this.renderRows()}</tbody>
                </table>
                ${this.renderPagination()}
            </div>
        `;
    }
  
    connectedCallback() {

    }


    fancyHoverMethod() {
    }

    refresh(){

    }

    
    page(number){

    }

    rowDbClicked(row){

    }

    rowSelected(){

    }

    sort(){

    }

    query(){

    }

    update(){

    }

  }
  
  customElements.define('data-table', DataTable);