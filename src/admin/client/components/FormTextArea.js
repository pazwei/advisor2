import {BaseHTMLElement} from './BaseHTMLElement.js'

class FormTextarea extends BaseHTMLElement {
    constructor() {
      // Аlways call super() first in the constructor.
      // This also calls the extended class' constructor.
      super();
      this.id = this.generateId();
    }
  
    render() {
      this.innerHTML = `
      <div class="mb-3">
        <label for="${this.id}" class="form-label">${this.required ? '<span style="color:red">*</span> ' : ''} ${this.label}</label>
        <input type="${this.type}" class="form-control" id="${this.id}" name=${this.name} placeholder="${this.placeholder}" />
        <div class="valid-feedback">
          Looks good!
        </div>
        <div class="invalid-feedback">
          Looks bad!
        </div>
      </div>
      `
    }
  }
  
  customElements.define('form-textarea', FormTextarea);