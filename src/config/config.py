import configparser
import os
import socket

config = configparser.ConfigParser()

script_dir = os.path.dirname(__file__)
abs_config_file_path = os.path.join(script_dir, 'config.ini')
    
config.read(abs_config_file_path)

computer_name = socket.gethostname()

IS_PROD = bool(config['DEFAULT']['Environment']) == 'PROD'
VERBOSE = bool(config['DEFAULT']['Verbose'])
# SQLITE_FILENAME = config['DEFAULT']['SqliteFileName']
SQLITE_FILENAME_PATH = config['DEFAULT'][f'SqliteFilePath_{computer_name}']
ALPHA_VANTAGE_APIKEY = config['AlphaVantage']['ApiKey']
ALPHA_VANTAGE_DELAY = 0.0 if bool(config['AlphaVantage']['SkipDelay']) else float(config['AlphaVantage']['Delay'])

DANDELION_TOKEN = config['Dandelion']['Token']
DANDELION_SENTIMENT_COST = config['Dandelion']['SentimentCost']
DANDELION_ENTITY_EXTRACTION_COST = config['Dandelion']['EntityExtractionCost']

SMTP_USERNAME = config['Email']['Username']
SMTP_PASSWORD = config['Email']['Password']
SMTP_HOST = config['Email']['SmtpHost']
SMTP_PORT = config['Email']['SmtpPort']
SENDER = config['Email']['Sender']
# SmtpHost = mail.bbqcalc.com
# SmtpPort = 25
# Recipients = 'paz.weisman@gmail.com;paz.vaysman@outlook.com'
# Sender = 'My financial advisor'