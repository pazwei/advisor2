import random
from os import path

from bs4 import BeautifulSoup
import requests

def rebuild():
    sources = {
    'taleb':{'url':'https://www.goodreads.com/author/quotes/21559.Nassim_Nicholas_Taleb?page=%i%', 'pages':59},
    'funny':{'url':'https://www.goodreads.com/quotes/tag/funny?page=%i%', 'pages':100},
    'life':{'url':'https://www.goodreads.com/quotes/tag/life?page=%i%', 'pages':100},
    'love':{'url':'https://www.goodreads.com/quotes/tag/love?page=%i%', 'pages':100},
    'inspirational':{'url':'https://www.goodreads.com/quotes/tag/inspirational?page=%i%', 'pages':100},
    'motivational':{'url':'https://www.goodreads.com/quotes/tag/motivational?page=%i%', 'pages':100},
    'happiness':{'url':'https://www.goodreads.com/quotes/tag/happiness?page=%i%', 'pages':100},
    'relationships':{'url':'https://www.goodreads.com/quotes/tag/relationships?page=%i%', 'pages':100},
    'success':{'url':'https://www.goodreads.com/quotes/tag/success?page=%i%', 'pages':100},
    'books':{'url':'https://www.goodreads.com/quotes/tag/books?page=%i%', 'pages':100},
    'time':{'url':'https://www.goodreads.com/quotes/tag/time?page=%i%', 'pages':100},
    'science':{'url':'https://www.goodreads.com/quotes/tag/science?page=%i%', 'pages':100},
    'truth':{'url':'https://www.goodreads.com/quotes/tag/truth?page=%i%', 'pages':100},
    'wisdom':{'url':'https://www.goodreads.com/quotes/tag/wisdom?page=%i%', 'pages':100}
    }

    for source,data in sources.items():
        quotes = []
        for i in range(60):
            url = data['url'].replace('%i%',str(i)) #f'https://www.goodreads.com/author/quotes/21559.Nassim_Nicholas_Taleb?page={i}'
            res = requests.get(url)
            soup = BeautifulSoup(res.text, 'html.parser')
            elements = soup.find_all("div", class_="quoteText")
            print(f'Found on {url} {len(elements)} quotes')
            for q in elements:
                txt = q.text.strip().replace('\r','').replace('\n','').replace('     ','').replace('   ','')
                quotes.append(txt)

        script_dir = path.dirname(__file__)
        save_to_path = path.join(script_dir, f'{source}.txt')
        file = open(save_to_path,'w', encoding='utf-8')
        file.write('\n'.join(set(quotes)))
        file.close()

quotes = []
def get():
    global quotes

    if len(quotes) == 0:
        init()

    i = random.randint(0, len(quotes))
    return quotes[i]

def init(subjects=['taleb','programming','science', 'life']):
    global quotes
    script_dir = path.dirname(__file__)
        
    for subject in subjects:
        open_from_path = path.join(script_dir, f'{subject}.txt')
        file = open(open_from_path,'r', encoding='utf-8')
        quotes = quotes + file.read().split('\n')

if __name__ == '__main__':
    q = get()
    print(q)