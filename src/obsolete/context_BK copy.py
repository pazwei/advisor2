#!/usr/bin/env python3
from datetime import datetime, timedelta
import uuid
import os
import json

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean, JSON
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey
from sqlalchemy import create_engine
import hashlib

from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from sqlalchemy.ext.declarative import declarative_base

#https://rss.app/feeds/j59uhR7YTumQBrfw.xml #wallstreetbets as rss feed
# class SymbolMetaDataModel():
#     def __init__(self, hasAnalystReports = True, hasArticles = True, hasRecords = True, hasIncomeStatements = True, hasBalanceSheets = True, hasCashFlows = True, hasEarnings = True, hasSustainabilityReports = True, hasDividends = True):
#         self.hasAnalystReports = True
#         self.hasArticles = True
#         self.hasRecords = True
#         self.hasIncomeStatements = True
#         self.hasBalanceSheets = True
#         self.hasCashFlows = True
#         self.hasEarnings = True
#         self.hasSustainabilityReports = True
#         self.hasDividends = True

# TODO: Split this file to many files by entity, but keep a context file with all imports
Base = declarative_base()

class Symbol(Base):
    __tablename__ = 'symbols'

    id = Column(String(), primary_key=True)
    assetType = Column(String(), nullable=True)
    name = Column(String(), nullable=True)
    description = Column(String(), nullable=True)
    exchange = Column(String(), nullable=True)
    currency = Column(String(), nullable=True)
    exchange = Column(String(), nullable=True)
    indices = Column(String(), nullable=True) #csv of indices the stock is included in
    ipoDate = Column(String(), nullable=True)
    country = Column(String(), nullable=True)
    sector = Column(String(), nullable=True)
    industry = Column(String(), nullable=True)
    address = Column(String(), nullable=True)
    city = Column(String(), nullable=True)
    tags = Column(String(), nullable=True)
    website = Column(String(), nullable=True)
    primarySicCode = Column(String(), nullable=True)
    fullTimeEmployees = Column(Integer(), nullable=True)
    ceo = Column(String(), nullable=True)
    peers = Column(String(), nullable=True)
    dividendPaymentsPerYear = Column(Integer(), default=0, nullable=True)
    meta = Column(JSON(), nullable=True)
    isDeleted = Column(Boolean(), nullable=True, default=False)

    priority = Column(Integer(), nullable=False, default=-1)
    dateCreated = Column(DateTime(), nullable=False, default = datetime.now())

    digest = Column(JSON(), nullable=True)
    sustainability = Column(JSON(), nullable=True)

    def __repr__(self):
       return f'<Symbol id={self.id} name={self.name}>'

class Ipo(Base):
    __tablename__ = 'ipos'

    id = Column(String(), primary_key=True)
    dealId = Column(String(), nullable=False)
    dateCreated = Column(DateTime(), nullable=False, default = datetime.now())
    companyName = Column(String(), nullable=False)
    exchange = Column(String(), nullable=False)
    proposedSharePriceLow = Column(Float(), nullable=False) #proposedSharePrice=14.00-16.00
    proposedSharePriceHigh = Column(Float(), nullable=False) #proposedSharePrice=14.00-16.00
    openingPrice = Column(Float(), nullable=False)
    sharesOffered = Column(Integer(), nullable=False) #8,900,000
    sharedOutstanding = Column(Integer(), nullable=True)
    ipoDate = Column(DateTime(), nullable=False) #01/29/2021
    valueOfSharedOffered = Column(Integer(), nullable=False)
    employees = Column(String(), nullable=True) #4 (as of 01/20/2021)
    website = Column(String(), nullable=True)
    ceo = Column(String(), nullable=True)
    description = Column(String(), nullable=True)

class Feed(Base):
    __tablename__ = 'feeds'

    id = Column(Integer(), primary_key=True, autoincrement=True)
    dateCreated = Column(DateTime(), nullable=False, default = datetime.now())

    type = Column(String(), nullable=False) # RSS|ATOM|TWITTER|REDDIT
    url = Column(String(), unique=True)
    name = Column(String(), unique=True)
    isActive = Column(Boolean(), nullable=False, default=True)
    class FEED_TYPE:
        RSS = 'RSS'
        ATOM = 'ATOM'
        TWITTER = 'TWITTER'
        REDDIT = 'REDDIT'
        
    def __repr__(self):
       return f'<Feed id={self.id} symbolId={self.type} name={self.name}>'

class Post(Base):
    __tablename__ = 'posts'

    id = Column(Integer(), primary_key=True, autoincrement=True)
    dateCreated = Column(DateTime(), nullable=False, default = datetime.now())

    feedId = Column(Integer(), ForeignKey('feeds.id'), nullable=True)
    feed = relationship("Feed", backref=backref("posts", uselist=False), lazy='select')

    symbolIds = Column(String(), nullable = False) #will contain csv of found symbols

    link = Column(String(), nullable=False)
    title = Column(String(), nullable=False)
    description = Column(String(), nullable=True)
    author = Column(String(), nullable=True)
    hash = Column(String(), nullable=False) #since media outlets have referers, where each referer has it's own query param uuid for tracking, I need to make sure the description content is unique 
    naiveSentiment = Column(String(), nullable=True)

    ups = Column(Integer(), nullable=True)
    downs = Column(Integer(), nullable=True)
    awards = Column(Integer(), nullable=True)
    comments = Column(Integer(), nullable=True)

    publishedAt = Column(DateTime(), nullable=False, default = datetime.now())

    def join_texts(self):
        return f'{self.title}{"." if self.title[-1] != "." else ""} {self.description}'

    def create_hash(self):
        return hashlib.sha256(bytes(self.join_texts(), 'utf-8')).hexdigest()
        
    def __repr__(self):
       return f'<Post id={self.id} symbolIds={self.symbolIds} naiveSentiment={self.naiveSentiment}>'

class Article(Base):
    __tablename__ = 'articles'

    id = Column(Integer(), primary_key=True, autoincrement=True)
    dateCreated = Column(DateTime(), nullable=False, default = datetime.now())

    feedId = Column(Integer(), ForeignKey('feeds.id'), nullable=True)
    feed = relationship("Feed", backref=backref("articles", uselist=False), lazy='select') #back_populates="articles")

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("articles", uselist=True), lazy='select')

    link = Column(String(), nullable=False)
    title = Column(String(), nullable=False)
    description = Column(String(), nullable=True)
    source = Column(String(), nullable=True)

    hash = Column(String(), nullable=False) #since media outlets have referers, where each referer has it's own query param uuid for tracking, I need to make sure the description content is unique 
    naiveSentiment = Column(String(), nullable=True)
    sentiment = Column(String(), nullable=True)
    sentimentScore = Column(Float(), nullable=True)
    sentimentConfidence = Column(Float(), nullable=True)
    keywords = Column(String(), nullable=True)

    publishedAt = Column(DateTime(), nullable=False, default = datetime.now())

    def join_texts(self):
        return f'{self.title}{"." if self.title[-1] != "." else ""} {self.description}'

    def create_hash(self):
        return hashlib.sha256(bytes(self.join_texts(), 'utf-8')).hexdigest()

    def __repr__(self):
       return f'<Article id={self.id} symbol={self.symbolId} naiveSentiment={self.naiveSentiment}>'

class Record(Base):
    __tablename__ = 'records'

    id = Column(Integer(), primary_key=True, autoincrement=True)
    dateCreated = Column(DateTime, nullable=False, default = datetime.now())

    symbolId = Column(String(), ForeignKey('symbols.id'))
    symbol = relationship("Symbol", backref=backref("Record", uselist=True), lazy='select')

    date = Column(DateTime, nullable=False, default = datetime.now())

    open = Column(Float(), nullable=False)
    high = Column(Float(), nullable=False)
    low = Column(Float(), nullable=False)
    close = Column(Float(), nullable=False)
    adjustedClose = Column(Float(), nullable=True)
    volatility = Column(Float(), nullable=False)
    change = Column(Float(), nullable=False)
    volume = Column(Integer(), nullable=False)

    def __repr__(self):
       return f'<Record id={self.id} date={self.date.strftime("%Y-%m-%d")} close={self.close} change={self.change}>'

class Event(Base):
    __tablename__ = 'events'

    id = Column(Integer(), primary_key=True)
    dateCreated = Column(DateTime, nullable=False, default = datetime.now())

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("events", uselist=True), lazy='select')

    date = Column(DateTime(), nullable=False, default = datetime.now())
    name = Column(String(), nullable=True)
    effect = Column(String(), nullable=False)
    description = Column(String(), nullable=True)

    def __repr__(self):
       return f'<Event id={self.id} name={self.name} date={self.date.strftime("%Y-%m-%d")}>'

class BalanceSheet(Base):
    __tablename__ = 'balancesheets'

    id = Column(Integer(), primary_key=True)
    dateCreated = Column(DateTime, nullable=False, default = datetime.now())

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("balancesheets", uselist=True), lazy='select')

    term = Column(String(), nullable=False)

    fiscalDateEnding = Column(DateTime(), nullable=True)
    reportedCurrency = Column(String(), nullable=True)
    totalAssets = Column(Integer(), nullable=True)
    intangibleAssets = Column(Integer(), nullable=True)
    earningAssets = Column(Integer(), nullable=True)
    otherCurrentAssets = Column(Integer(), nullable=True)
    totalLiabilities = Column(Integer(), nullable=True)
    totalShareholderEquity = Column(Integer(), nullable=True)
    deferredLongTermLiabilities = Column(Integer(), nullable=True)
    otherCurrentLiabilities = Column(Integer(), nullable=True)
    commonStock = Column(Integer(), nullable=True)
    retainedEarnings = Column(Integer(), nullable=True)
    otherLiabilities = Column(Integer(), nullable=True)
    goodwill = Column(Integer(), nullable=True)
    otherAssets = Column(Integer(), nullable=True)
    cash = Column(Integer(), nullable=True)
    totalCurrentLiabilities = Column(Integer(), nullable=True)
    shortTermDebt = Column(Integer(), nullable=True)
    currentLongTermDebt = Column(Integer(), nullable=True)
    otherShareholderEquity = Column(Integer(), nullable=True)
    propertyPlantEquipment = Column(Integer(), nullable=True)
    totalCurrentAssets = Column(Integer(), nullable=True)
    longTermInvestments = Column(Integer(), nullable=True)
    netTangibleAssets = Column(Integer(), nullable=True)
    shortTermInvestments = Column(Integer(), nullable=True)
    netReceivables = Column(Integer(), nullable=True)
    longTermDebt = Column(Integer(), nullable=True)
    inventory = Column(Integer(), nullable=True)
    accountsPayable = Column(Integer(), nullable=True)
    totalPermanentEquity = Column(Integer(), nullable=True)
    additionalPaidInCapital = Column(Integer(), nullable=True)
    commonStockTotalEquity = Column(Integer(), nullable=True)
    preferredStockTotalEquity = Column(Integer(), nullable=True)
    retainedEarningsTotalEquity = Column(Integer(), nullable=True)
    treasuryStock = Column(Integer(), nullable=True)
    accumulatedAmortization = Column(Integer(), nullable=True)
    otherNonCurrrentAssets = Column(Integer(), nullable=True)
    deferredLongTermAssetCharges = Column(Integer(), nullable=True)
    totalNonCurrentAssets = Column(Integer(), nullable=True)
    capitalLeaseObligations = Column(Integer(), nullable=True)
    totalLongTermDebt = Column(Integer(), nullable=True)
    otherNonCurrentLiabilities = Column(Integer(), nullable=True)
    totalNonCurrentLiabilities = Column(Integer(), nullable=True)
    negativeGoodwill = Column(Integer(), nullable=True)
    warrants = Column(Integer(), nullable=True)
    preferredStockRedeemable = Column(Integer(), nullable=True)
    capitalSurplus = Column(Integer(), nullable=True)
    liabilitiesAndShareholderEquity = Column(Integer(), nullable=True)
    cashAndShortTermInvestments = Column(Integer(), nullable=True)
    accumulatedDepreciation = Column(Integer(), nullable=True)
    commonStockSharesOutstanding = Column(Integer(), nullable=True)

class IncomeStatement(Base):
    __tablename__ = 'incomestatements'

    id = Column(Integer(), primary_key=True)
    dateCreated = Column(DateTime, nullable=False, default = datetime.now())

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("incomestatements", uselist=True), lazy='select')

    term = Column(String(), nullable=False)

    fiscalDateEnding = Column(DateTime(), nullable=True)
    reportedCurrency = Column(String(), nullable=True)
    totalRevenue = Column(Integer(), nullable=True)
    totalOperatingExpense = Column(Integer(), nullable=True)
    costOfRevenue = Column(Integer(), nullable=True)
    grossProfit = Column(Integer(), nullable=True)
    ebit = Column(Integer(), nullable=True)
    netIncome = Column(Integer(), nullable=True)
    researchAndDevelopment = Column(Integer(), nullable=True)
    effectOfAccountingCharges = Column(Integer(), nullable=True)
    incomeBeforeTax = Column(Integer(), nullable=True)
    minorityInterest = Column(Integer(), nullable=True)
    sellingGeneralAdministrative = Column(Integer(), nullable=True)
    otherNonOperatingIncome = Column(Integer(), nullable=True)
    operatingIncome = Column(Integer(), nullable=True)
    otherOperatingExpense = Column(Integer(), nullable=True)
    interestExpense = Column(Integer(), nullable=True)
    taxProvision = Column(Integer(), nullable=True)
    interestIncome = Column(Integer(), nullable=True)
    netInterestIncome = Column(Integer(), nullable=True)
    extraordinaryItems = Column(Integer(), nullable=True)
    nonRecurring = Column(Integer(), nullable=True)
    otherItems = Column(Integer(), nullable=True)
    incomeTaxExpense = Column(Integer(), nullable=True)
    totalOtherIncomeExpense = Column(Integer(), nullable=True)
    discontinuedOperations = Column(Integer(), nullable=True)
    netIncomeFromContinuingOperations = Column(Integer(), nullable=True)
    netIncomeApplicableToCommonShares = Column(Integer(), nullable=True)
    preferredStockAndOtherAdjustments = Column(Integer(), nullable=True)

class CashFlow(Base):
    __tablename__ = 'cashflows'

    id = Column(Integer(), primary_key=True)
    dateCreated = Column(DateTime(), nullable=False, default = datetime.now())

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("cashflows", uselist=True), lazy='select')

    term = Column(String(), nullable=False)

    fiscalDateEnding = Column(DateTime(), nullable=False)
    reportedCurrency = Column(String(), nullable=False)
    investments = Column(Integer(), nullable=True)
    changeInLiabilities = Column(Integer(), nullable=True)
    cashflowFromInvestment = Column(Integer(), nullable=True)
    otherCashflowFromInvestment = Column(Integer(), nullable=True)
    netBorrowings = Column(Integer(), nullable=True)
    cashflowFromFinancing = Column(Integer(), nullable=True)
    otherCashflowFromFinancing = Column(Integer(), nullable=True)
    changeInOperatingActivities = Column(Integer(), nullable=True)
    netIncome = Column(Integer(), nullable=True)
    changeInCash = Column(Integer(), nullable=True)
    operatingCashflow = Column(Integer(), nullable=True)
    otherOperatingCashflow = Column(Integer(), nullable=True)
    depreciation = Column(Integer(), nullable=True)
    dividendPayout = Column(Integer(), nullable=True)
    stockSaleAndPurchase = Column(Integer(), nullable=True)
    changeInInventory = Column(Integer(), nullable=True)
    changeInAccountReceivables = Column(Integer(), nullable=True)
    changeInNetIncome = Column(Integer(), nullable=True)
    capitalExpenditures = Column(Integer(), nullable=True)
    changeInReceivables = Column(Integer(), nullable=True)
    changeInExchangeRate = Column(Integer(), nullable=True)
    changeInCashAndCashEquivalents = Column(Integer(), nullable=True)

class Earning(Base):
    __tablename__ = 'earnings'

    id = Column(Integer(), primary_key=True)
    dateCreated = Column(DateTime, nullable=False, default = datetime.now())

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("earnings", uselist=True), lazy='select')

    term = Column(String(), nullable=False)

    fiscalDateEnding = Column(DateTime(), nullable = True)
    reportedDate = Column(DateTime(), nullable = True)
    reportedEPS = Column(Float(), nullable = True)
    estimatedEPS = Column(Float(), nullable = True)
    surprise = Column(Float(), nullable = True)
    surprisePercentage = Column(Float(), nullable = True)

class Overview(Base):
    __tablename__ = 'overviews'

    id = Column(Integer(), primary_key=True)
    dateCreated = Column(DateTime, nullable=False, default = datetime.now())

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("overviews", uselist=True), lazy='select')

    fiscalYearEnd = Column(String(), nullable=True)
    latestQuarter = Column(DateTime(), nullable=True)
    marketCapitalization = Column(Integer(), nullable=True)
    ebitda = Column(Integer(), nullable=True)
    peRatio = Column(Float(), nullable=True)
    pegRatio = Column(Float(), nullable=True)
    bookValue = Column(Float(), nullable=True)
    dividendPerShare = Column(Float(), nullable=True)
    dividendYield = Column(Float(), nullable=True)
    eps = Column(Float(), nullable=True)
    revenuePerShareTTM = Column(Float(), nullable=True)
    profitMargin = Column(Float(), nullable=True)
    operatingMarginTTM = Column(Float(), nullable=True)
    returnOnAssetsTTM = Column(Float(), nullable=True)
    returnOnEquityTTM = Column(Float(), nullable=True)
    revenueTTM = Column(Integer(), nullable=True)
    grossProfitTTM = Column(Integer(), nullable=True)
    dilutedEPSTTM = Column(Float(), nullable=True)
    quarterlyEarningsGrowthYOY = Column(Float(), nullable=True)
    quarterlyRevenueGrowthYOY = Column(Float(), nullable=True)
    analystTargetPrice = Column(Float(), nullable=True)
    trailingPE = Column(Float(), nullable=True)
    forwardPE = Column(Float(), nullable=True)
    priceToSalesRatioTTM = Column(Float(), nullable=True)
    priceToBookRatio = Column(Float(), nullable=True)
    evToRevenue = Column(Float(), nullable=True)
    evToEBITDA = Column(Float(), nullable=True)
    beta = Column(Float(), nullable=True)
    week52High = Column(Float(), nullable=True)
    week52Low = Column(Float(), nullable=True)
    day50MovingAverage = Column(Float(), nullable=True)
    day200MovingAverage = Column(Float(), nullable=True)
    sharesOutstanding = Column(Integer(), nullable=True)
    sharesFloat = Column(Integer(), nullable=True)
    sharesShort = Column(Integer(), nullable=True)
    sharesShortPriorMonth = Column(Float(), nullable=True)
    shortRatio = Column(Float(), nullable=True)
    shortPercentOutstanding = Column(Float(), nullable=True)
    shortPercentFloat = Column(Float(), nullable=True)
    percentInsiders = Column(Float(), nullable=True)
    percentInstitutions = Column(Float(), nullable=True)
    forwardAnnualDividendRate = Column(Float(), nullable=True)
    forwardAnnualDividendYield = Column(Float(), nullable=True)
    payoutRatio = Column(Float(), nullable=True)
    dividendDate = Column(DateTime(), nullable=True)
    exDividendDate = Column(DateTime(), nullable=True)
    lastSplitFactor = Column(String(), nullable=True)
    lastSplitDate = Column(DateTime(), nullable=True)

class Executive(Base):
    __tablename__ = 'executives'

    id = Column(Integer(), primary_key=True)
    dateCreated = Column(DateTime, nullable=False, default = datetime.now())

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("executives", uselist=True), lazy='select')

    fullname = Column(String(), nullable=False)
    position = Column(String(), nullable=False)
    bio = Column(String(), nullable=False)
    compensation = Column(String(), nullable=False)
    age = Column(String(), nullable=False)
    since = Column(String(), nullable=False) #check to see if it can be an year or something like that
    retired = Column(Boolean(), nullable=False)
    retiredDate = Column(DateTime(), nullable=False)

class Dividend(Base):
    __tablename__ = 'dividends'

    id = Column(Integer(), primary_key=True)
    dateCreated = Column(DateTime, nullable=False, default = datetime.now())

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("dividends", uselist=True), lazy='select')

    announceDate = Column(DateTime(), nullable=False)
    exDate = Column(DateTime(), nullable=False)
    paymentDate = Column(DateTime(), nullable=False)
    payment = Column(Float(), nullable=False)

class Algorithm(Base):
    __tablename__ = 'algorithms'

    id = Column(Integer(), primary_key=True)
    dateCreated = Column(DateTime, nullable=False, default = datetime.now())

    name = Column(String(), nullable=True)
    execution_timeframe = Column(Integer(), nullable=True)
    formula = Column(String(), nullable=True)
    stockFilter = Column(String(), nullable=True)
    stockFilter_value = Column(String(), nullable=True)
    trendFilter = Column(String(), nullable=True)
    
class Recommendation(Base):
    __tablename__ = 'recommendations'

    id = Column(Integer(), primary_key=True)
    dateCreated = Column(DateTime, nullable=False, default = datetime.now())

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("recommendations", uselist=True), lazy='select')

    algorithmId = Column(Integer(), ForeignKey('algorithms.id'), nullable = True)
    algorithm = relationship("Algorithm", backref=backref("recommendations", uselist=True), lazy='select')

    # algorithm = ForeignKeyField(Algorithm, related_name='recommendations')
    date = Column(DateTime(), nullable=False, default = datetime.now())

    entryPrice = Column(Float(), nullable=True)
    exitPrice = Column(Float(), nullable=True)
    stopLoss = Column(Float(), nullable=True)
    execution_date = Column(DateTime(), nullable=True)
    dueDate = Column(DateTime(), nullable=False)
    outcome = Column(String(), nullable=True)
    comments = Column(String(), nullable=True)
    profit = Column(Float(), nullable=True)

class AnalystRecommendation(Base):
    __tablename__ = 'analystRecommendation'

    id = Column(Integer(), primary_key=True)
    dateCreated = Column(DateTime, nullable=False, default = datetime.now())

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("analyst_recommendations", uselist=True), lazy='select')

    date = Column(DateTime(), nullable=True)
    firm = Column(String(), nullable=True)
    fromGrade = Column(String(), nullable=True)
    toGrade = Column(String(), nullable=True)
    gradeAction = Column(String(), nullable=True)
    targetPriceHigh = Column(Float(), nullable=True)
    targetPriceLow = Column(Float(), nullable=True)

class SustainabilityReport(Base):

    __tablename__ = 'sustainabilityReports'

    id = Column(Integer(), primary_key=True)

    symbolId = Column(String(), ForeignKey('symbols.id'), nullable = True)
    symbol = relationship("Symbol", backref=backref("sustainabilityreports", uselist=True), lazy='select')
    dateCreated = Column(DateTime, nullable=False, default = datetime.now())
    
    year = Column(Integer(), nullable=True)
    quater = Column(Integer(), nullable=True)
    palmOil = Column(Boolean(), nullable=True)
    controversialWeapons = Column(Boolean(), nullable=True)
    gambling = Column(Boolean(), nullable=True)
    socialScore = Column(Float(), nullable=True)
    nuclear = Column(Boolean(), nullable=True)
    furLeather = Column(Boolean(), nullable=True)
    alcoholic = Column(Boolean(), nullable=True)
    gmo  = Column(Boolean(), nullable=True)
    catholic = Column(Boolean(), nullable=True)
    socialPercentile = Column(Float(), nullable=True)
    peerCount = Column(Integer(), nullable=True)
    governanceScore = Column(Float(), nullable=True)
    environmentPercentile = Column(Float(), nullable=True)
    animalTesting  = Column(Boolean(), nullable=True)
    tobacco  = Column(Boolean(), nullable=True)
    totalEsg = Column(Float(), nullable=True)
    highestControversy = Column(Integer(), nullable=True)
    esgPerformance = Column(String(), nullable=True)
    coal = Column(Boolean(), nullable=True)
    pesticides = Column(Boolean(), nullable=True)
    adult = Column(Boolean(), nullable=True)
    percentile = Column(Float(), nullable=True)
    peerGroup = Column(String(), nullable=True)
    smallArms = Column(Boolean(), nullable=True)
    environmentScore = Column(Integer(), nullable=True)
    governancePercentile = Column(Float(), nullable=True)
    militaryContract = Column(Boolean(), nullable=True)

def get_session():
    script_dir = os.path.dirname(__file__)
    abs_sqlite_file_path = os.path.join(script_dir, '..', 'data', 'db.sqlite')
    db_exists = os.path.exists(abs_sqlite_file_path)
    
    engine = create_engine(f'sqlite:///{abs_sqlite_file_path}', connect_args={'check_same_thread': False}, echo=False) #, pool_size=10
    
    if(db_exists == False):
        Base.metadata.create_all(engine)

    # https://stackoverflow.com/questions/6297404/multi-threaded-use-of-sqlalchemy
    session_factory = sessionmaker(bind=engine, autoflush=False) #, expire_on_commit=False
    session = scoped_session(session_factory)

    #Single thread session
    # Session = sessionmaker(bind=engine, autoflush=False)
    # session = Session()

    return session

def __read_seed_json_file__():
    script_dir = os.path.dirname(__file__)
    abs_seed_file_path = os.path.join(script_dir, 'seed.json')
    
    with open(abs_seed_file_path) as json_file:
        data = json.load(json_file)
        return data

    # TODO: 
    # https://il.investing.com/indices/us-spx-500-historical-data
    # https://www.investing.com/indices/nasdaq-composite-historical-data
    # http://cafim.sssup.it/~giulio/other/alpha_vantage/index.html
    # https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&outputsize=full&symbol=GE&apikey=[APIKEY]&datatype=csv
    # http://old.nasdaq.com/screening/companies-by-name.aspx?letter=0&exchange=nyse&render=download

if __name__ == '__main__':
    session = get_session()