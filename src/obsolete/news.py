#!/usr/bin/env python3

import datetime
from xml.etree import ElementTree as etree
import hashlib
import html
import urllib.parse

import requests
import bs4 as bs

from db import Article, Feed
import queries
import dandelion

headers_dict = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36', "Upgrade-Insecure-Requests": "1","DNT": "1","Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8","Accept-Language": "en-US,en;q=0.5","Accept-Encoding": "gzip, deflate"}
 
#{'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', 'accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9', 'accept-language':'he-IL,he;q=0.9,en-US;q=0.8,en;q=0.7', "Upgrade-Insecure-Requests": "1","DNT": "1"}

#Use Rss.app to convert social media feeds into RSS feeds

def get_rss_feed(feed):
    print(f'<RSS link="{feed.url}" />')
    articles = []
    try:
        s = requests.Session()
        parsed_link = urllib.parse.urlparse(feed.url)
        s.get(f'{parsed_link.scheme}://{parsed_link.netloc}', headers=headers_dict, timeout=5, allow_redirects = False)
        rss = s.get(feed.url, headers=headers_dict, timeout=5)
        # rss = requests.get(link, headers=headers_dict, timeout=5)

        root = etree.fromstring(rss.text)
        item = root.findall('channel/item')

        for entry in item:   
            title = html.unescape(entry.findtext('title')) 
            link = entry.findtext('link')
            description = html.unescape(bs.BeautifulSoup(title if entry.findtext('description') is None else entry.findtext('description'), features="html.parser").text)
            articles.append(Article(feedId=feed.id, title=title, link=link, description=description, descriptionHash=Article.create_hash(description)))
    except requests.errors.Timeout as e:
        print(e)


    return articles

def get_atom_feed(feed):
    print(f'<ATOM link="{feed.url}" />')
    articles = []
    try:
        res = requests.get(feed.url, headers=headers_dict, timeout=5)

        root = etree.fromstring(res.text)
        atom_prefix = '{http://www.w3.org/2005/Atom}'
        for entry in root.findall(f'{atom_prefix}entry'):
            title = html.unescape(entry.findtext(f'{atom_prefix}title')) 
            link = entry.findtext(f'{atom_prefix}link')
            description = html.unescape(bs.BeautifulSoup(entry.findtext(f'{atom_prefix}summary'), features="html.parser").text) if entry.find(f'{atom_prefix}summary') is not None else 'N/A'
            descriptionHash = None if description == None else hashlib.sha256(_to_binary(description)).hexdigest()
            articles.append(db.Article(title=title, link=link, description=description, descriptionHash=descriptionHash))
    except requests.errors.Timeout as e:
        print(e)

    return articles

def insert_new_articles():
    from_date = datetime.datetime.now() + datetime.timedelta(days=-60)
    existing_articles = queries.get_all_articles(from_date)
    all_feeds = queries.get_all_feeds()
    new_articles = []

    for feed in all_feeds:
        if feed.type == 'RSS':
            new_articles += get_rss_feed(feed)
        else:
            new_articles += get_atom_feed(feed)

    existing_hash = [a.hash for a in existing_articles]
    new_non_existing_articles = [a for a in new_articles if a.hash not in existing_hash]

    for item in new_non_existing_articles:
        txt = Article.join_texts(item.title, item.description)

        item.keywords = dandelion.extract_entities(txt)

        sentiment = dandelion.analyze_sentiment(txt)
        item.sentiment = sentiment['type']
        item.sentimentScore = sentiment['score']
        item.sentimentConfidence = sentiment['confidence']

    queries.bulk_insert(new_non_existing_articles)

    return new_non_existing_articles

if __name__ == '__main__':
    print(get_rss_feed(db.Feed(url='https://www.nasdaq.com/feed/rssoutbound?category=Stocks', id=-1)))