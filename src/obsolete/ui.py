# https://build-system.fman.io/pyqt5-tutorial
# https://www.learnpyqt.com/tutorials/first-steps-qt-creator/

# from PyQt5.QtWidgets import *

# app = QApplication([])
# button = QPushButton('Click')
# def on_button_clicked():
#     alert = QMessageBox()
#     alert.setText('You clicked the button!')
#     alert.exec_()

# button.clicked.connect(on_button_clicked)
# button.show()
# app.exec_()

# from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout

# app = QApplication([])
# window = QWidget()
# layout = QVBoxLayout()
# layout.addWidget(QPushButton('Top'))
# layout.addWidget(QPushButton('Bottom'))
# window.setLayout(layout)
# window.show()
# app.exec_()


# try:
#   from tkinter import *
#   from tkinter.ttk import *
# except ImportError :  
#   print("exception in importing module")

# class MyWindow(Frame):


#     def CreateUI(self):
#         tv = Treeview(self)
#         tv['columns'] = ('Name', 'Mobile', 'course')
#         tv.heading("#0", text='RollNo', anchor='w')
#         tv.column("#0", anchor="w")
#         tv.heading('Name', text='Name')
#         tv.column('Name', anchor='center', width=100)
#         tv.heading('Mobile', text='Mobile')
#         tv.column('Mobile', anchor='center', width=100)
#         tv.heading('course', text='course')
#         tv.column('course', anchor='center', width=100)
#         tv.grid(sticky = (N,S,W,E))
#         self.treeview = tv
#         self.grid_rowconfigure(0, weight = 1)
#         self.grid_columnconfigure(0, weight = 1)
# root = Tk()
# MyWindow(root)
# root.mainloop()


try:
    from tkinter import * 
except ImportError:
    from Tkinter import *

root = Tk()

height = 5
width = 5
for i in range(height): #Rows
    for j in range(width): #Columns
        b = Entry(root, text="")
        b.grid(row=i, column=j)

mainloop()