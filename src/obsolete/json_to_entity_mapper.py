import datetime
import os
import json
import re
import pipes

__maps__ = {}

def __init__():
    """
    Load all json mappings once, on module load.
    """
    
    script_dir = os.path.dirname(__file__)
    abs_path_to_json_dir = os.path.join(script_dir, 'maps')
    json_files = [f for f in os.listdir(abs_path_to_json_dir) if os.path.isfile(os.path.join(abs_path_to_json_dir, f))]
    
    for item in json_files:
        file = open(os.path.join(abs_path_to_json_dir, item))
        data =  json.load(file)
        file.close()
        entity_name = os.path.splitext(item)[0]
        __maps__[entity_name] = data

__init__()

def fill_entity(pristine_db_entity, api_name, json_response):
    """
    Each api will know exactly what db entities it can fulfill kind of data it have.
    For abstraction between api and db there is a mapper provided 
    Use implicit mapping - if there is a need to transform json to entity field then write it, if not, don't try to translate.
    Don't use explicit mapping were each field MUST be regarded
    If there is a need for transformation use a pipe function if the syntax of _pipe(%FieldName%, other args)
    If there is not special map for the field, use naive translation by type (__get_value_by_entity_field_type)
    {'entity_field':'json_field|pipe|optional pipe args'}
    """
    entity_name = pristine_db_entity.__class__.__name__
    entity_map = __maps__[entity_name][api_name]
    for entity_field in dir(pristine_db_entity):
        value = None
        if entity_field in entity_map.keys(): # field is mapped
            response_field_name = entity_map[entity_field].split('|')[0]
            if '|' in entity_map[entity_field]: #with pipe
                pipe = entity_map[entity_field].split('|')[1:]
                if len(pipe) > 1:
                    p = f"{pipe[0]}('{json_response[response_field_name]}','{pipe[1]}')"
                    # print(p)
                    value = eval(p) #for datetime parse pipe format
                else:
                    p = f"{pipe[0]}('{json_response[response_field_name]}')"
                    # print(p)
                    value = eval(p)
            else: #without pipe
                # print(json_response)
                value = __get_value_by_regex(json_response[response_field_name]) #if response_field_name in json_response.keys() else None
        elif entity_field in json_response.keys(): #exact field is on entity, json could have different type str for example
            value = __get_value_by_regex(json_response[entity_field]) 
        else: #python object prototype field
            continue
        
        setattr(pristine_db_entity, entity_field, value)

    return pristine_db_entity

def __get_value_by_regex(json_value):
    """
    json_value can be anything, mostly string, but it need to be auto casted before saving it to DB.
    It's like an auto pipe for simple string parsing.
    Use regex to find the type of the underlying stringed type
    """
    #https://www.w3schools.com/python/python_regex.asp
    if json_value is None:
        return None

    str_value = str(json_value)

    if str_value.lower() in ['none','n/a','null']:
        return None
    elif re.match('^\d{4}-\d{2}-\d{2}$',str_value) is not None:
        return datetime.datetime.strptime(json_value, '%Y-%m-%d')
    elif re.match('^-*\d*\.\d*$',str_value) is not None:
        return float(json_value)
    elif re.match('^-*\d*$',str_value) is not None:
        return int(json_value)
    else:
        return json_value

########
# Pipes
########
def to_float(value):
    return float(value)

def to_int(value):
    return int(value)

def to_datetime(value, format):
    return datetime.datetime.strptime(value, format)

def from_timestamp(value):
    res = datetime.datetime.fromtimestamp(int(value) / 1000.0)
    return res

if __name__ == '__main__':
    print(__maps__)