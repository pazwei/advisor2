import db.queries as queries
from db.context import Overview
from db.context import get_session
from bs4 import BeautifulSoup


def __get_executives(ticker):
    url = f'https://www.reuters.com/finance/stocks/company-officers/{ticker}'
    
    try:
        r1 = requests.get(url, headers = config.userAgent)
        soup1 = BeautifulSoup(r1.text, 'html.parser')
        mainTable = soup1.findAll('tbody', {'class': 'dataSmall'})[0]
        bioTable = soup1.findAll('tbody', {'class': 'dataSmall'})[1]
        compensationTable = soup1.findAll('tbody', {'class': 'dataSmall'})[2]
    except Exception as ex:
        utils.notify(f'Error while getting {ticker} executives! {ex}', 'danger')
        return (0,0)

    existing_executives = list(entities.Stock.select().where(entities.Stock.ticker == ticker)[0].executives)
    new_executives = []
    count_new = 0
    count_retired = 0

    for index, row in enumerate(mainTable.findAll('tr')[1:]):
        cols = row.findAll('td')
        fullname = cols[0].text.strip()
        age = cols[1].text.strip()
        since = cols[2].text.strip()
        position = cols[3].text.strip()
        bio = bioTable.findAll('tr')[index + 1].text.strip().replace(fullname, '', 1).strip()
        compensation = None if '--' in compensationTable.findAll('tr')[index + 1].text.strip() else int(compensationTable.findAll('tr')[index + 1].text.strip().replace(',','').replace(fullname, '', 1))
        new_executives.append({'fullname': fullname, 'bio': bio, 'age': age, 'since': since, 'compensation': compensation, 'position': position})

    existing = [f'{executive.fullname}' for executive in existing_executives]
    for new in new_executives:
        if f'{new["fullname"]}' not in existing:
            entities.Executive.create(stock = ticker, fullname = new['fullname'], bio = new['bio'], position = new['position'], compensation = new['compensation'], age = new['age'], since = new['since'], retired = False, retired_date = None)
            count_new = count_new + 1

    news = [executive['fullname'] for executive in new_executives]
    for exist in existing_executives:
        if f'{exist.fullname}' not in news and exist.retired == False:
            utils.notify(f'Retiring {exist.fullname}, {exist.position} at {ticker} from management', 'warning')
            exist.retired = True
            exist.retired_date = datetime.datetime.now().strftime('%Y-%m-%d')
            exist.save()
            count_retired = count_retired + 1

    utils.notify(f'Got {count_new} new executives, {count_retired} retired for {ticker}...','success')
    return (count_new, count_retired)