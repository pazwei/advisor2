# import datetime
# from importers.BaseImporter import BaseImporter

# # import rotator.freeTierApiRotator as rotator
# # import db.queries as queries
# from db.context import Overview
# # from db.context import get_session

# class OverviewImporter(BaseImporter):
#     def __init__(self):
#         super().__init__(Overview.__class__.__name__)

#     def __alpha_vantage(self, symbolIds):
#         #AplhaVantage does not support bulk operations - FUCKERS
#         results = []
#         for symbolId in symbolIds:
#             print(f'Fetching overview of {symbolId}...')
#             querystring = {
#                     'function':'OVERVIEW',
#                     'symbol':symbolId,
#                     'apikey':'%APIKEY%'
#                 }

#             response = rotator.API_ROTATOR.dispatch('AlphaVantage', 'overviews', querystring, None)
#             if response is None or len(response.keys()) < 5 or 'Error Message' in response.keys() or response['Sector'] == 'None' or response['Industry'] == 'None' or response['Address'] == 'None' or response['FiscalYearEnd'] == 'None':
#                 # raise Exception(f'[Data is empty object for {symbolId}]')
#                 print(f'[Data is empty object for {symbolId}]') #TODO change the priority to -2
#                 return

#             obj = Overview()
#             obj.symbolId = symbolId
#             obj.date = datetime.datetime.now()
#             obj.dateCreated = datetime.datetime.now()
#             obj = self.fill_entity(obj, api, response)
#             results.append(obj)
#         return results

#     def __iex(self, symbolIds):
#         #TODO
#         mapped = []
#         url_override = f'https://cloud.iexapis.com/stable/stock/{symbolId}/keystatistics'
#         latest_articles = queries.get_latest_articles(symbolId, 50)

#         querystring = {
#                 'last': '50' if len(latest_articles) == 0 else '10',
#                 'token':'%APIKEY%'
#             }
#         try:
#             response = rotator.API_ROTATOR.memoize(api, 'articlehistory' if len(latest_articles) == 0 else 'articles', querystring, None, url_override)
#             # response2 = None
#             for item in response:
#                 obj = Article()
#                 obj.symbolId = symbolId
#                 obj.dateCreated = datetime.datetime.now()
#                 obj.feedId = None

#                 obj = fill_entity(obj, api, item)
#                 obj.hash = obj.create_hash()
#                 obj.naiveSentiment = classify(obj.join_texts())
#                 # print(obj.naiveSentiment)
#                 # if with_sentiment and obj.hash not in [a.hash for a in latest_articles]:
#                 #     query_string2 = {'lang':'en', 'token':'%APIKEY%', 'text':obj.join_texts()}
#                 #     response2 = rotator.API_ROTATOR.memoize('Dandelion','sentiment', query_string2, None)
            
#                 #     obj.sentimentScore = response2['sentiment']['score']
#                 #     obj.sentiment = response2['sentiment']['type']
#                     # obj.sentimentConfidence = response2['sentiment']['confidence']

#                 # print(obj.hash)
#                 if obj.hash not in [a.hash for a in latest_articles]:
#                     mapped.append(obj)
                
#             return mapped
#         except:
#             print('[Error]') 

#     def __yahoo_fiance(self, symbolIds):
#         pass

# """
# import datetime

# import importers.json_to_entity_mapper as mapper
# import rotator.freeTierApiRotator as rotator
# import db.queries as queries
# from db.context import Overview
# from db.context import get_session

# def refresh(priorities = [2]):
#     symbolIds = []

#     for priority in priorities:
#         stocks = [item for item in queries.get_symbols_by_priority(priority) if item.assetType == 'Stock']
#         symbolIds = symbolIds + [item.id for item in stocks]

#     for symbolId in symbolIds:
#         print(f'[Getting overviews for {symbolId}...]')
#         __refresh_db(symbolId)

# def __refresh_db(symbolId):

#     DAYS_FROM_LAST_RECORD_THRESHOLD = 7

#     last = queries.get_last_overview(symbolId)
#     api = 'AlphaVantage' #TODO add IEX keystats alternative, and make it the default option. Alphavantage is very slow
#     days_passed_from_last_report = (datetime.datetime.now() - last.dateCreated).days if last is not None else DAYS_FROM_LAST_RECORD_THRESHOLD

#     if days_passed_from_last_report >= DAYS_FROM_LAST_RECORD_THRESHOLD:
    
#         if api == 'AlphaVantage':
#             querystring = {
#                 'function':'OVERVIEW',
#                 'symbol':symbolId,
#                 'apikey':'%APIKEY%'
#             }

#             response = rotator.API_ROTATOR.dispatch('AlphaVantage', 'overviews', querystring, None)
#             if response is None or len(response.keys()) < 5 or 'Error Message' in response.keys() or response['Sector'] == 'None' or response['Industry'] == 'None' or response['Address'] == 'None' or response['FiscalYearEnd'] == 'None':
#                 # raise Exception(f'[Data is empty object for {symbolId}]')
#                 print(f'[Data is empty object for {symbolId}]') #TODO change the priority to -2
#                 return

#             obj = Overview()
#             obj.symbolId = symbolId
#             obj.date = datetime.datetime.now()
#             obj.dateCreated = datetime.datetime.now()
#             obj = mapper.fill_entity(obj, api, response)

#             if last is None or (last != None and obj.date > last.date):
#                 # print(response)
#                 queries.update_symbol_from_overview_data(symbolId, response['Sector'], response['Industry'], response['Description'], response['Country'], response['Address'], int(response['FullTimeEmployees']), response['Currency'])
#                 queries.bulk_insert([obj])
#                 print(f'Added overview of {symbolId} and updated the symbol in DB')
#         else: #IEX
#             api = 'IEX'
#             url_override = f'https://cloud.iexapis.com/stable/stock/{symbolId}/keystatistics'
#             latest_articles = queries.get_latest_articles(symbolId, 50)

#             querystring = {
#                     'last': '50' if len(latest_articles) == 0 else '10',
#                     'token':'%APIKEY%'
#                 }
#             try:
#                 response = rotator.API_ROTATOR.memoize(api, 'articlehistory' if len(latest_articles) == 0 else 'articles', querystring, None, url_override)
#                 # response2 = None
#                 mapped = []
#                 for item in response:
#                     obj = Article()
#                     obj.symbolId = symbolId
#                     obj.dateCreated = datetime.datetime.now()
#                     obj.feedId = None

#                     obj = mapper.fill_entity(obj, api, item)
#                     obj.hash = obj.create_hash()
#                     obj.naiveSentiment = classify(obj.join_texts())
#                     # print(obj.naiveSentiment)
#                     # if with_sentiment and obj.hash not in [a.hash for a in latest_articles]:
#                     #     query_string2 = {'lang':'en', 'token':'%APIKEY%', 'text':obj.join_texts()}
#                     #     response2 = rotator.API_ROTATOR.memoize('Dandelion','sentiment', query_string2, None)
                
#                     #     obj.sentimentScore = response2['sentiment']['score']
#                     #     obj.sentiment = response2['sentiment']['type']
#                         # obj.sentimentConfidence = response2['sentiment']['confidence']

#                     # print(obj.hash)
#                     if obj.hash not in [a.hash for a in latest_articles]:
#                         mapped.append(obj)


#     else:
#         print(f'No need to check for data, last record was in {last.dateCreated}')
# """