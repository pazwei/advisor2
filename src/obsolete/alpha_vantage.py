#https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=IBM&outputsize=full&apikey=demo

import freeTierApiRotator

# standard API volume limit 5 API requests per minute; 500 API requests per day

def get_records(symbol, history = False):
    querystring = {'function':'TIME_SERIES_DAILY_ADJUSTED',
    'symbol':symbol,
    'outputsize':'full' if history else 'compact',
    'apikey':'%APIKEY%'
    }
    """
    "Time Series (Daily)": {"2020-12-28": {
            "1. open": "125.1",
            "2. high": "126.6",
            "3. low": "124.46",
            "4. close": "124.82",
            "5. adjusted close": "124.82",
            "6. volume": "3583222",
            "7. dividend amount": "0.0000",
            "8. split coefficient": "1.0"
        },...}
    """
    return freeTierApiRotator.API_ROTATOR.dispatch('AlphaVantage', 'records', querystring, None) #history is not cached, each response is 1.7Mb of json

def get_today(symbol):
    querystring = {'function':'GLOBAL_QUOTE',
    'symbol':symbol,
    'apikey':'%APIKEY%'
    }
    return freeTierApiRotator.API_ROTATOR.memoize('AlphaVantage', 'today', querystring, None)
    """
    {
    "Global Quote": {
        "01. symbol": "IBM",
        "02. open": "125.1000",
        "03. high": "126.6000",
        "04. low": "124.4600",
        "05. price": "124.8200",
        "06. volume": "3583222",
        "07. latest trading day": "2020-12-28",
        "08. previous close": "124.6900",
        "09. change": "0.1300",
        "10. change percent": "0.1043%"
    }
}
    """

def get_balancesheets(symbol):
    querystring = {'function':'BALANCE_SHEET',
    'symbol':symbol,
    'apikey':'%APIKEY%'
    }

    """
    {
    "symbol": "IBM",
    "annualReports": [
        {
            "fiscalDateEnding": "2019-12-31",
            "reportedCurrency": "USD",
            "totalAssets": "152186000000",
            "intangibleAssets": "15235000000",
            "earningAssets": "None",
            "otherCurrentAssets": "3997000000",
            "totalLiabilities": "131202000000",
            "totalShareholderEquity": "20841000000",
            "deferredLongTermLiabilities": "3851000000",
            "otherCurrentLiabilities": "13406000000",
            "commonStock": "55895000000",
            "retainedEarnings": "162954000000",
            "otherLiabilities": "35519000000",
            "goodwill": "58222000000",
            "otherAssets": "16369000000",
            "cash": "8313000000",
            "totalCurrentLiabilities": "37701000000",
            "shortTermDebt": "8797000000",
            "currentLongTermDebt": "8797000000",
            "otherShareholderEquity": "-198010000000",
            "propertyPlantEquipment": "10010000000",
            "totalCurrentAssets": "38420000000",
            "longTermInvestments": "10786000000",
            "netTangibleAssets": "-52617000000",
            "shortTermInvestments": "696000000",
            "netReceivables": "23795000000",
            "longTermDebt": "54102000000",
            "inventory": "1619000000",
            "accountsPayable": "15498000000",
            "totalPermanentEquity": "None",
            "additionalPaidInCapital": "None",
            "commonStockTotalEquity": "55895000000",
            "preferredStockTotalEquity": "None",
            "retainedEarningsTotalEquity": "162954000000",
            "treasuryStock": "-169413000000",
            "accumulatedAmortization": "None",
            "otherNonCurrrentAssets": "14333000000",
            "deferredLongTermAssetCharges": "None",
            "totalNonCurrentAssets": "5182000000",
            "capitalLeaseObligations": "5259000000",
            "totalLongTermDebt": "54102000000",
            "otherNonCurrentLiabilities": "35547000000",
            "totalNonCurrentLiabilities": "93500000000",
            "negativeGoodwill": "None",
            "warrants": "None",
            "preferredStockRedeemable": "None",
            "capitalSurplus": "55447410000",
            "liabilitiesAndShareholderEquity": "152186000000",
            "cashAndShortTermInvestments": "9009000000",
            "accumulatedDepreciation": "-22018000000",
            "commonStockSharesOutstanding": "887110455"
        },
    """
    return freeTierApiRotator.API_ROTATOR.memoize('AlphaVantage', 'balancesheets', querystring, None)

def get_income_statements(symbol):
    querystring = {'function':'INCOME_STATEMENT',
    'symbol':symbol,
    'apikey':'%APIKEY%'
    }
    """
    {
    "symbol": "IBM",
    "annualReports": [
        {
            "fiscalDateEnding": "2019-12-31",
            "reportedCurrency": "USD",
            "totalRevenue": "77147000000",
            "totalOperatingExpense": "25945000000",
            "costOfRevenue": "40659000000",
            "grossProfit": "36488000000",
            "ebit": "11511000000",
            "netIncome": "9431000000",
            "researchAndDevelopment": "5989000000",
            "effectOfAccountingCharges": "None",
            "incomeBeforeTax": "10166000000",
            "minorityInterest": "144000000",
            "sellingGeneralAdministrative": "19956000000",
            "otherNonOperatingIncome": "968000000",
            "operatingIncome": "10543000000",
            "otherOperatingExpense": "-614000000",
            "interestExpense": "1344000000",
            "taxProvision": "731000000",
            "interestIncome": "349000000",
            "netInterestIncome": "-995000000",
            "extraordinaryItems": "-150000000",
            "nonRecurring": "None",
            "otherItems": "None",
            "incomeTaxExpense": "731000000",
            "totalOtherIncomeExpense": "529000000",
            "discontinuedOperations": "-4000000",
            "netIncomeFromContinuingOperations": "9435000000",
            "netIncomeApplicableToCommonShares": "9431000000",
            "preferredStockAndOtherAdjustments": "None"
        },
    """
    return freeTierApiRotator.API_ROTATOR.dispatch('AlphaVantage','incomestatements', querystring, None)

def get_cashflows(symbol):
    querystring = {'function':'CASH_FLOW',
    'symbol':symbol,
    'apikey':'%APIKEY%'
    }
    """
    {
    "symbol": "IBM",
    "annualReports": [
        {
            "fiscalDateEnding": "2019-12-31",
            "reportedCurrency": "USD",
            "investments": "6988000000",
            "changeInLiabilities": "-503000000",
            "cashflowFromInvestment": "-26936000000",
            "otherCashflowFromInvestment": "-31638000000",
            "netBorrowings": "16284000000",
            "cashflowFromFinancing": "9042000000",
            "otherCashflowFromFinancing": "-173000000",
            "changeInOperatingActivities": "1159000000",
            "netIncome": "9431000000",
            "changeInCash": "-3124000000",
            "operatingCashflow": "14770000000",
            "otherOperatingCashflow": "63000000",
            "depreciation": "6059000000",
            "dividendPayout": "-5707000000",
            "stockSaleAndPurchase": "-1361000000",
            "changeInInventory": "67000000",
            "changeInAccountReceivables": "491000000",
            "changeInNetIncome": "-848000000",
            "capitalExpenditures": "2286000000",
            "changeInReceivables": "502000000",
            "changeInExchangeRate": "None",
            "changeInCashAndCashEquivalents": "-3124000000"
        },
    """
    return freeTierApiRotator.API_ROTATOR.dispatch('AlphaVantage', 'cashflows', querystring ,None)

def get_earnings(symbol):
    querystring = {'function':'EARNINGS',
    'symbol':symbol,
    'apikey':'%APIKEY%'
    }
    
    """
        {
            "fiscalDateEnding": "1997-12-31",
            "reportedEPS": "3.09"
        },
        {
            "fiscalDateEnding": "1996-12-31",
            "reportedEPS": "2.5662"
        }
    ],
    "quarterlyEarnings": [
        {
            "fiscalDateEnding": "2020-09-30",
            "reportedDate": "2020-10-19",
            "reportedEPS": "2.58",
            "estimatedEPS": "2.579",
            "surprise": "0.001",
            "surprisePercentage": "0.0388"
        },
    """
    return freeTierApiRotator.API_ROTATOR.memoize('AlphaVantage', 'earnings', querystring ,None)

#return csv
def get_listings(symbol):
    querystring = {'function':'LISTING_STATUS',
    'symbol':symbol,
    'apikey':'%APIKEY%'
    }
    return freeTierApiRotator.API_ROTATOR.dispatch('AlphaVantage', 'listings', querystring ,None)

def get_overview(symbol, history = False):
    querystring = {'function':'OVERVIEW',
    'symbol':symbol,
    'apikey':'%APIKEY%'
    }
    """
    {
    "Symbol": "IBM",
    "AssetType": "Common Stock",
    "Name": "International Business Machines Corporation",
    "Description": "International Business Machines Corporation provides integrated solutions and services worldwide. Its Cloud & Cognitive Software segment offers software for vertical and domain-specific solutions in health, financial services, and Internet of Things (IoT), weather, and security software and services application areas; and customer information control system and storage, and analytics and integration software solutions to support client mission critical on-premise workloads in banking, airline, and retail industries. It also offers middleware and data platform software, including Red Hat that enables the operation of clients' hybrid multi-cloud environments; and Cloud Paks, WebSphere distributed, and analytics platform software, such as DB2 distributed, information integration, and enterprise content management, as well as IoT, Blockchain and AI/Watson platforms. The company's Global Business Services segment offers business consulting services; system integration, application management, maintenance, and support services for packaged software; finance, procurement, talent and engagement, and industry-specific business process outsourcing services; and IT infrastructure and platform services. Its Global Technology Services segment provides project, managed, outsourcing, and cloud-delivered services for enterprise IT infrastructure environments; and IT infrastructure support services. The company's Systems segment offers servers for businesses, cloud service providers, and scientific computing organizations; data storage products and solutions; and z/OS, an enterprise operating system, as well as Linux. Its Global Financing segment provides lease, installment payment, loan financing, short-term working capital financing, and remanufacturing and remarketing services. The company was formerly known as Computing-Tabulating-Recording Co. The company was founded in 1911 and is headquartered in Armonk, New York.",
    "Exchange": "NYSE",
    "Currency": "USD",
    "Country": "USA",
    "Sector": "Technology",
    "Industry": "Information Technology Services",
    "Address": "One New Orchard Road, Armonk, NY, United States, 10504",
    "FullTimeEmployees": "352600",
    "FiscalYearEnd": "December",
    "LatestQuarter": "2020-09-30",
    "MarketCapitalization": "111221735424",
    "EBITDA": "15690000384",
    "PERatio": "14.1471",
    "PEGRatio": "9.2577",
    "BookValue": "23.801",
    "DividendPerShare": "6.52",
    "DividendYield": "0.0523",
    "EPS": "8.823",
    "RevenuePerShareTTM": "84.402",
    "ProfitMargin": "0.1053",
    "OperatingMarginTTM": "0.1205",
    "ReturnOnAssetsTTM": "0.0372",
    "ReturnOnEquityTTM": "0.401",
    "RevenueTTM": "75030003712",
    "GrossProfitTTM": "36489000000",
    "DilutedEPSTTM": "8.823",
    "QuarterlyEarningsGrowthYOY": "0.011",
    "QuarterlyRevenueGrowthYOY": "-0.026",
    "AnalystTargetPrice": "137.13",
    "TrailingPE": "14.1471",
    "ForwardPE": "10.6496",
    "PriceToSalesRatioTTM": "1.4879",
    "PriceToBookRatio": "5.2389",
    "EVToRevenue": "2.2111",
    "EVToEBITDA": "10.8346",
    "Beta": "1.2399",
    "52WeekHigh": "150.8394",
    "52WeekLow": "86.9458",
    "50DayMovingAverage": "122.2753",
    "200DayMovingAverage": "121.8666",
    "SharesOutstanding": "891057024",
    "SharesFloat": "889453213",
    "SharesShort": "22915875",
    "SharesShortPriorMonth": "23255478",
    "ShortRatio": "4.53",
    "ShortPercentOutstanding": "0.03",
    "ShortPercentFloat": "0.0257",
    "PercentInsiders": "0.132",
    "PercentInstitutions": "58.583",
    "ForwardAnnualDividendRate": "6.52",
    "ForwardAnnualDividendYield": "0.0523",
    "PayoutRatio": "0.5756",
    "DividendDate": "2020-12-10",
    "ExDividendDate": "2020-11-09",
    "LastSplitFactor": "2:1",
    "LastSplitDate": "1999-05-27"
}
    """
    return freeTierApiRotator.API_ROTATOR.memoize('AlphaVantage','records', querystring ,None)