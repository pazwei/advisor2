#!/usr/bin/env python3
import freeTierApiRotator

def analyze_sentiment(text):
    query_string = {'lang':'en', 'token':'%APIKEY%', 'text':text}
    json_payload = None
    response = freeTierApiRotator.API_ROTATOR.memoize('Dandelion','sentiment', query_string, json_payload)
    
    return {
        'score': response['sentiment']['score'],
        'type': response['sentiment']['type'],
        'confidence': response['sentiment']['confidence']
    }

def extract_entities(text):
    query_string = {'lang':'en', 'token':'%APIKEY%', 'text':text}
    json_payload = None
    response = freeTierApiRotator.API_ROTATOR.memoize('Dandelion','keywords', query_string, json_payload)
    
    result = []
    for annotation in response['annotations']:
        result.append({'spot':annotation['spot'], 'title': annotation['title'], 'confidence': annotation['confidence'], 'uri': annotation['uri']})
    
    return result

should_be_negative = 'The U.S. Department of Justice (DOJ) filed a lawsuit against Facebook (NASDAQ: FB) on Thursday, alleging that the company favored foreign workers with temporary immigration status over native-born U.S. applicants. The DOJs civil rights department charged that holders of H1-B visa'
should_be_positive = 'As Apple (NASDAQ: AAPL) begins sales of 5G devices and ramps up for the holiday season, investors have good reason for optimism. Amid the shifts in workloads brought about by COVID-19, demand for Apple devices has increased on many fronts.However, the stock has also risen significantly'

def test_analyze_sentiment():
    print(f'Should be negative: {analyze_sentiment(should_be_negative)["score"]}')
    print(f'Should be positive: {analyze_sentiment(should_be_positive)["score"]}')

def test_extract_entities():
    print(', '.join(sorted([item['spot'] for item in extract_entities(should_be_negative)], key=lambda x: x['confidence']))

if __name__ == '__main__':
    test_extract_entities()