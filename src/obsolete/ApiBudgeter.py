import configparser
import os.path as path
import datetime
from datetime import time
import pickle
import json

import requests

class ApiBudgeter():
    """
    Track API budget
    """
    def __init__(self, api_config, verbose = True):

        # self.api_config = {
        #     'IEX':{ 'DailyDataBudget':50000, 'SecondsDelay':1, 'apiKey':'' },
        #     'AlphaVantage':{ 'DailyDataBudget':500, 'SecondsDelay':10, 'apiKey':''}
        # }
        self.api_config = api_config
        self.verbose = verbose
        self.is_first_iteration = True

        script_dir = path.dirname(__file__)
        pickle_file = 'apiBudgeterState.pickle'
        self.PICKLE_FILE = path.join(script_dir, pickle_file)
        self.state = self.__load_saved_state()

    def dispatch(self, api_name, verb, url, querystring_params, json_payload, is_last_iteration = True):
        today = datetime.datetime.now().date()
        if self.state[today][api_name]['DailyDataBudget'] > 0:
            if querystring_params is not None:
                qs = '&'.join([f'{key}={querystring_params[key]}' for key in querystring_params.keys()]).replace('%APIKEY%', self.api_config[api_name]['ApiKey'])
                compiled_endpoint = f'{url}?{qs}' #f'https://api.dandelion.eu/datatxt/nex/v1/?lang=en&token=%APIKEY%'
            
            compiled_json_payload = json_payload
            if compiled_json_payload is not None:
                for key in compiled_json_payload.keys():
                    if compiled_json_payload[key] == '%APIKEY%':
                        compiled_json_payload[key] = self.api_config[api_name]['ApiKey']
            
            print(f'[Dispatch to="{compiled_endpoint}"]')

            try:
                data = None
                if verb == 'post':
                    res = requests.post(compiled_endpoint, compiled_json_payload)
                elif verb == 'get':
                    res = requests.get(compiled_endpoint, compiled_json_payload)
                elif verb == 'put':
                    res = requests.put(compiled_endpoint, compiled_json_payload)
                elif verb == 'delete':
                    res = requests.delete(compiled_endpoint, compiled_json_payload)
                elif verb == 'patch':
                    res = requests.patch(compiled_endpoint, compiled_json_payload)

                data = res.json()

                if not self.is_first_iteration and is_last_iteration:
                    delay = int(self.api_config[api_name]['SecondsDelay'])
                    print(f'[Delaying for {delay} seconds...]')
                    time.sleep(delay)

                self.is_first_iteration = False

                if self.api_config["AutoPay"]:
                    self.pay(self.api_name, unit_cost, units)

                return data

            except Exception as e:
                print(f'FAILED to access {compiled_endpoint} with json payload {compiled_json_payload}')
                print(e)
        else:
            print(f'[No credits left for {api_name}!]')

    def pay(self, api_name, unit_cost, units):
        today = datetime.datetime.now().date()
        self.state[today][api_name] -= unit_cost * units
        self.__save_state()
        
    def __save_state(self):
        file = open(self.PICKLE_FILE, 'wb')
        pickle.dump(self.state, file)
        file.close()

    def __load_saved_state(self):
        if path.exists(self.PICKLE_FILE):
            file = open(self.PICKLE_FILE, 'rb')
            state = pickle.load(file)
            file.close()

            today = datetime.datetime.now().date()
            if today not in self.state.keys():
                state = {**state, **self.__init_state()}
            return state
        else:
            return self.__init_state()

    def __init_state(self):
        today = datetime.datetime.now().date()
        print(self.api_config)
        counters = {key:value['DailyDataBudget'] for key,value in self.api_config.items()}
        state = {today:counters}
        return state