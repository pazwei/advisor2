import yfinance as yf
from pprint import pprint

#https://pypi.org/project/yfinance/

msft = yf.Ticker("MSFT")

# get stock info
msft.info

# get historical market data
hist = msft.history(period="max")

# show actions (dividends, splits)
msft.actions

# show dividends
msft.dividends

# show splits
msft.splits

# show financials
msft.financials
msft.quarterly_financials

# show major holders
msft.major_holders

# show institutional holders
msft.institutional_holders

# show balance sheet
msft.balance_sheet
msft.quarterly_balance_sheet

# show cashflow
msft.cashflow
msft.quarterly_cashflow

# show earnings
msft.earnings
msft.quarterly_earnings

# show sustainability
msft.sustainability

# show analysts recommendations
msft.recommendations

# show next event (earnings, etc)
msft.calendar

# show ISIN code - *experimental*
# ISIN = International Securities Identification Number
msft.isin

# show options expirations
msft.options

# get option chain for specific expiration
opt = msft.option_chain('YYYY-MM-DD')
# data available via: opt.calls, opt.puts

"""
>>> pprint.pprint(msft.info)
{'52WeekChange': 0.2771771,
 'SandP52WeekChange': 0.134745,
 'address1': 'One Microsoft Way',
 'algorithm': None,
 'annualHoldingsTurnover': None,
 'annualReportExpenseRatio': None,
 'ask': 212.64,
 'askSize': 1300,
 'averageDailyVolume10Day': 26767137,
 'averageVolume': 28207888,
 'averageVolume10days': 26767137,
 'beta': 0.826155,
 'beta3Year': None,
 'bid': 212.42,
 'bidSize': 1300,
 'bookValue': 16.313,
 'category': None,
 'circulatingSupply': None,
 'city': 'Redmond',
 'companyOfficers': [],
 'country': 'United States',
 'currency': 'USD',
 'dateShortInterest': 1609372800,
 'dayHigh': 214.51,
 'dayLow': 212.03,
 'dividendRate': 2.24,
 'dividendYield': 0.0105,
 'earningsQuarterlyGrowth': 0.301,
 'enterpriseToEbitda': 22.797,
 'enterpriseToRevenue': 10.556,
 'enterpriseValue': 1553006460928,
 'exDividendDate': 1613520000,
 'exchange': 'NMS',
 'exchangeTimezoneName': 'America/New_York',
 'exchangeTimezoneShortName': 'EST',
 'expireDate': None,
 'fiftyDayAverage': 217.19618,
 'fiftyTwoWeekHigh': 232.86,
 'fiftyTwoWeekLow': 132.52,
 'fiveYearAverageReturn': None,
 'fiveYearAvgDividendYield': 1.74,
 'floatShares': 7449961518,
 'forwardEps': 7.47,
 'forwardPE': 28.467201,
 'fromCurrency': None,
 'fullTimeEmployees': 163000,
 'fundFamily': None,
 'fundInceptionDate': None,
 'gmtOffSetMilliseconds': '-18000000',
 'heldPercentInsiders': 0.00059,
 'heldPercentInstitutions': 0.71844,
 'impliedSharesOutstanding': None,
 'industry': 'Software—Infrastructure',
 'isEsgPopulated': False,
 'lastCapGain': None,
 'lastDividendDate': 1605657600,
 'lastDividendValue': 0.56,
 'lastFiscalYearEnd': 1593475200,
 'lastMarket': None,
 'lastSplitDate': 1045526400,
 'lastSplitFactor': '2:1',
 'legalType': None,
 'logo_url': 'https://logo.clearbit.com/microsoft.com',
 'longBusinessSummary': 'Microsoft Corporation develops, licenses, and '
                        'supports software, services, devices, and solutions '
                        'worldwide. Its Productivity and Business Processes '
                        'segment offers Office, Exchange, SharePoint, '
                        'Microsoft Teams, Office 365 Security and Compliance, '
                        'and Skype for Business, as well as related Client '
                        'Access Licenses (CAL); Skype, Outlook.com, OneDrive, '
                        'and LinkedIn; and Dynamics 365, a set of cloud-based '
                        'and on-premises business solutions for small and '
                        'medium businesses, large organizations, and divisions '
                        'of enterprises. Its Intelligent Cloud segment '
                        'licenses SQL and Windows Servers, Visual Studio, '
                        'System Center, and related CALs; GitHub that provides '
                        'a collaboration platform and code hosting service for '
                        'developers; and Azure, a cloud platform. It also '
                        'offers support services and Microsoft consulting '
                        'services to assist customers in developing, '
                        'deploying, and managing Microsoft server and desktop '
                        'solutions; and training and certification to '
                        'developers and IT professionals on various Microsoft '
                        'products. Its More Personal Computing segment '
                        'provides Windows original equipment manufacturer '
                        '(OEM) licensing and other non-volume licensing of the '
                        'Windows operating system; Windows Commercial, such as '
                        'volume licensing of the Windows operating system, '
                        'Windows cloud services, and other Windows commercial '
                        'offerings; patent licensing; Windows Internet of '
                        'Things; and MSN advertising. It also offers Surface, '
                        'PC accessories, PCs, tablets, gaming and '
                        'entertainment consoles, and other devices; Gaming, '
                        'including Xbox hardware, and Xbox content and '
                        'services; video games and third-party video game '
                        'royalties; and Search, including Bing and Microsoft '
                        'advertising. It sells its products through OEMs, '
                        'distributors, and resellers; and directly through '
                        'digital marketplaces, online stores, and retail '
                        'stores. It has a strategic collaboration with DXC '
                        'Technology. The company was founded in 1975 and is '
                        'headquartered in Redmond, Washington.',
 'longName': 'Microsoft Corporation',
 'market': 'us_market',
 'marketCap': 1607740293120,
 'maxAge': 1,
 'maxSupply': None,
 'messageBoardId': 'finmb_21835',
 'morningStarOverallRating': None,
 'morningStarRiskRating': None,
 'mostRecentQuarter': 1601424000,
 'navPrice': None,
 'netIncomeToCommon': 47495999488,
 'nextFiscalYearEnd': 1656547200,
 'open': 213.52,
 'openInterest': None,
 'payoutRatio': 0.32900003,
 'pegRatio': 2.17,
 'phone': '425-882-8080',
 'previousClose': 213.02,
 'priceHint': 2,
 'priceToBook': 13.035616,
 'priceToSalesTrailing12Months': 10.928534,
 'profitMargins': 0.32285,
 'quoteType': 'EQUITY',
 'regularMarketDayHigh': 214.51,
 'regularMarketDayLow': 212.03,
 'regularMarketOpen': 213.52,
 'regularMarketPreviousClose': 213.02,
 'regularMarketPrice': 213.52,
 'regularMarketVolume': 31746512,
 'revenueQuarterlyGrowth': None,
 'sector': 'Technology',
 'sharesOutstanding': 7560500224,
 'sharesPercentSharesOut': 0.0052,
 'sharesShort': 39201229,
 'sharesShortPreviousMonthDate': 1606694400,
 'sharesShortPriorMonth': 39002778,
 'shortName': 'Microsoft Corporation',
 'shortPercentOfFloat': 0.0052,
 'shortRatio': 1.44,
 'startDate': None,
 'state': 'WA',
 'strikePrice': None,
 'symbol': 'MSFT',
 'threeYearAverageReturn': None,
 'toCurrency': None,
 'totalAssets': None,
 'tradeable': False,
 'trailingAnnualDividendRate': 2.09,
 'trailingAnnualDividendYield': 0.009811285,
 'trailingEps': 6.199,
 'trailingPE': 34.30392,
 'twoHundredDayAverage': 212.71504,
 'volume': 31746512,
 'volume24Hr': None,
 'volumeAllCurrencies': None,
 'website': 'http://www.microsoft.com',
 'yield': None,
 'ytdReturn': None,
 'zip': '98052-6399'}
"""