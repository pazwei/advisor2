
# #!/usr/bin/env python3

# ###########
# # OBSOLETE
# ###########

# import base64
# import io
# import pprint

# import pandas as pd
# import matplotlib.pyplot as plt
# from alpha_vantage.timeseries import TimeSeries
# from alpha_vantage.sectorperformance import SectorPerformances
# from alpha_vantage.techindicators import TechIndicators

# import config
# from decorators import delay

# # ******************************************************************
# # Get historical data for all the tickers listed, including indices
# # http://cafim.sssup.it/~giulio/other/alpha_vantage/index.html
# # https://github.com/RomelTorres/alpha_vantage
# # ******************************************************************

# @delay
# def get_interday_records(interval = '1hour', symbol = 'MSFT'):
#     """
#     intervals could be ['1min', '5min', '15min', '30min', '60min']
#     """
#     ts = TimeSeries(key=config.ALPHA_VANTAGE_APIKEY, output_format='pandas')
#     data, meta_data = ts.get_intraday(symbol=symbol, interval='1min', outputsize='full')
#     pprint.pprint(data.head(10))
#     return data

# def get_interday_records_chart(data, meta_data):
#     pass

# @delay
# def get_daily_records(symbol = 'MSFT'):
#     ts = TimeSeries(key=config.ALPHA_VANTAGE_APIKEY, output_format='pandas')
#     data, meta_data = ts.get_daily_adjusted(symbol=symbol, outputsize='full')
#     pprint.pprint(data.head(2))
#     return data

# def insert_daily_records(symbol_id, df):
#     last_record = queries.get_last_record(symbol_id)
#     new_rows = df[df.date > last_record.date]
#     records = [db.Record(date=i['date'], symbolId=symbol_id, open=i['open'], close=i['close'], high=i['high'], low=i['low'], change=i['change'], volume=i['volume']) for i in new_rows]
#     queries.bulk_insert(records)

# def get_daily_records_chart(data, meta_data):
#     pass

# @delay
# def get_sector_performance():
#     sp = SectorPerformances(key=config.ALPHA_VANTAGE_APIKEY, output_format='pandas')
#     data, meta_data = sp.get_sector()
#     return data, meta_data

# def get_sector_performance_chart(data, meta_data, display = False):
#     data['Rank A: Real-Time Performance'].plot(kind='bar')
#     plt.title('Real Time Performance (%) per Sector')
#     plt.tight_layout()
#     plt.grid()

#     if display:
#         plt.show()
#         return None
#     else:
#         pic_IObytes = io.BytesIO()
#         plt.savefig(pic_IObytes,  format='png')
#         pic_IObytes.seek(0)
#         pic_hash = base64.b64encode(pic_IObytes.read())
#         return pic_hash

# @delay
# def get_technical_indicators_charts(symbol, indicator, periods):
#     ti = TechIndicators(key=config.ALPHA_VANTAGE_APIKEY, output_format='pandas')
#     if indicator == 'rsi':
#         data, meta_data = ti.get_rsi(symbol=symbol, interval='daily', time_period=periods)
#     elif indicator == 'bolinger':
#         data, meta_data = ti.get_bbands(symbol=symbol, interval='daily', time_period=periods)
#     elif indicator == 'stochastics':
#         data, meta_data = ti.get_stoch(symbol=symbol, interval='daily', time_period=periods)
#     elif indicator == 'stochastic_rsi':
#         data, meta_data = ti.get_stochrsi(symbol=symbol, interval='daily', time_period=periods)
#     elif indicator == 'momentum':
#         data, meta_data = ti.get_mom(symbol=symbol, interval='daily', time_period=periods)
#     elif indicator == 'sma':
#         data, meta_data = ti.get_sma(symbol=symbol, interval='daily', time_period=periods)
#     elif indicator == 'macd':
#         data, meta_data = ti.get_macd(symbol=symbol, interval='daily', time_period=periods)
#     elif indicator == 'macd_text':
#         data, meta_data = ti.get_macdext(symbol=symbol, interval='daily', time_period=periods)
    
#     data.plot()
#     plt.title('BBbands indicator for  MSFT stock (60 min)')
#     plt.show()
#     return data

# def get_all_symbols():
#     pass

# @delay
# def get_historical_records(symbolId):
#     pass

# if __name__ == '__main__':
#     data, meta_data = get_sector_performance()
#     print(data)
#     get_sector_performance_chart(data, meta_data, True)