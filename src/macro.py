#!/usr/bin/env python3
import signal
import sys
import argparse
import datetime

import logic.macro as macro
import logic.charts as charts
import quotes.quote as quote

###########
# globals #
###########
from_date = datetime.datetime(2020,1,1)
to_date = datetime.datetime.now()
symbol = ''
function = ''

def graceful_exit(signal, frame):
    print(quote.get())
    sys.exit(0)

def set_args():
    global symbol
    global from_date
    global to_date
    global function

    # https://docs.python.org/3/library/argparse.html
    parser = argparse.ArgumentParser(description='Macro Analyze')
    parser.add_argument("--fn", "--function", help="Function to execute. 'gl' for gainers and losers, 'gla' for gainer and losers by articles, 'sip' for sector and industry performance, etf for etf returns", type=str, default='sip', required=True)
    parser.add_argument("--s", "--symbol", help="Symbol", type=str, default='MSFT', required=False)
    parser.add_argument("--f", "--from", help="From date yyyy-mm-dd", type=valid_date, default='2020-01-01', required=False)
    parser.add_argument("--t", "--to", help="From date yyyy-mm-dd", type=valid_date, default=datetime.datetime.now().strftime('%Y-%m-%d'), required=False)
    
    args = parser.parse_args()

    from_date = args.f
    to_date = args.t
    symbol = args.s
    function = args.fn

def valid_date(s):
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)

def main():
    global symbol
    global from_date
    global to_date
    global function

    if function == 'glst':
        macro.gainers_and_losers()
    if function == 'gllt':
        macro.gainers_and_losers(time_frames = [19,49,99], sort_by_time_frame = 99)
    elif function == 'gla':
        macro.get_gainer_and_losers_by_articles()
    elif function == 'glp':
        macro.gainers_and_losers_by_posts()
    elif function == 'sip':
        macro.get_sector_and_industry_performance()
    elif function == 'etf':
        macro.get_etf_performance()
    elif function == 'analyst':
        macro.get_symbols_by_analyst_recommendations()
    elif function == 'dividend':
        macro.get_symbols_by_next_dividend_yield()
    elif function == 'bot':
        telegram.main()

if __name__ == '__main__':
    signal.signal(signal.SIGINT, graceful_exit)
    set_args()
    main()