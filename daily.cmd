@REM @ECHO OFF
SET now=%date:~6,4%-%date:~3,2%-%date:~0,2%
@REM echo.%now%
@REM Put the following command in Task Scheduler "pipenv run src\job.cmd", start directory is C:\pazdev\advisor, schedule the job to run daily in the evening

git pull
git add *
git push
python src\importer.py --e Post,Record,Article,Dividend,IncomeStatement,BalanceSheet,CashFlow,Earning,Analyst,Ipo
git add *
git commit -m "Add %now% data"
git push